-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Jeu 11 Juin 2015 à 13:48
-- Version du serveur: 5.1.44
-- Version de PHP: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `docoeur`
--

-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_ap_meta`
--

CREATE TABLE IF NOT EXISTS `dcHyv_ap_meta` (
  `apmeta_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `apmeta_userid` bigint(20) DEFAULT NULL,
  `apmeta_type` varchar(256) DEFAULT NULL,
  `apmeta_actionid` bigint(20) DEFAULT NULL,
  `apmeta_value` text,
  `apmeta_param` longtext,
  `apmeta_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`apmeta_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `dcHyv_ap_meta`
--


-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_commentmeta`
--

CREATE TABLE IF NOT EXISTS `dcHyv_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `dcHyv_commentmeta`
--


-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_comments`
--

CREATE TABLE IF NOT EXISTS `dcHyv_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `dcHyv_comments`
--

INSERT INTO `dcHyv_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(2, 64, 'House Gregory', 'house@princeton.hospital.com', 'http://fr.wikipedia.org/wiki/Gregory_House', '10.192.189.9', '2015-06-10 14:10:12', '2015-06-10 13:10:12', 'Rien de tel qu''un bon steak pour réguler tout ça!', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', '', 0, 5),
(3, 64, 'moderateur1', 'moderateur@test.ch', '', '10.192.189.9', '2015-06-10 14:14:52', '2015-06-10 13:14:52', 'Ou alors se taire et laisser les "VRAIS" spécialistes répondre', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', '', 2, 4),
(4, 64, 'membre1', 'membre1@test.ch', '', '10.192.189.9', '2015-06-10 14:31:59', '2015-06-10 13:31:59', 'C net!', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0', '', 3, 3),
(5, 64, 'moderateur1', 'moderateur@test.ch', '', '10.192.189.9', '2015-06-11 10:00:56', '2015-06-11 09:00:56', '<i class="fa fa-heart red"></i> ;) \r\n;-)\r\n:( :-(\r\n:) :-)\r\n<3', 0, 'spam', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36', '', 0, 4);

-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_links`
--

CREATE TABLE IF NOT EXISTS `dcHyv_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `dcHyv_links`
--


-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_options`
--

CREATE TABLE IF NOT EXISTS `dcHyv_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1429 ;

--
-- Contenu de la table `dcHyv_options`
--

INSERT INTO `dcHyv_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://127.0.0.1/docoeur', 'yes'),
(2, 'home', 'http://127.0.0.1/docoeur', 'yes'),
(3, 'blogname', 'docoeur.ch', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '1', 'yes'),
(6, 'admin_email', 'elie.turc@cpnv.ch', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '1', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F Y', 'yes'),
(24, 'time_format', 'G \\h i \\m\\i\\n', 'yes'),
(25, 'links_updated_date_format', 'j F Y G \\h i \\m\\i\\n', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'gzipcompression', '0', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:8:{i:1;s:41:"dw-question-answer/dw-question-answer.php";i:2;s:33:"nav-menu-roles/nav-menu-roles.php";i:3;s:33:"theme-my-login/theme-my-login.php";i:4;s:27:"updraftplus/updraftplus.php";i:5;s:37:"user-role-editor/user-role-editor.php";i:6;s:53:"wp-roles-at-registration/wp-roles-at-registration.php";i:7;s:25:"wp-user-frontend/wpuf.php";i:8;s:24:"wp-users-media/index.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'advanced_edit', '0', 'yes'),
(37, 'comment_max_links', '2', 'yes'),
(38, 'gmt_offset', '1', 'yes'),
(39, 'default_email_category', '1', 'yes'),
(40, 'recently_edited', 'a:5:{i:0;s:85:"/Applications/XAMPP/xamppfiles/htdocs/wp_v2/wp-content/themes/base-wp-child/style.css";i:2;s:87:"/Applications/XAMPP/xamppfiles/htdocs/wp_v2/wp-content/plugins/wp-users-media/index.php";i:3;s:88:"/Applications/XAMPP/xamppfiles/htdocs/wp_v2/wp-content/plugins/wp-users-media/readme.txt";i:4;s:79:"/Applications/XAMPP/xamppfiles/htdocs/wp_v2/wp-content/themes/base-wp/style.css";i:5;s:89:"/Applications/XAMPP/xamppfiles/htdocs/wp_v2/wp-content/themes/base-wp-child/functions.php";}', 'no'),
(41, 'template', 'base-wp', 'yes'),
(42, 'stylesheet', 'base-wp-child', 'yes'),
(43, 'comment_whitelist', '1', 'yes'),
(44, 'blacklist_keys', '', 'no'),
(45, 'comment_registration', '1', 'yes'),
(46, 'html_type', 'text/html', 'yes'),
(47, 'use_trackback', '0', 'yes'),
(48, 'default_role', 'membre', 'yes'),
(49, 'db_version', '31535', 'yes'),
(50, 'uploads_use_yearmonth_folders', '1', 'yes'),
(51, 'upload_path', '', 'yes'),
(52, 'blog_public', '0', 'yes'),
(53, 'default_link_category', '2', 'yes'),
(54, 'show_on_front', 'posts', 'yes'),
(55, 'tag_base', '', 'yes'),
(56, 'show_avatars', '1', 'yes'),
(57, 'avatar_rating', 'G', 'yes'),
(58, 'upload_url_path', '', 'yes'),
(59, 'thumbnail_size_w', '150', 'yes'),
(60, 'thumbnail_size_h', '150', 'yes'),
(61, 'thumbnail_crop', '1', 'yes'),
(62, 'medium_size_w', '300', 'yes'),
(63, 'medium_size_h', '300', 'yes'),
(64, 'avatar_default', 'identicon', 'yes'),
(65, 'large_size_w', '1024', 'yes'),
(66, 'large_size_h', '1024', 'yes'),
(67, 'image_default_link_type', 'file', 'yes'),
(68, 'image_default_size', '', 'yes'),
(69, 'image_default_align', '', 'yes'),
(70, 'close_comments_for_old_posts', '', 'yes'),
(71, 'close_comments_days_old', '14', 'yes'),
(72, 'thread_comments', '1', 'yes'),
(73, 'thread_comments_depth', '5', 'yes'),
(74, 'page_comments', '', 'yes'),
(75, 'comments_per_page', '50', 'yes'),
(76, 'default_comments_page', 'newest', 'yes'),
(77, 'comment_order', 'asc', 'yes'),
(78, 'sticky_posts', 'a:0:{}', 'yes'),
(79, 'widget_categories', 'a:3:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;i:4;a:0:{}}', 'yes'),
(80, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(82, 'uninstall_plugins', 'a:2:{s:53:"anspress-question-answer/anspress-question-answer.php";s:18:"anspress_uninstall";s:33:"theme-my-login/theme-my-login.php";a:2:{i:0;s:20:"Theme_My_Login_Admin";i:1;s:9:"uninstall";}}', 'no'),
(83, 'timezone_string', '', 'yes'),
(84, 'page_for_posts', '0', 'yes'),
(85, 'page_on_front', '0', 'yes'),
(86, 'default_post_format', '0', 'yes'),
(87, 'link_manager_enabled', '0', 'yes'),
(88, 'initial_db_version', '31535', 'yes'),
(89, 'dcHyv_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:81:{s:16:"activate_plugins";b:1;s:9:"add_users";b:1;s:12:"create_users";b:1;s:19:"delete_others_pages";b:1;s:19:"delete_others_posts";b:1;s:12:"delete_pages";b:1;s:14:"delete_plugins";b:1;s:12:"delete_posts";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:13:"delete_themes";b:1;s:12:"delete_users";b:1;s:22:"dwqa_can_delete_answer";b:1;s:23:"dwqa_can_delete_comment";b:1;s:24:"dwqa_can_delete_question";b:1;s:20:"dwqa_can_edit_answer";b:1;s:21:"dwqa_can_edit_comment";b:1;s:22:"dwqa_can_edit_question";b:1;s:20:"dwqa_can_post_answer";b:1;s:21:"dwqa_can_post_comment";b:1;s:22:"dwqa_can_post_question";b:1;s:20:"dwqa_can_read_answer";b:1;s:21:"dwqa_can_read_comment";b:1;s:22:"dwqa_can_read_question";b:1;s:14:"edit_dashboard";b:1;s:10:"edit_files";b:1;s:17:"edit_others_pages";b:1;s:17:"edit_others_posts";b:1;s:10:"edit_pages";b:1;s:12:"edit_plugins";b:1;s:10:"edit_posts";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:20:"edit_published_pages";b:1;s:20:"edit_published_posts";b:1;s:18:"edit_theme_options";b:1;s:11:"edit_themes";b:1;s:10:"edit_users";b:1;s:6:"export";b:1;s:6:"import";b:1;s:15:"install_plugins";b:1;s:14:"install_themes";b:1;s:7:"level_0";b:1;s:7:"level_1";b:1;s:8:"level_10";b:1;s:7:"level_2";b:1;s:7:"level_3";b:1;s:7:"level_4";b:1;s:7:"level_5";b:1;s:7:"level_6";b:1;s:7:"level_7";b:1;s:7:"level_8";b:1;s:7:"level_9";b:1;s:10:"list_users";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:13:"promote_users";b:1;s:13:"publish_pages";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:12:"remove_users";b:1;s:13:"switch_themes";b:1;s:15:"unfiltered_html";b:1;s:17:"unfiltered_upload";b:1;s:11:"update_core";b:1;s:14:"update_plugins";b:1;s:13:"update_themes";b:1;s:12:"upload_files";b:1;s:23:"ure_create_capabilities";b:1;s:16:"ure_create_roles";b:1;s:23:"ure_delete_capabilities";b:1;s:16:"ure_delete_roles";b:1;s:14:"ure_edit_roles";b:1;s:18:"ure_manage_options";b:1;s:15:"ure_reset_roles";b:1;}}s:6:"membre";a:2:{s:4:"name";s:6:"Membre";s:12:"capabilities";a:18:{s:12:"delete_posts";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"dwqa_can_post_answer";b:1;s:21:"dwqa_can_post_comment";b:1;s:22:"dwqa_can_post_question";b:1;s:20:"dwqa_can_read_answer";b:1;s:21:"dwqa_can_read_comment";b:1;s:22:"dwqa_can_read_question";b:1;s:10:"edit_posts";b:1;s:18:"edit_private_posts";b:1;s:20:"edit_published_posts";b:1;s:7:"level_0";b:1;s:7:"level_1";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:17:"unfiltered_upload";b:1;s:12:"upload_files";b:1;}}s:10:"moderateur";a:2:{s:4:"name";s:11:"Modérateur";s:12:"capabilities";a:34:{s:9:"add_users";b:1;s:12:"create_users";b:1;s:12:"delete_pages";b:1;s:12:"delete_posts";b:1;s:12:"delete_users";b:1;s:22:"dwqa_can_delete_answer";b:1;s:23:"dwqa_can_delete_comment";b:1;s:24:"dwqa_can_delete_question";b:1;s:20:"dwqa_can_edit_answer";b:1;s:21:"dwqa_can_edit_comment";b:1;s:22:"dwqa_can_edit_question";b:1;s:20:"dwqa_can_post_answer";b:1;s:21:"dwqa_can_post_comment";b:1;s:22:"dwqa_can_post_question";b:1;s:20:"dwqa_can_read_answer";b:1;s:21:"dwqa_can_read_comment";b:1;s:22:"dwqa_can_read_question";b:1;s:17:"edit_others_posts";b:1;s:10:"edit_pages";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_pages";b:1;s:20:"edit_published_posts";b:1;s:7:"level_0";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:13:"promote_users";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:12:"remove_users";b:1;s:15:"unfiltered_html";b:1;s:17:"unfiltered_upload";b:1;s:12:"upload_files";b:1;}}s:11:"specialiste";a:2:{s:4:"name";s:12:"Spécialiste";s:12:"capabilities";a:13:{s:20:"delete_private_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"dwqa_can_post_answer";b:1;s:21:"dwqa_can_post_comment";b:1;s:22:"dwqa_can_post_question";b:1;s:20:"dwqa_can_read_answer";b:1;s:21:"dwqa_can_read_comment";b:1;s:22:"dwqa_can_read_question";b:1;s:18:"edit_private_posts";b:1;s:7:"level_0";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:12:"upload_files";b:1;}}s:7:"pending";a:2:{s:4:"name";s:7:"Pending";s:12:"capabilities";a:0:{}}}', 'yes'),
(90, 'WPLANG', 'fr_FR', 'yes'),
(91, 'widget_search', 'a:3:{i:2;a:1:{s:5:"title";s:0:"";}i:3;a:1:{s:5:"title";s:20:"Chercher sur le site";}s:12:"_multiwidget";i:1;}', 'yes'),
(92, 'widget_recent-posts', 'a:3:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}i:3;a:3:{s:5:"title";s:22:"Publications récentes";s:6:"number";i:5;s:9:"show_date";b:1;}s:12:"_multiwidget";i:1;}', 'yes'),
(93, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'sidebars_widgets', 'a:7:{s:19:"wp_inactive_widgets";a:5:{i:0;s:14:"recent-posts-2";i:1;s:17:"recent-comments-2";i:2;s:10:"archives-2";i:3;s:12:"categories-2";i:4;s:6:"meta-2";}s:9:"sidebar-1";a:2:{i:0;s:8:"search-2";i:1;s:23:"dwqa-related-question-3";}s:24:"first-footer-widget-area";a:1:{i:0;s:22:"dwqa-latest-question-3";}s:25:"second-footer-widget-area";a:1:{i:0;s:14:"recent-posts-3";}s:24:"third-footer-widget-area";a:1:{i:0;s:8:"search-3";}s:25:"fourth-footer-widget-area";a:0:{}s:13:"array_version";i:3;}', 'yes'),
(1315, '_site_transient_timeout_browser_83271fd20a6687ffcd0d1b6f7ad13cdd', '1434546760', 'yes'),
(1316, '_site_transient_browser_83271fd20a6687ffcd0d1b6f7ad13cdd', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:7:"Firefox";s:7:"version";s:4:"38.0";s:10:"update_url";s:23:"http://www.firefox.com/";s:7:"img_src";s:50:"http://s.wordpress.org/images/browsers/firefox.png";s:11:"img_src_ssl";s:49:"https://wordpress.org/images/browsers/firefox.png";s:15:"current_version";s:2:"16";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(99, 'cron', 'a:6:{i:1434031403;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1434031418;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1434032885;a:1:{s:17:"dwqa_hourly_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1434032965;a:2:{s:14:"updraft_backup";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:23:"updraft_backup_database";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1434047940;a:1:{s:20:"wp_maybe_auto_update";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}s:7:"version";i:2;}', 'yes'),
(1139, 'updraft_interval', 'daily', 'yes'),
(1140, 'updraft_interval_increments', '', 'yes'),
(1141, 'updraft_interval_database', 'daily', 'yes'),
(109, '_transient_random_seed', 'ce3bad6117a68f6664af88c53d3f2aeb', 'yes'),
(103, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:64:"http://downloads.wordpress.org/release/fr_FR/wordpress-4.2.2.zip";s:6:"locale";s:5:"fr_FR";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:64:"http://downloads.wordpress.org/release/fr_FR/wordpress-4.2.2.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.2.2";s:7:"version";s:5:"4.2.2";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1434008429;s:15:"version_checked";s:5:"4.2.2";s:12:"translations";a:0:{}}', 'yes'),
(1423, '_site_transient_timeout_theme_roots', '1434025730', 'yes'),
(1424, '_site_transient_theme_roots', 'a:5:{s:13:"base-wp-child";s:7:"/themes";s:7:"base-wp";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";s:14:"twentyfourteen";s:7:"/themes";s:14:"twentythirteen";s:7:"/themes";}', 'yes'),
(1311, 'rewrite_rules', 'a:105:{s:11:"question/?$";s:33:"index.php?post_type=dwqa-question";s:41:"question/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_type=dwqa-question&feed=$matches[1]";s:36:"question/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_type=dwqa-question&feed=$matches[1]";s:28:"question/page/([0-9]{1,})/?$";s:51:"index.php?post_type=dwqa-question&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:34:"question/.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"question/.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"question/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"question/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"question/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:27:"question/(.+?)/trackback/?$";s:40:"index.php?dwqa-question=$matches[1]&tb=1";s:47:"question/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?dwqa-question=$matches[1]&feed=$matches[2]";s:42:"question/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?dwqa-question=$matches[1]&feed=$matches[2]";s:35:"question/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?dwqa-question=$matches[1]&paged=$matches[2]";s:42:"question/(.+?)/comment-page-([0-9]{1,})/?$";s:53:"index.php?dwqa-question=$matches[1]&cpage=$matches[2]";s:27:"question/(.+?)(/[0-9]+)?/?$";s:52:"index.php?dwqa-question=$matches[1]&page=$matches[2]";s:37:"dwqa-answer/.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:47:"dwqa-answer/.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:67:"dwqa-answer/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"dwqa-answer/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"dwqa-answer/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"dwqa-answer/(.+?)/trackback/?$";s:38:"index.php?dwqa-answer=$matches[1]&tb=1";s:38:"dwqa-answer/(.+?)/page/?([0-9]{1,})/?$";s:51:"index.php?dwqa-answer=$matches[1]&paged=$matches[2]";s:45:"dwqa-answer/(.+?)/comment-page-([0-9]{1,})/?$";s:51:"index.php?dwqa-answer=$matches[1]&cpage=$matches[2]";s:30:"dwqa-answer/(.+?)(/[0-9]+)?/?$";s:50:"index.php?dwqa-answer=$matches[1]&page=$matches[2]";s:68:"questions/question-category/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:61:"index.php?dwqa-question_category=$matches[1]&feed=$matches[2]";s:63:"questions/question-category/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:61:"index.php?dwqa-question_category=$matches[1]&feed=$matches[2]";s:56:"questions/question-category/([^/]+)/page/?([0-9]{1,})/?$";s:62:"index.php?dwqa-question_category=$matches[1]&paged=$matches[2]";s:38:"questions/question-category/([^/]+)/?$";s:44:"index.php?dwqa-question_category=$matches[1]";s:63:"questions/question-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:56:"index.php?dwqa-question_tag=$matches[1]&feed=$matches[2]";s:58:"questions/question-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:56:"index.php?dwqa-question_tag=$matches[1]&feed=$matches[2]";s:51:"questions/question-tag/([^/]+)/page/?([0-9]{1,})/?$";s:57:"index.php?dwqa-question_tag=$matches[1]&paged=$matches[2]";s:33:"questions/question-tag/([^/]+)/?$";s:39:"index.php?dwqa-question_tag=$matches[1]";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:58:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:68:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:88:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:85:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1";s:77:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:65:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(/[0-9]+)?/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]";s:47:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:57:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:77:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]";s:51:"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]";s:38:"([0-9]{4})/comment-page-([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&cpage=$matches[2]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)(/[0-9]+)?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:36:"^questions/question-category/([^/]*)";s:86:"index.php?page_id=8&taxonomy=dwqa-question_category&dwqa-question_category=$matches[1]";s:31:"^questions/question-tag/([^/]*)";s:76:"index.php?page_id=8&taxonomy=dwqa-question_tag&dwqa-question_tag=$matches[1]";}', 'yes'),
(1399, '_site_transient_timeout_browser_4ad29f5f873a83bc43d5da0aa231a06b', '1434616147', 'yes'),
(1425, '_transient_is_multi_author', '1', 'yes'),
(1426, '_transient_base_categories', '2', 'yes'),
(1411, '_transient_timeout_dwqa_latest_answer_for_150', '1434034044', 'no'),
(1412, '_transient_dwqa_latest_answer_for_150', 'O:7:"WP_Post":24:{s:2:"ID";i:153;s:11:"post_author";s:1:"4";s:9:"post_date";s:19:"2015-06-10 15:29:21";s:13:"post_date_gmt";s:19:"2015-06-10 14:29:21";s:12:"post_content";s:104:"Difficile à dire cela doit dépendre des personnes, un spécialiste peu tîl vérifier mes propos svp ?";s:10:"post_title";s:82:"répondreEst-il dangereux de faire du sport quelques heures après un don de sang?";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:79:"repondreest-il-dangereux-de-faire-du-sport-quelques-heures-apres-un-don-de-sang";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2015-06-10 15:56:14";s:17:"post_modified_gmt";s:19:"2015-06-10 14:56:14";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:123:"http://127.0.0.1/docoeur/dwqa-answer/repondreest-il-dangereux-de-faire-du-sport-quelques-heures-apres-un-don-de-sang/";s:10:"menu_order";i:0;s:9:"post_type";s:11:"dwqa-answer";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}', 'no'),
(1413, '_transient_timeout_dwqa_answer_count_for_150', '1434034044', 'no'),
(1414, '_transient_dwqa_answer_count_for_150', '1', 'no'),
(1400, '_site_transient_browser_4ad29f5f873a83bc43d5da0aa231a06b', 'a:9:{s:8:"platform";s:9:"Macintosh";s:4:"name";s:6:"Chrome";s:7:"version";s:13:"43.0.2357.124";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(110, '_site_transient_timeout_browser_f02d9cb431915b4d1befb313c9060730', '1432819224', 'yes'),
(111, '_site_transient_browser_f02d9cb431915b4d1befb313c9060730', 'a:9:{s:8:"platform";s:9:"Macintosh";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"43.0.2357.65";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(180, '_site_transient_timeout_wporg_theme_feature_list', '1432653170', 'yes'),
(181, '_site_transient_wporg_theme_feature_list', 'a:4:{s:6:"Colors";a:15:{i:0;s:5:"black";i:1;s:4:"blue";i:2;s:5:"brown";i:3;s:4:"gray";i:4;s:5:"green";i:5;s:6:"orange";i:6;s:4:"pink";i:7;s:6:"purple";i:8;s:3:"red";i:9;s:6:"silver";i:10;s:3:"tan";i:11;s:5:"white";i:12;s:6:"yellow";i:13;s:4:"dark";i:14;s:5:"light";}s:6:"Layout";a:9:{i:0;s:12:"fixed-layout";i:1;s:12:"fluid-layout";i:2;s:17:"responsive-layout";i:3;s:10:"one-column";i:4;s:11:"two-columns";i:5;s:13:"three-columns";i:6;s:12:"four-columns";i:7;s:12:"left-sidebar";i:8;s:13:"right-sidebar";}s:8:"Features";a:20:{i:0;s:19:"accessibility-ready";i:1;s:8:"blavatar";i:2;s:10:"buddypress";i:3;s:17:"custom-background";i:4;s:13:"custom-colors";i:5;s:13:"custom-header";i:6;s:11:"custom-menu";i:7;s:12:"editor-style";i:8;s:21:"featured-image-header";i:9;s:15:"featured-images";i:10;s:15:"flexible-header";i:11;s:20:"front-page-post-form";i:12;s:19:"full-width-template";i:13;s:12:"microformats";i:14;s:12:"post-formats";i:15;s:20:"rtl-language-support";i:16;s:11:"sticky-post";i:17;s:13:"theme-options";i:18;s:17:"threaded-comments";i:19;s:17:"translation-ready";}s:7:"Subject";a:3:{i:0;s:7:"holiday";i:1;s:13:"photoblogging";i:2;s:8:"seasonal";}}', 'yes'),
(117, 'can_compress_scripts', '1', 'yes'),
(1102, '_transient_dwqa_latest_answer_for_128', 'O:7:"WP_Post":24:{s:2:"ID";i:129;s:11:"post_author";s:1:"4";s:9:"post_date";s:19:"2015-06-09 08:13:04";s:13:"post_date_gmt";s:19:"2015-06-09 07:13:04";s:12:"post_content";s:3:"Oui";s:10:"post_title";s:22:"répondreQuestion test";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:21:"repondrequestion-test";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2015-06-09 08:13:04";s:17:"post_modified_gmt";s:19:"2015-06-09 07:13:04";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:57:"http://127.0.0.1/wp_v2/dwqa-answer/repondrequestion-test/";s:10:"menu_order";i:0;s:9:"post_type";s:11:"dwqa-answer";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}', 'no'),
(1406, '_transient_feed_mod_96281909e104f3c547a3bba0b6d36ad5', '1434011348', 'no'),
(1407, '_transient_timeout_dash_4077549d03da2e451c8b5f002294ff51', '1434054549', 'no'),
(1408, '_transient_dash_4077549d03da2e451c8b5f002294ff51', '<div class="rss-widget"><ul><li><a class=''rsswidget'' href=''http://feedproxy.google.com/~r/WordpressFrancophone/~3/XyIbHAGFLdw/''>L’Hebdo WordPress n°260 : WooThemes – Slack – Insights</a> <span class="rss-date">3 juin 2015</span><div class="rssSummary">Automattic acquiert WooThemes L’info de cette dernière quinzaine est sans aucun doute le rachat (en) de WooThemes (en) par Automattic (en). Le plus gros marchand de thèmes et le créateur du plus célèbre outil de e-commerce pour WordPress rejoint donc le giron de l’outil le plus puissant du web. 12e anniversaire de WordPress Que le [&hellip;]</div></li></ul></div><div class="rss-widget"><ul><li><a class=''rsswidget'' href=''http://feedproxy.google.com/~r/feedburner/cjgL/~3/siKfBLAVPrQ/''>L’Hebdo WordPress n°260 : WooThemes – Slack – Insights</a></li><li><a class=''rsswidget'' href=''http://feedproxy.google.com/~r/feedburner/cjgL/~3/O5_p3VM9M2Q/''>L’Hebdo WordPress n°259 : Des événements WordPress – WordPress 4.3 – BuddyPress</a></li><li><a class=''rsswidget'' href=''http://feedproxy.google.com/~r/feedburner/cjgL/~3/FiV0asGKK6M/''>L’Hebdo WordPress n°258 : WordPress 4.3 – WordCamps &amp; Evénements WP</a></li></ul></div><div class="rss-widget"><ul></ul></div>', 'no'),
(1004, '_transient_timeout_plugin_slugs', '1434020939', 'no'),
(1005, '_transient_plugin_slugs', 'a:9:{i:0;s:19:"akismet/akismet.php";i:1;s:41:"dw-question-answer/dw-question-answer.php";i:2;s:33:"nav-menu-roles/nav-menu-roles.php";i:3;s:33:"theme-my-login/theme-my-login.php";i:4;s:27:"updraftplus/updraftplus.php";i:5;s:37:"user-role-editor/user-role-editor.php";i:6;s:53:"wp-roles-at-registration/wp-roles-at-registration.php";i:7;s:25:"wp-user-frontend/wpuf.php";i:8;s:24:"wp-users-media/index.php";}', 'no'),
(1403, '_transient_feed_mod_66a70e9599b658d5cc038e8074597e7c', '1434011348', 'no'),
(1401, '_transient_timeout_feed_66a70e9599b658d5cc038e8074597e7c', '1434054548', 'no'),
(1402, '_transient_timeout_feed_mod_66a70e9599b658d5cc038e8074597e7c', '1434054548', 'no'),
(1404, '_transient_timeout_feed_96281909e104f3c547a3bba0b6d36ad5', '1434054548', 'no'),
(1405, '_transient_timeout_feed_mod_96281909e104f3c547a3bba0b6d36ad5', '1434054548', 'no'),
(845, '_site_transient_timeout_available_translations', '1433418749', 'yes');
INSERT INTO `dcHyv_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(846, '_site_transient_available_translations', 'a:56:{s:2:"ar";a:8:{s:8:"language";s:2:"ar";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 06:57:37";s:12:"english_name";s:6:"Arabic";s:11:"native_name";s:14:"العربية";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/ar.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:2;s:3:"ara";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:2:"az";a:8:{s:8:"language";s:2:"az";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-01 14:30:22";s:12:"english_name";s:11:"Azerbaijani";s:11:"native_name";s:16:"Azərbaycan dili";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/az.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:2;s:3:"aze";}s:7:"strings";a:1:{s:8:"continue";s:5:"Davam";}}s:5:"bg_BG";a:8:{s:8:"language";s:5:"bg_BG";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-27 06:36:25";s:12:"english_name";s:9:"Bulgarian";s:11:"native_name";s:18:"Български";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/bg_BG.zip";s:3:"iso";a:2:{i:1;s:2:"bg";i:2;s:3:"bul";}s:7:"strings";a:1:{s:8:"continue";s:22:"Продължение";}}s:5:"bs_BA";a:8:{s:8:"language";s:5:"bs_BA";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-25 18:55:51";s:12:"english_name";s:7:"Bosnian";s:11:"native_name";s:8:"Bosanski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/bs_BA.zip";s:3:"iso";a:2:{i:1;s:2:"bs";i:2;s:3:"bos";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:2:"ca";a:8:{s:8:"language";s:2:"ca";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-24 05:23:15";s:12:"english_name";s:7:"Catalan";s:11:"native_name";s:7:"Català";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/ca.zip";s:3:"iso";a:2:{i:1;s:2:"ca";i:2;s:3:"cat";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"cy";a:8:{s:8:"language";s:2:"cy";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-30 08:59:10";s:12:"english_name";s:5:"Welsh";s:11:"native_name";s:7:"Cymraeg";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/cy.zip";s:3:"iso";a:2:{i:1;s:2:"cy";i:2;s:3:"cym";}s:7:"strings";a:1:{s:8:"continue";s:6:"Parhau";}}s:5:"da_DK";a:8:{s:8:"language";s:5:"da_DK";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-03 00:26:43";s:12:"english_name";s:6:"Danish";s:11:"native_name";s:5:"Dansk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/da_DK.zip";s:3:"iso";a:2:{i:1;s:2:"da";i:2;s:3:"dan";}s:7:"strings";a:1:{s:8:"continue";s:12:"Forts&#230;t";}}s:5:"de_CH";a:8:{s:8:"language";s:5:"de_CH";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:20:"German (Switzerland)";s:11:"native_name";s:17:"Deutsch (Schweiz)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/de_CH.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:10:"Fortfahren";}}s:5:"de_DE";a:8:{s:8:"language";s:5:"de_DE";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-28 23:38:57";s:12:"english_name";s:6:"German";s:11:"native_name";s:7:"Deutsch";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/de_DE.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:10:"Fortfahren";}}s:2:"el";a:8:{s:8:"language";s:2:"el";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-06-01 09:29:51";s:12:"english_name";s:5:"Greek";s:11:"native_name";s:16:"Ελληνικά";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/el.zip";s:3:"iso";a:2:{i:1;s:2:"el";i:2;s:3:"ell";}s:7:"strings";a:1:{s:8:"continue";s:16:"Συνέχεια";}}s:5:"en_CA";a:8:{s:8:"language";s:5:"en_CA";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:16:"English (Canada)";s:11:"native_name";s:16:"English (Canada)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/en_CA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_GB";a:8:{s:8:"language";s:5:"en_GB";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:12:"English (UK)";s:11:"native_name";s:12:"English (UK)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/en_GB.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_AU";a:8:{s:8:"language";s:5:"en_AU";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:19:"English (Australia)";s:11:"native_name";s:19:"English (Australia)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/en_AU.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:2:"eo";a:8:{s:8:"language";s:2:"eo";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:9:"Esperanto";s:11:"native_name";s:9:"Esperanto";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/eo.zip";s:3:"iso";a:2:{i:1;s:2:"eo";i:2;s:3:"epo";}s:7:"strings";a:1:{s:8:"continue";s:8:"Daŭrigi";}}s:5:"es_ES";a:8:{s:8:"language";s:5:"es_ES";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-18 10:50:06";s:12:"english_name";s:15:"Spanish (Spain)";s:11:"native_name";s:8:"Español";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/es_ES.zip";s:3:"iso";a:1:{i:1;s:2:"es";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_PE";a:8:{s:8:"language";s:5:"es_PE";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-25 13:39:01";s:12:"english_name";s:14:"Spanish (Peru)";s:11:"native_name";s:17:"Español de Perú";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/es_PE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_MX";a:8:{s:8:"language";s:5:"es_MX";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-29 17:53:27";s:12:"english_name";s:16:"Spanish (Mexico)";s:11:"native_name";s:19:"Español de México";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/es_MX.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CL";a:8:{s:8:"language";s:5:"es_CL";s:7:"version";s:3:"4.0";s:7:"updated";s:19:"2014-09-04 19:47:01";s:12:"english_name";s:15:"Spanish (Chile)";s:11:"native_name";s:17:"Español de Chile";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.0/es_CL.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"eu";a:8:{s:8:"language";s:2:"eu";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:6:"Basque";s:11:"native_name";s:7:"Euskara";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/eu.zip";s:3:"iso";a:2:{i:1;s:2:"eu";i:2;s:3:"eus";}s:7:"strings";a:1:{s:8:"continue";s:8:"Jarraitu";}}s:5:"fa_IR";a:8:{s:8:"language";s:5:"fa_IR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-16 10:01:41";s:12:"english_name";s:7:"Persian";s:11:"native_name";s:10:"فارسی";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/fa_IR.zip";s:3:"iso";a:2:{i:1;s:2:"fa";i:2;s:3:"fas";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:2:"fi";a:8:{s:8:"language";s:2:"fi";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-15 10:49:37";s:12:"english_name";s:7:"Finnish";s:11:"native_name";s:5:"Suomi";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/fi.zip";s:3:"iso";a:2:{i:1;s:2:"fi";i:2;s:3:"fin";}s:7:"strings";a:1:{s:8:"continue";s:5:"Jatka";}}s:5:"fr_FR";a:8:{s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-29 17:08:38";s:12:"english_name";s:15:"French (France)";s:11:"native_name";s:9:"Français";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/fr_FR.zip";s:3:"iso";a:1:{i:1;s:2:"fr";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:2:"gd";a:8:{s:8:"language";s:2:"gd";s:7:"version";s:3:"4.0";s:7:"updated";s:19:"2014-09-05 17:37:43";s:12:"english_name";s:15:"Scottish Gaelic";s:11:"native_name";s:9:"Gàidhlig";s:7:"package";s:59:"https://downloads.wordpress.org/translation/core/4.0/gd.zip";s:3:"iso";a:3:{i:1;s:2:"gd";i:2;s:3:"gla";i:3;s:3:"gla";}s:7:"strings";a:1:{s:8:"continue";s:15:"Lean air adhart";}}s:5:"gl_ES";a:8:{s:8:"language";s:5:"gl_ES";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:8:"Galician";s:11:"native_name";s:6:"Galego";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/gl_ES.zip";s:3:"iso";a:2:{i:1;s:2:"gl";i:2;s:3:"glg";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:3:"haz";a:8:{s:8:"language";s:3:"haz";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-26 15:20:27";s:12:"english_name";s:8:"Hazaragi";s:11:"native_name";s:15:"هزاره گی";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.1.5/haz.zip";s:3:"iso";a:1:{i:2;s:3:"haz";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:5:"he_IL";a:8:{s:8:"language";s:5:"he_IL";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 19:32:58";s:12:"english_name";s:6:"Hebrew";s:11:"native_name";s:16:"עִבְרִית";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/he_IL.zip";s:3:"iso";a:1:{i:1;s:2:"he";}s:7:"strings";a:1:{s:8:"continue";s:12:"להמשיך";}}s:2:"hr";a:8:{s:8:"language";s:2:"hr";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-27 08:22:08";s:12:"english_name";s:8:"Croatian";s:11:"native_name";s:8:"Hrvatski";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/hr.zip";s:3:"iso";a:2:{i:1;s:2:"hr";i:2;s:3:"hrv";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:5:"hu_HU";a:8:{s:8:"language";s:5:"hu_HU";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 06:43:50";s:12:"english_name";s:9:"Hungarian";s:11:"native_name";s:6:"Magyar";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/hu_HU.zip";s:3:"iso";a:2:{i:1;s:2:"hu";i:2;s:3:"hun";}s:7:"strings";a:1:{s:8:"continue";s:7:"Tovább";}}s:5:"id_ID";a:8:{s:8:"language";s:5:"id_ID";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 07:07:32";s:12:"english_name";s:10:"Indonesian";s:11:"native_name";s:16:"Bahasa Indonesia";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/id_ID.zip";s:3:"iso";a:2:{i:1;s:2:"id";i:2;s:3:"ind";}s:7:"strings";a:1:{s:8:"continue";s:9:"Lanjutkan";}}s:5:"is_IS";a:8:{s:8:"language";s:5:"is_IS";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-27 11:14:20";s:12:"english_name";s:9:"Icelandic";s:11:"native_name";s:9:"Íslenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/is_IS.zip";s:3:"iso";a:2:{i:1;s:2:"is";i:2;s:3:"isl";}s:7:"strings";a:1:{s:8:"continue";s:6:"Áfram";}}s:5:"it_IT";a:8:{s:8:"language";s:5:"it_IT";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-31 19:34:18";s:12:"english_name";s:7:"Italian";s:11:"native_name";s:8:"Italiano";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/it_IT.zip";s:3:"iso";a:2:{i:1;s:2:"it";i:2;s:3:"ita";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"ja";a:8:{s:8:"language";s:2:"ja";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-15 08:57:03";s:12:"english_name";s:8:"Japanese";s:11:"native_name";s:9:"日本語";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/ja.zip";s:3:"iso";a:1:{i:1;s:2:"ja";}s:7:"strings";a:1:{s:8:"continue";s:9:"続ける";}}s:5:"ko_KR";a:8:{s:8:"language";s:5:"ko_KR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 06:57:22";s:12:"english_name";s:6:"Korean";s:11:"native_name";s:9:"한국어";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/ko_KR.zip";s:3:"iso";a:2:{i:1;s:2:"ko";i:2;s:3:"kor";}s:7:"strings";a:1:{s:8:"continue";s:6:"계속";}}s:5:"lt_LT";a:8:{s:8:"language";s:5:"lt_LT";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:10:"Lithuanian";s:11:"native_name";s:15:"Lietuvių kalba";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/lt_LT.zip";s:3:"iso";a:2:{i:1;s:2:"lt";i:2;s:3:"lit";}s:7:"strings";a:1:{s:8:"continue";s:6:"Tęsti";}}s:5:"my_MM";a:8:{s:8:"language";s:5:"my_MM";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-26 15:57:42";s:12:"english_name";s:17:"Myanmar (Burmese)";s:11:"native_name";s:15:"ဗမာစာ";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.1.5/my_MM.zip";s:3:"iso";a:2:{i:1;s:2:"my";i:2;s:3:"mya";}s:7:"strings";a:1:{s:8:"continue";s:54:"ဆက်လက်လုပ်ေဆာင်ပါ။";}}s:5:"nb_NO";a:8:{s:8:"language";s:5:"nb_NO";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-27 10:29:43";s:12:"english_name";s:19:"Norwegian (Bokmål)";s:11:"native_name";s:13:"Norsk bokmål";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/nb_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nb";i:2;s:3:"nob";}s:7:"strings";a:1:{s:8:"continue";s:8:"Fortsett";}}s:5:"nl_NL";a:8:{s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 06:59:29";s:12:"english_name";s:5:"Dutch";s:11:"native_name";s:10:"Nederlands";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/nl_NL.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nn_NO";a:8:{s:8:"language";s:5:"nn_NO";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:19:"Norwegian (Nynorsk)";s:11:"native_name";s:13:"Norsk nynorsk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/nn_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nn";i:2;s:3:"nno";}s:7:"strings";a:1:{s:8:"continue";s:9:"Hald fram";}}s:3:"oci";a:8:{s:8:"language";s:3:"oci";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-25 19:53:10";s:12:"english_name";s:7:"Occitan";s:11:"native_name";s:7:"Occitan";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.2.2/oci.zip";s:3:"iso";a:2:{i:1;s:2:"oc";i:2;s:3:"oci";}s:7:"strings";a:1:{s:8:"continue";s:9:"Contunhar";}}s:5:"pl_PL";a:8:{s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-09 10:15:05";s:12:"english_name";s:6:"Polish";s:11:"native_name";s:6:"Polski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/pl_PL.zip";s:3:"iso";a:2:{i:1;s:2:"pl";i:2;s:3:"pol";}s:7:"strings";a:1:{s:8:"continue";s:9:"Kontynuuj";}}s:2:"ps";a:8:{s:8:"language";s:2:"ps";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-29 22:19:48";s:12:"english_name";s:6:"Pashto";s:11:"native_name";s:8:"پښتو";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.1.5/ps.zip";s:3:"iso";a:1:{i:1;s:2:"ps";}s:7:"strings";a:1:{s:8:"continue";s:8:"دوام";}}s:5:"pt_PT";a:8:{s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-27 09:25:14";s:12:"english_name";s:21:"Portuguese (Portugal)";s:11:"native_name";s:10:"Português";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/pt_PT.zip";s:3:"iso";a:1:{i:1;s:2:"pt";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"pt_BR";a:8:{s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 06:44:19";s:12:"english_name";s:19:"Portuguese (Brazil)";s:11:"native_name";s:20:"Português do Brasil";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/pt_BR.zip";s:3:"iso";a:2:{i:1;s:2:"pt";i:2;s:3:"por";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"ro_RO";a:8:{s:8:"language";s:5:"ro_RO";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-21 15:14:01";s:12:"english_name";s:8:"Romanian";s:11:"native_name";s:8:"Română";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/ro_RO.zip";s:3:"iso";a:2:{i:1;s:2:"ro";i:2;s:3:"ron";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuă";}}s:5:"ru_RU";a:8:{s:8:"language";s:5:"ru_RU";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-31 11:58:44";s:12:"english_name";s:7:"Russian";s:11:"native_name";s:14:"Русский";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/ru_RU.zip";s:3:"iso";a:2:{i:1;s:2:"ru";i:2;s:3:"rus";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продолжить";}}s:5:"sk_SK";a:8:{s:8:"language";s:5:"sk_SK";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 09:29:23";s:12:"english_name";s:6:"Slovak";s:11:"native_name";s:11:"Slovenčina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/sk_SK.zip";s:3:"iso";a:2:{i:1;s:2:"sk";i:2;s:3:"slk";}s:7:"strings";a:1:{s:8:"continue";s:12:"Pokračovať";}}s:5:"sl_SI";a:8:{s:8:"language";s:5:"sl_SI";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-26 16:25:46";s:12:"english_name";s:9:"Slovenian";s:11:"native_name";s:13:"Slovenščina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.1.5/sl_SI.zip";s:3:"iso";a:2:{i:1;s:2:"sl";i:2;s:3:"slv";}s:7:"strings";a:1:{s:8:"continue";s:10:"Nadaljujte";}}s:2:"sq";a:8:{s:8:"language";s:2:"sq";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-29 08:27:12";s:12:"english_name";s:8:"Albanian";s:11:"native_name";s:5:"Shqip";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/sq.zip";s:3:"iso";a:2:{i:1;s:2:"sq";i:2;s:3:"sqi";}s:7:"strings";a:1:{s:8:"continue";s:6:"Vazhdo";}}s:5:"sr_RS";a:8:{s:8:"language";s:5:"sr_RS";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 16:55:54";s:12:"english_name";s:7:"Serbian";s:11:"native_name";s:23:"Српски језик";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/sr_RS.zip";s:3:"iso";a:2:{i:1;s:2:"sr";i:2;s:3:"srp";}s:7:"strings";a:1:{s:8:"continue";s:14:"Настави";}}s:5:"sv_SE";a:8:{s:8:"language";s:5:"sv_SE";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 07:08:28";s:12:"english_name";s:7:"Swedish";s:11:"native_name";s:7:"Svenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/sv_SE.zip";s:3:"iso";a:2:{i:1;s:2:"sv";i:2;s:3:"swe";}s:7:"strings";a:1:{s:8:"continue";s:9:"Fortsätt";}}s:2:"th";a:8:{s:8:"language";s:2:"th";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 15:16:26";s:12:"english_name";s:4:"Thai";s:11:"native_name";s:9:"ไทย";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/th.zip";s:3:"iso";a:2:{i:1;s:2:"th";i:2;s:3:"tha";}s:7:"strings";a:1:{s:8:"continue";s:15:"ต่อไป";}}s:5:"tr_TR";a:8:{s:8:"language";s:5:"tr_TR";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-26 07:01:28";s:12:"english_name";s:7:"Turkish";s:11:"native_name";s:8:"Türkçe";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/tr_TR.zip";s:3:"iso";a:2:{i:1;s:2:"tr";i:2;s:3:"tur";}s:7:"strings";a:1:{s:8:"continue";s:5:"Devam";}}s:5:"ug_CN";a:8:{s:8:"language";s:5:"ug_CN";s:7:"version";s:5:"4.1.5";s:7:"updated";s:19:"2015-03-26 16:45:38";s:12:"english_name";s:6:"Uighur";s:11:"native_name";s:9:"Uyƣurqə";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.1.5/ug_CN.zip";s:3:"iso";a:2:{i:1;s:2:"ug";i:2;s:3:"uig";}s:7:"strings";a:1:{s:8:"continue";s:26:"داۋاملاشتۇرۇش";}}s:2:"uk";a:8:{s:8:"language";s:2:"uk";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-05-28 13:43:48";s:12:"english_name";s:9:"Ukrainian";s:11:"native_name";s:20:"Українська";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.2/uk.zip";s:3:"iso";a:2:{i:1;s:2:"uk";i:2;s:3:"ukr";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продовжити";}}s:5:"zh_TW";a:8:{s:8:"language";s:5:"zh_TW";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-29 06:37:03";s:12:"english_name";s:16:"Chinese (Taiwan)";s:11:"native_name";s:12:"繁體中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/zh_TW.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"繼續";}}s:5:"zh_CN";a:8:{s:8:"language";s:5:"zh_CN";s:7:"version";s:5:"4.2.2";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:15:"Chinese (China)";s:11:"native_name";s:12:"简体中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.2/zh_CN.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"继续";}}}', 'yes'),
(182, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1434024876;s:7:"checked";a:5:{s:13:"base-wp-child";s:5:"1.0.0";s:7:"base-wp";s:4:"1.29";s:13:"twentyfifteen";s:3:"1.2";s:14:"twentyfourteen";s:3:"1.4";s:14:"twentythirteen";s:3:"1.5";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'yes'),
(1023, '_transient_timeout_dwqa_answer_count_for_127', '1433768442', 'no'),
(162, 'recently_activated', 'a:2:{s:30:"db-prefix-change/db_prefix.php";i:1433886870;s:19:"akismet/akismet.php";i:1433755979;}', 'yes'),
(1059, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1433779527', 'yes'),
(1060, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'a:40:{s:6:"widget";a:3:{s:4:"name";s:6:"widget";s:4:"slug";s:6:"widget";s:5:"count";s:4:"5223";}s:4:"post";a:3:{s:4:"name";s:4:"Post";s:4:"slug";s:4:"post";s:5:"count";s:4:"3269";}s:6:"plugin";a:3:{s:4:"name";s:6:"plugin";s:4:"slug";s:6:"plugin";s:5:"count";s:4:"3204";}s:5:"admin";a:3:{s:4:"name";s:5:"admin";s:4:"slug";s:5:"admin";s:5:"count";s:4:"2734";}s:5:"posts";a:3:{s:4:"name";s:5:"posts";s:4:"slug";s:5:"posts";s:5:"count";s:4:"2503";}s:7:"sidebar";a:3:{s:4:"name";s:7:"sidebar";s:4:"slug";s:7:"sidebar";s:5:"count";s:4:"2001";}s:9:"shortcode";a:3:{s:4:"name";s:9:"shortcode";s:4:"slug";s:9:"shortcode";s:5:"count";s:4:"1906";}s:6:"google";a:3:{s:4:"name";s:6:"google";s:4:"slug";s:6:"google";s:5:"count";s:4:"1836";}s:7:"twitter";a:3:{s:4:"name";s:7:"twitter";s:4:"slug";s:7:"twitter";s:5:"count";s:4:"1787";}s:6:"images";a:3:{s:4:"name";s:6:"images";s:4:"slug";s:6:"images";s:5:"count";s:4:"1769";}s:4:"page";a:3:{s:4:"name";s:4:"page";s:4:"slug";s:4:"page";s:5:"count";s:4:"1738";}s:8:"comments";a:3:{s:4:"name";s:8:"comments";s:4:"slug";s:8:"comments";s:5:"count";s:4:"1728";}s:5:"image";a:3:{s:4:"name";s:5:"image";s:4:"slug";s:5:"image";s:5:"count";s:4:"1621";}s:8:"facebook";a:3:{s:4:"name";s:8:"Facebook";s:4:"slug";s:8:"facebook";s:5:"count";s:4:"1419";}s:3:"seo";a:3:{s:4:"name";s:3:"seo";s:4:"slug";s:3:"seo";s:5:"count";s:4:"1357";}s:9:"wordpress";a:3:{s:4:"name";s:9:"wordpress";s:4:"slug";s:9:"wordpress";s:5:"count";s:4:"1299";}s:5:"links";a:3:{s:4:"name";s:5:"links";s:4:"slug";s:5:"links";s:5:"count";s:4:"1207";}s:6:"social";a:3:{s:4:"name";s:6:"social";s:4:"slug";s:6:"social";s:5:"count";s:4:"1165";}s:7:"gallery";a:3:{s:4:"name";s:7:"gallery";s:4:"slug";s:7:"gallery";s:5:"count";s:4:"1150";}s:5:"email";a:3:{s:4:"name";s:5:"email";s:4:"slug";s:5:"email";s:5:"count";s:4:"1021";}s:7:"widgets";a:3:{s:4:"name";s:7:"widgets";s:4:"slug";s:7:"widgets";s:5:"count";s:3:"975";}s:11:"woocommerce";a:3:{s:4:"name";s:11:"woocommerce";s:4:"slug";s:11:"woocommerce";s:5:"count";s:3:"942";}s:5:"pages";a:3:{s:4:"name";s:5:"pages";s:4:"slug";s:5:"pages";s:5:"count";s:3:"932";}s:6:"jquery";a:3:{s:4:"name";s:6:"jquery";s:4:"slug";s:6:"jquery";s:5:"count";s:3:"896";}s:3:"rss";a:3:{s:4:"name";s:3:"rss";s:4:"slug";s:3:"rss";s:5:"count";s:3:"865";}s:5:"media";a:3:{s:4:"name";s:5:"media";s:4:"slug";s:5:"media";s:5:"count";s:3:"853";}s:5:"video";a:3:{s:4:"name";s:5:"video";s:4:"slug";s:5:"video";s:5:"count";s:3:"806";}s:4:"ajax";a:3:{s:4:"name";s:4:"AJAX";s:4:"slug";s:4:"ajax";s:5:"count";s:3:"791";}s:7:"content";a:3:{s:4:"name";s:7:"content";s:4:"slug";s:7:"content";s:5:"count";s:3:"767";}s:5:"login";a:3:{s:4:"name";s:5:"login";s:4:"slug";s:5:"login";s:5:"count";s:3:"743";}s:9:"ecommerce";a:3:{s:4:"name";s:9:"ecommerce";s:4:"slug";s:9:"ecommerce";s:5:"count";s:3:"738";}s:10:"javascript";a:3:{s:4:"name";s:10:"javascript";s:4:"slug";s:10:"javascript";s:5:"count";s:3:"736";}s:10:"buddypress";a:3:{s:4:"name";s:10:"buddypress";s:4:"slug";s:10:"buddypress";s:5:"count";s:3:"695";}s:5:"photo";a:3:{s:4:"name";s:5:"photo";s:4:"slug";s:5:"photo";s:5:"count";s:3:"687";}s:4:"feed";a:3:{s:4:"name";s:4:"feed";s:4:"slug";s:4:"feed";s:5:"count";s:3:"682";}s:4:"link";a:3:{s:4:"name";s:4:"link";s:4:"slug";s:4:"link";s:5:"count";s:3:"669";}s:7:"youtube";a:3:{s:4:"name";s:7:"youtube";s:4:"slug";s:7:"youtube";s:5:"count";s:3:"649";}s:8:"security";a:3:{s:4:"name";s:8:"security";s:4:"slug";s:8:"security";s:5:"count";s:3:"645";}s:4:"spam";a:3:{s:4:"name";s:4:"spam";s:4:"slug";s:4:"spam";s:5:"count";s:3:"640";}s:6:"photos";a:3:{s:4:"name";s:6:"photos";s:4:"slug";s:6:"photos";s:5:"count";s:3:"639";}}', 'yes'),
(183, 'theme_mods_twentyfifteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1432642421;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(172, 'user_role_editor', 'a:7:{s:11:"ure_version";s:6:"4.18.4";s:17:"ure_caps_readable";i:0;s:24:"ure_show_deprecated_caps";i:0;s:19:"ure_hide_pro_banner";i:0;s:19:"other_default_roles";a:0:{}s:15:"show_admin_role";s:1:"1";s:14:"edit_user_caps";s:1:"1";}', 'yes'),
(173, 'wp_backup_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:69:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:9:"add_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:14:"ure_edit_roles";b:1;s:16:"ure_create_roles";b:1;s:16:"ure_delete_roles";b:1;s:23:"ure_create_capabilities";b:1;s:23:"ure_delete_capabilities";b:1;s:18:"ure_manage_options";b:1;s:15:"ure_reset_roles";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'no'),
(1379, '_site_transient_timeout_ure_caps_readable', '1433949159', 'yes'),
(1380, '_site_transient_ure_caps_readable', '0', 'yes'),
(203, 'wp_rar_roles', 's:46:"a:2:{i:0;s:11:"specialiste";i:1;s:6:"membre";}";', 'yes'),
(204, 'wp_rar_role_label', 'Rôle', 'yes'),
(1288, '_site_transient_timeout_browser_34dbda094a85b9be15d4598da322883d', '1434534388', 'yes'),
(1289, '_site_transient_browser_34dbda094a85b9be15d4598da322883d', 'a:9:{s:8:"platform";s:9:"Macintosh";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"43.0.2357.81";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(184, 'current_theme', 'base-wp', 'yes'),
(185, 'theme_mods_base-wp', 'a:2:{i:0;b:0;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1432645771;s:4:"data";a:6:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:24:"first-footer-widget-area";N;s:25:"second-footer-widget-area";N;s:24:"third-footer-widget-area";N;s:25:"fourth-footer-widget-area";N;}}}', 'yes'),
(186, 'theme_switched', '', 'yes'),
(230, 'anspress_opt', 'a:67:{s:17:"show_login_signup";b:1;s:10:"show_login";b:1;s:11:"show_signup";b:1;s:22:"show_title_in_question";b:0;s:17:"show_social_login";b:0;s:5:"theme";s:7:"default";s:14:"author_credits";b:0;s:14:"clear_database";b:0;s:21:"minimum_qtitle_length";i:10;s:23:"minimum_question_length";i:10;s:16:"multiple_answers";b:0;s:21:"disallow_op_to_answer";b:0;s:18:"minimum_ans_length";i:5;s:21:"avatar_size_qquestion";i:50;s:18:"allow_private_post";b:1;s:19:"avatar_size_qanswer";i:50;s:20:"avatar_size_qcomment";i:25;s:16:"avatar_size_list";i:45;s:17:"question_per_page";s:2:"20";s:16:"answers_per_page";s:1:"5";s:12:"answers_sort";s:6:"active";s:14:"close_selected";b:1;s:21:"moderate_new_question";s:6:"no_mod";s:18:"mod_question_point";i:10;s:19:"categories_per_page";i:20;s:15:"question_prefix";s:8:"question";s:17:"min_point_new_tag";i:100;s:15:"allow_anonymous";b:0;s:21:"only_admin_can_answer";b:0;s:21:"logged_in_can_see_ans";b:0;s:25:"logged_in_can_see_comment";b:0;s:24:"show_comments_by_default";b:0;s:20:"question_text_editor";b:0;s:18:"answer_text_editor";b:0;s:15:"base_page_title";s:9:"Questions";s:14:"ask_page_title";s:12:"Ask question";s:17:"search_page_title";s:11:"Search "%s"";s:28:"disable_comments_on_question";b:0;s:26:"disable_comments_on_answer";b:0;s:19:"new_question_status";s:7:"publish";s:17:"new_answer_status";s:7:"publish";s:20:"edit_question_status";s:7:"publish";s:18:"edit_answer_status";s:7:"publish";s:20:"disable_delete_after";i:86400;s:10:"db_cleanup";b:0;s:26:"disable_voting_on_question";b:0;s:24:"disable_voting_on_answer";b:0;s:16:"enable_recaptcha";b:0;s:18:"recaptcha_site_key";s:0:"";s:20:"recaptcha_secret_key";s:0:"";s:18:"disable_reputation";b:0;s:22:"users_page_avatar_size";i:80;s:14:"users_per_page";i:20;s:22:"enable_users_directory";b:1;s:25:"question_permalink_follow";b:1;s:21:"show_question_sidebar";b:1;s:18:"allow_upload_image";b:1;s:18:"question_help_page";s:0:"";s:16:"answer_help_page";s:0:"";s:18:"disable_answer_nav";b:0;s:14:"image_per_post";i:3;s:9:"base_page";i:6;s:12:"base_page_id";s:14:"anspress_title";s:12:"ap_installed";s:5:"false";s:10:"ap_version";s:7:"2.2.0.1";s:13:"ap_db_version";s:2:"12";s:8:"ap_flush";s:5:"false";}', 'yes'),
(188, 'widget_calendar', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(189, 'widget_nav_menu', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(190, 'widget_tag_cloud', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(191, 'widget_pages', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(196, 'theme_mods_base-wp-child', 'a:5:{i:0;b:0;s:18:"nav_menu_locations";a:1:{s:7:"primary";i:3;}s:12:"header_image";s:73:"http://127.0.0.1/docoeur/wp-content/uploads/2015/06/cropped-Banniere2.jpg";s:17:"header_image_data";O:8:"stdClass":5:{s:13:"attachment_id";i:131;s:3:"url";s:71:"http://127.0.0.1/wp_v2/wp-content/uploads/2015/06/cropped-Banniere2.jpg";s:13:"thumbnail_url";s:71:"http://127.0.0.1/wp_v2/wp-content/uploads/2015/06/cropped-Banniere2.jpg";s:6:"height";i:150;s:5:"width";i:1500;}s:16:"header_textcolor";s:6:"555555";}', 'yes'),
(249, 'dwqa_subscrible_sendto_address', '', 'yes'),
(250, 'dwqa_subscrible_cc_address', '', 'yes'),
(251, 'dwqa_subscrible_bcc_address', '', 'yes'),
(252, 'dwqa_subscrible_from_address', '', 'yes'),
(253, 'dwqa_subscrible_send_copy_to_admin', '', 'yes'),
(254, 'dwqa_subscrible_email_logo', '', 'yes'),
(255, 'dwqa_subscrible_new_question_email', '<div style="background: #f5f5f5; color: #333; font-size: 14px; line-height: 20px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">\r\n<div style="width: 600px; margin: 0 auto;">\r\n<div style="border-bottom: 1px solid #ddd; padding: 20px 0; text-align: center;">{site_logo}</div>\r\n<div style="padding: 10px 0 40px;">\r\n<p>A new question was posted on {site_name},</p>\r\n<table width="100%" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td class="user_avatar" valign="top" width="80">\r\n<div style="width: 60px; height: 60px; background: #ddd; text-align: center; word-wrap: break-word;">{user_avatar}</div>\r\n</td>\r\n<td valign="top">\r\n<p style="font-size: 14px; line-height: 20px; color: #333; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><a style="text-decoration: none; color: #f7682c;" href="{user_link}">{username}</a> has asked a question "<a style="text-decoration: none; color: #f7682c;" href="{question_link}">{question_title}</a>": <br /> {question_content}</p>\r\n<div style="padding: 10px 0 0;"><a class="btn" style="color: #fff; border-radius: 3px; text-decoration: none; background-color: #f7682c; padding: 10px 20px; font-size: 14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;" href="{question_link}">View Question →</a></div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<div style="border-top: 1px solid #ddd; padding: 20px 0; clear: both; text-align: center;"><small style="font-size: 11px;">{site_name} - {site_description}</small></div>\r\n</div>\r\n</div>', 'yes'),
(238, 'dwqa_options', 'a:11:{s:5:"pages";a:3:{s:16:"archive-question";s:1:"8";s:15:"submit-question";s:1:"9";i:404;s:1:"0";}s:14:"posts-per-page";s:2:"25";s:15:"single-template";s:2:"-1";s:23:"question-new-time-frame";s:1:"4";s:27:"question-overdue-time-frame";s:1:"2";s:19:"captcha-in-question";s:1:"1";s:25:"captcha-google-public-key";s:40:"6LfuJAgTAAAAABLfVv1noods6ZdIjBGZmqTVgpcA";s:26:"captcha-google-private-key";s:40:"6LfuJAgTAAAAAPddzlFx-S18dcKJ1PNtI_QU3ILG";s:16:"question-rewrite";s:8:"question";s:25:"question-category-rewrite";s:17:"question-category";s:20:"question-tag-rewrite";s:12:"question-tag";}', 'yes'),
(239, 'dwqa-question_category_children', 'a:0:{}', 'yes'),
(1021, '_transient_dwqa_latest_answer_for_127', '', 'no'),
(463, 'theme_my_login', 'a:4:{s:10:"enable_css";b:1;s:11:"email_login";b:1;s:14:"active_modules";a:7:{i:0;s:29:"custom-email/custom-email.php";i:1;s:37:"custom-passwords/custom-passwords.php";i:2;s:41:"custom-redirection/custom-redirection.php";i:3;s:23:"recaptcha/recaptcha.php";i:4;s:21:"security/security.php";i:5;s:35:"themed-profiles/themed-profiles.php";i:6;s:35:"user-moderation/user-moderation.php";}s:7:"version";s:6:"6.3.12";}', 'yes'),
(241, 'dwqa_permission', 'a:5:{s:13:"administrator";a:3:{s:8:"question";a:4:{s:4:"read";s:1:"1";s:4:"post";s:1:"1";s:4:"edit";s:1:"1";s:6:"delete";s:1:"1";}s:6:"answer";a:4:{s:4:"read";s:1:"1";s:4:"post";s:1:"1";s:4:"edit";s:1:"1";s:6:"delete";s:1:"1";}s:7:"comment";a:4:{s:4:"read";s:1:"1";s:4:"post";s:1:"1";s:4:"edit";s:1:"1";s:6:"delete";s:1:"1";}}s:6:"membre";a:3:{s:8:"question";a:2:{s:4:"read";s:1:"1";s:4:"post";s:1:"1";}s:6:"answer";a:2:{s:4:"read";s:1:"1";s:4:"post";s:1:"1";}s:7:"comment";a:2:{s:4:"read";s:1:"1";s:4:"post";s:1:"1";}}s:10:"moderateur";a:3:{s:8:"question";a:4:{s:4:"read";s:1:"1";s:4:"post";s:1:"1";s:4:"edit";s:1:"1";s:6:"delete";s:1:"1";}s:6:"answer";a:4:{s:4:"read";s:1:"1";s:4:"post";s:1:"1";s:4:"edit";s:1:"1";s:6:"delete";s:1:"1";}s:7:"comment";a:4:{s:4:"read";s:1:"1";s:4:"post";s:1:"1";s:4:"edit";s:1:"1";s:6:"delete";s:1:"1";}}s:11:"specialiste";a:3:{s:8:"question";a:2:{s:4:"read";s:1:"1";s:4:"post";s:1:"1";}s:6:"answer";a:2:{s:4:"read";s:1:"1";s:4:"post";s:1:"1";}s:7:"comment";a:2:{s:4:"read";s:1:"1";s:4:"post";s:1:"1";}}s:9:"anonymous";a:3:{s:8:"question";a:1:{s:4:"read";s:1:"1";}s:6:"answer";a:1:{s:4:"read";s:1:"1";}s:7:"comment";a:1:{s:4:"read";s:1:"1";}}}', 'yes'),
(242, 'dwqa_has_roles', '1', 'yes'),
(256, 'dwqa_subscrible_new_question_email_subject', '', 'yes'),
(257, 'dwqa_subscrible_enable_new_question_notification', '1', 'yes'),
(258, 'dwqa_subscrible_new_answer_email', '<div style="background: #f5f5f5; color: #333; font-size: 14px; line-height: 20px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">\r\n<div style="width: 600px; margin: 0 auto;">\r\n<div style="border-bottom: 1px solid #ddd; padding: 20px 0; text-align: center;">{site_logo}</div>\r\n<div style="padding: 10px 0 40px;">\r\n<p>Bonjour {question_author},</p>\r\n<table width="100%" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top" width="80">\r\n<div style="width: 60px; height: 60px; background: #ddd; text-align: center; word-wrap: break-word;">{answer_avatar}</div>\r\n</td>\r\n<td valign="top">\r\n<p style="font-size: 14px; line-height: 20px; color: #333; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><a style="text-decoration: none; color: #f7682c;" href="{answer_author_link}">{answer_author}</a> a répondu à votre question : "<a style="text-decoration: none; color: #f7682c;" href="{question_link}">{question_title}</a>":</p>\r\n<blockquote>\r\n<p>{answer_content}</p>\r\n</blockquote>\r\n<div style="padding: 10px 0 0;"><a class="btn" style="color: #fff; border-radius: 3px; text-decoration: none; background-color: #f7682c; padding: 10px 20px; font-size: 14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;" href="{answer_link}">Voir la réponse →</a></div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<div style="border-top: 1px solid #ddd; padding: 20px 0; clear: both; text-align: center;"><small style="font-size: 11px;">{site_name} - {site_description}</small></div>\r\n</div>\r\n</div>', 'yes'),
(259, 'dwqa_subscrible_new_answer_email_subject', '{answer_author} a répondu à votre question !', 'yes'),
(260, 'dwqa_subscrible_enable_new_answer_notification', '1', 'yes'),
(261, 'dwqa_subscrible_new_answer_followers_email', '<div style="background: #f5f5f5; color: #333; font-size: 14px; line-height: 20px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">\r\n<div style="width: 600px; margin: 0 auto;">\r\n<div style="border-bottom: 1px solid #ddd; padding: 20px 0; text-align: center;">{site_logo}</div>\r\n<div style="padding: 10px 0 40px;">\r\n<p>Bonjour {follower},</p>\r\n<table width="100%" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top" width="80">\r\n<div style="width: 60px; height: 60px; background: #ddd; text-align: center; word-wrap: break-word;">{answer_avatar}</div>\r\n</td>\r\n<td valign="top">\r\n<p style="font-size: 14px; line-height: 20px; color: #333; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><a style="text-decoration: none; color: #f7682c;" href="{answer_author_link}">{answer_author}</a> a répondu à la question que vous suivez : "<a style="text-decoration: none; color: #f7682c;" href="{question_link}">{question_title}</a>":</p>\r\n<blockquote>\r\n<p>{answer_content}</p>\r\n</blockquote>\r\n<div style="padding: 10px 0 0;"><a class="btn" style="color: #fff; border-radius: 3px; text-decoration: none; background-color: #f7682c; padding: 10px 20px; font-size: 14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;" href="{answer_link}">Voir la réponse →</a></div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<div style="border-top: 1px solid #ddd; padding: 20px 0; clear: both; text-align: center;"><small style="font-size: 11px;">{site_name} - {site_description}</small></div>\r\n</div>\r\n</div>', 'yes'),
(262, 'dwqa_subscrible_new_answer_followers_email_subject', 'Une nouvelle réponse a été postée sur la question que vous suivez !', 'yes'),
(263, 'dwqa_subscrible_enable_new_answer_followers_notification', '1', 'yes'),
(264, 'dwqa_subscrible_new_comment_question_email_subject', '', 'yes'),
(265, 'dwqa_subscrible_new_comment_question_email', '<div style="background: #f5f5f5; color: #333; font-size: 14px; line-height: 20px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">\r\n<div style="width: 600px; margin: 0 auto;">\r\n<div style="border-bottom: 1px solid #ddd; padding: 20px 0; text-align: center;">{site_logo}</div>\r\n<div style="padding: 10px 0 40px;">\r\n<p>Howdy {question_author},</p>\r\n<table width="100%" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top" width="80">\r\n<div style="width: 60px; height: 60px; background: #ddd; text-align: center; word-wrap: break-word;">{comment_author_avatar}</div>\r\n</td>\r\n<td valign="top">\r\n<p style="font-size: 14px; line-height: 20px; color: #333; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><a style="text-decoration: none; color: #f7682c;" href="{comment_author_link}">{comment_author}</a> has commented on your question at "<a style="text-decoration: none; color: #f7682c;" href="{comment_link}">{question_title}</a>": <br /> {comment_content}</p>\r\n<div style="padding: 10px 0 0;"><a class="btn" style="color: #fff; border-radius: 3px; text-decoration: none; background-color: #f7682c; padding: 10px 20px; font-size: 14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;" href="{comment_link}">View Comment →</a></div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<div style="border-top: 1px solid #ddd; padding: 20px 0; clear: both; text-align: center;"><small style="font-size: 11px;">{site_name} - {site_description}</small></div>\r\n</div>\r\n</div>', 'yes'),
(266, 'dwqa_subscrible_enable_new_comment_question_notification', '', 'yes'),
(267, 'dwqa_subscrible_new_comment_question_followers_email_subject', 'Un nouveau commentaire a été posté sur la question que vous suivez !', 'yes'),
(268, 'dwqa_subscrible_new_comment_question_followers_email', '<div style="background: #f5f5f5; color: #333; font-size: 14px; line-height: 20px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">\r\n<div style="width: 600px; margin: 0 auto;">\r\n<div style="border-bottom: 1px solid #ddd; padding: 20px 0; text-align: center;">{site_logo}</div>\r\n<div style="padding: 10px 0 40px;">\r\n<p>Bonjour {follower},</p>\r\n<table width="100%" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top" width="80">\r\n<div style="width: 60px; height: 60px; background: #ddd; text-align: center; word-wrap: break-word;">{comment_author_avatar}</div>\r\n</td>\r\n<td valign="top">\r\n<p style="font-size: 14px; line-height: 20px; color: #333; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><a style="text-decoration: none; color: #f7682c;" href="{comment_author_link}">{comment_author}</a> a commenté la question que vous suivez : "<a style="text-decoration: none; color: #f7682c;" href="{comment_link}">{question_title}</a>" </p>\r\n<blockquote>\r\n<p>{comment_content}</p>\r\n</blockquote>\r\n<div style="padding: 10px 0 0;"><a class="btn" style="color: #fff; border-radius: 3px; text-decoration: none; background-color: #f7682c; padding: 10px 20px; font-size: 14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;" href="{comment_link}">Voir le commentaire →</a></div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<div style="border-top: 1px solid #ddd; padding: 20px 0; clear: both; text-align: center;"><small style="font-size: 11px;">{site_name} - {site_description}</small></div>\r\n</div>\r\n</div>', 'yes'),
(269, 'dwqa_subscrible_enable_new_comment_question_followers_notify', '1', 'yes'),
(270, 'dwqa_subscrible_new_comment_answer_email_subject', '', 'yes'),
(271, 'dwqa_subscrible_new_comment_answer_email', '<div style="background: #f5f5f5; color: #333; font-size: 14px; line-height: 20px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">\r\n<div style="width: 600px; margin: 0 auto;">\r\n<div style="border-bottom: 1px solid #ddd; padding: 20px 0; text-align: center;">{site_logo}</div>\r\n<div style="padding: 10px 0 40px;">\r\n<p>Howdy {answer_author},</p>\r\n<table width="100%" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top" width="80">\r\n<div style="width: 60px; height: 60px; background: #ddd; text-align: center; word-wrap: break-word;">{comment_author_avatar}</div>\r\n</td>\r\n<td valign="top">\r\n<p style="font-size: 14px; line-height: 20px; color: #333; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><a style="text-decoration: none; color: #f7682c;" href="{comment_author_link}">{comment_author}</a> has commented on your answer at "<a style="text-decoration: none; color: #f7682c;" href="{comment_link}">{question_title}</a>": <br /> {comment_content}</p>\r\n<div style="padding: 10px 0 0;"><a class="btn" style="color: #fff; border-radius: 3px; text-decoration: none; background-color: #f7682c; padding: 10px 20px; font-size: 14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;" href="{comment_link}">View Comment →</a></div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<div style="border-top: 1px solid #ddd; padding: 20px 0; clear: both; text-align: center;"><small style="font-size: 11px;">{site_name} - {site_description}</small></div>\r\n</div>\r\n</div>', 'yes'),
(272, 'dwqa_subscrible_enable_new_comment_answer_notification', '', 'yes'),
(273, 'dwqa_subscrible_new_comment_answer_followers_email_subject', 'Un nouveau commentaire a été posté sur la réponse que vous suivez !', 'yes'),
(274, 'dwqa_subscrible_new_comment_answer_followers_email', '<div style="background: #f5f5f5; color: #333; font-size: 14px; line-height: 20px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">\r\n<div style="width: 600px; margin: 0 auto;">\r\n<div style="border-bottom: 1px solid #ddd; padding: 20px 0; text-align: center;">{site_logo}</div>\r\n<div style="padding: 10px 0 40px;">\r\n<p>Bonjour {follower},</p>\r\n<table width="100%" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top" width="80">\r\n<div style="width: 60px; height: 60px; background: #ddd; text-align: center; word-wrap: break-word;">{comment_author_avatar}</div>\r\n</td>\r\n<td valign="top">\r\n<p style="font-size: 14px; line-height: 20px; color: #333; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><a style="text-decoration: none; color: #f7682c;" href="{comment_author_link}">{comment_author}</a> a commenté la réponse que vous suivez "<a style="text-decoration: none; color: #f7682c;" href="{comment_link}">{question_title}</a>": </p>\r\n<blockquote>{comment_content}</blockquote>\r\n<div style="padding: 10px 0 0;"><a class="btn" style="color: #fff; border-radius: 3px; text-decoration: none; background-color: #f7682c; padding: 10px 20px; font-size: 14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;" href="{comment_link}">Voir le commentaire →</a></div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<div style="border-top: 1px solid #ddd; padding: 20px 0; clear: both; text-align: center;"><small style="font-size: 11px;">{site_name} - {site_description}</small></div>\r\n</div>\r\n</div>', 'yes'),
(275, 'dwqa_subscrible_enable_new_comment_answer_followers_notification', '1', 'yes'),
(297, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(311, 'widget_dwqa-closed-question', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(312, 'widget_dwqa-latest-question', 'a:3:{i:1;a:0:{}s:12:"_multiwidget";i:1;i:3;a:2:{s:5:"title";s:28:"Dernières Questions Posées";s:6:"number";s:1:"5";}}', 'yes'),
(313, 'widget_dwqa-popular-question', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(314, 'widget_dwqa-related-question', 'a:3:{i:1;a:0:{}i:3;a:2:{s:5:"title";s:16:"Questions Liées";s:6:"number";s:1:"5";}s:12:"_multiwidget";i:1;}', 'yes'),
(973, 'category_children', 'a:0:{}', 'yes');
INSERT INTO `dcHyv_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(363, 'wpuf_labels', 'a:16:{s:11:"title_label";s:5:"Titre";s:10:"title_help";s:0:"";s:9:"cat_label";s:10:"Catégorie";s:8:"cat_help";s:0:"";s:10:"desc_label";s:11:"Description";s:9:"desc_help";s:0:"";s:9:"tag_label";s:4:"Tags";s:8:"tag_help";s:0:"";s:12:"submit_label";s:24:"Soumettre la publication";s:12:"update_label";s:14:"Mettre à jour";s:14:"updating_label";s:21:"Veuillez patienter...";s:14:"ft_image_label";s:15:"Image d''Aperçu";s:18:"ft_image_btn_label";s:18:"Uploader une image";s:16:"attachment_label";s:13:"Pièce-jointe";s:15:"attachment_help";s:68:"Vous permet d''ajouter une pièce-jointe au contenu de votre article.";s:20:"attachment_btn_label";s:25:"Ajouter une pièce jointe";}', 'yes'),
(364, 'wpuf_frontend_posting', 'a:16:{s:11:"post_status";s:7:"pending";s:11:"post_author";s:8:"original";s:10:"map_author";s:1:"1";s:10:"allow_cats";s:2:"on";s:12:"exclude_cats";s:1:"1";s:11:"default_cat";s:1:"5";s:8:"cat_type";s:6:"normal";s:21:"enable_featured_image";s:3:"yes";s:16:"allow_attachment";s:3:"yes";s:14:"attachment_num";s:1:"3";s:19:"attachment_max_size";s:4:"4096";s:11:"editor_type";s:4:"full";s:10:"allow_tags";s:2:"on";s:19:"enable_custom_field";s:3:"off";s:16:"enable_post_date";s:3:"off";s:18:"enable_post_expiry";s:3:"off";}', 'yes'),
(365, 'wpuf_dashboard', 'a:6:{s:9:"post_type";s:4:"post";s:8:"per_page";s:2:"10";s:13:"show_user_bio";s:3:"off";s:15:"show_post_count";s:2:"on";s:13:"show_ft_image";s:3:"off";s:11:"ft_img_size";s:9:"thumbnail";}', 'yes'),
(366, 'wpuf_others', 'a:9:{s:17:"post_notification";s:3:"yes";s:16:"enable_post_edit";s:3:"yes";s:15:"enable_post_del";s:3:"yes";s:12:"edit_page_id";s:2:"19";s:12:"admin_access";s:17:"edit_others_posts";s:13:"cf_show_front";s:3:"off";s:14:"att_show_front";s:3:"off";s:17:"override_editlink";s:2:"no";s:10:"custom_css";s:0:"";}', 'yes'),
(367, 'wpuf_payment', '', 'yes'),
(368, 'wpuf_support', '', 'yes'),
(526, '_transient_timeout_base_b10b7379cf31b3e89e9b5d5a38a4215b', '1432986203', 'no'),
(467, 'theme_my_login_themed_profiles', 'a:4:{s:13:"administrator";a:2:{s:13:"theme_profile";b:0;s:14:"restrict_admin";b:0;}s:6:"membre";a:2:{s:13:"theme_profile";b:0;s:14:"restrict_admin";b:0;}s:10:"moderateur";a:2:{s:13:"theme_profile";b:0;s:14:"restrict_admin";b:0;}s:11:"specialiste";a:2:{s:13:"theme_profile";b:0;s:14:"restrict_admin";b:0;}}', 'yes'),
(525, 'widget_theme-my-login', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(471, 'theme_my_login_security', 'a:3:{s:12:"private_site";b:0;s:13:"private_login";b:1;s:12:"failed_login";a:5:{s:9:"threshold";i:5;s:18:"threshold_duration";i:1;s:23:"threshold_duration_unit";s:4:"hour";s:16:"lockout_duration";i:2;s:21:"lockout_duration_unit";s:4:"hour";}}', 'yes'),
(546, 'theme_my_login_redirection', 'a:4:{s:13:"administrator";a:4:{s:10:"login_type";s:6:"custom";s:9:"login_url";s:25:"http://127.0.0.1/docoeur/";s:11:"logout_type";s:7:"default";s:10:"logout_url";s:0:"";}s:6:"membre";a:4:{s:10:"login_type";s:6:"custom";s:9:"login_url";s:25:"http://127.0.0.1/docoeur/";s:11:"logout_type";s:7:"default";s:10:"logout_url";s:0:"";}s:10:"moderateur";a:4:{s:10:"login_type";s:6:"custom";s:9:"login_url";s:25:"http://127.0.0.1/docoeur/";s:11:"logout_type";s:7:"default";s:10:"logout_url";s:0:"";}s:11:"specialiste";a:4:{s:10:"login_type";s:6:"custom";s:9:"login_url";s:25:"http://127.0.0.1/docoeur/";s:11:"logout_type";s:7:"default";s:10:"logout_url";s:0:"";}}', 'yes'),
(486, 'theme_my_login_recaptcha', 'a:3:{s:10:"public_key";s:40:"6LfsJAgTAAAAAJpQfD7QjgJNXvZmFSi2TE1k39XN";s:11:"private_key";s:40:"6LfsJAgTAAAAAMQyhFeZOYK6x4HSwqzUor5PEjQk";s:5:"theme";s:5:"white";}', 'yes'),
(527, '_transient_base_b10b7379cf31b3e89e9b5d5a38a4215b', 'O:8:"stdClass":2:{s:4:"info";a:3:{s:4:"page";i:1;s:5:"pages";i:1;s:7:"results";i:5;}s:6:"themes";a:5:{i:0;O:8:"stdClass":10:{s:4:"name";s:8:"Store WP";s:4:"slug";s:8:"store-wp";s:7:"version";s:4:"1.13";s:11:"preview_url";s:30:"https://wp-themes.com/store-wp";s:6:"author";s:9:"iografica";s:14:"screenshot_url";s:61:"//ts.w.org/wp-content/themes/store-wp/screenshot.png?ver=1.13";s:6:"rating";d:0;s:11:"num_ratings";s:1:"0";s:8:"homepage";s:38:"https://wordpress.org/themes/store-wp/";s:11:"description";s:386:"Store WP is a powerful and flexible free WordPress theme that offer deep integration with WooCommerce. Store WP will make your website adaptable with any type of mobile devices or tablet and it is the perfect theme for your next WooCommerce project. The theme has included a product carousel slider to enhance your shop''s page and can be customized thanks to the advanced options panel.";}i:1;O:8:"stdClass":10:{s:4:"name";s:7:"Onepage";s:4:"slug";s:7:"onepage";s:7:"version";s:4:"1.03";s:11:"preview_url";s:29:"https://wp-themes.com/onepage";s:6:"author";s:9:"iografica";s:14:"screenshot_url";s:60:"//ts.w.org/wp-content/themes/onepage/screenshot.png?ver=1.03";s:6:"rating";d:0;s:11:"num_ratings";s:1:"0";s:8:"homepage";s:37:"https://wordpress.org/themes/onepage/";s:11:"description";s:474:"Onepage is a responsive Wordpress theme with a one page home design. In the home page of the the theme you can display all the essential features of your website. Onepage is a responsive Wordpress theme with a one page home page design. In the home page of the the theme you can display all the essential features of your website. Onepage is flessible, lightweight and it comes with a clean and flat design. Onepage is fully customizable thanks to the options page built in.";}i:2;O:8:"stdClass":10:{s:4:"name";s:8:"Clearsky";s:4:"slug";s:8:"clearsky";s:7:"version";s:4:"1.15";s:11:"preview_url";s:30:"https://wp-themes.com/clearsky";s:6:"author";s:9:"iografica";s:14:"screenshot_url";s:61:"//ts.w.org/wp-content/themes/clearsky/screenshot.png?ver=1.15";s:6:"rating";d:60;s:11:"num_ratings";s:1:"2";s:8:"homepage";s:38:"https://wordpress.org/themes/clearsky/";s:11:"description";s:455:"Clearsky is a multi-purpose responsive WordPress masonry theme. Masonry is a popular and efficient style layout that permit you to show all types of visual elements quickly. Clearsky is fully responsive, flexible, WooCommerce compatible, retina ready, Search Engine Optimized and comes with an advanced option panel in order to make your life easier and build your site very quickly. The theme comes with a mobile ready carousel slider to show your posts.";}i:3;O:8:"stdClass":10:{s:4:"name";s:11:"Big Impresa";s:4:"slug";s:11:"big-impresa";s:7:"version";s:4:"1.18";s:11:"preview_url";s:33:"https://wp-themes.com/big-impresa";s:6:"author";s:9:"iografica";s:14:"screenshot_url";s:64:"//ts.w.org/wp-content/themes/big-impresa/screenshot.png?ver=1.18";s:6:"rating";d:100;s:11:"num_ratings";s:1:"2";s:8:"homepage";s:41:"https://wordpress.org/themes/big-impresa/";s:11:"description";s:282:"Big Impresa is a basic business WordPress theme with clean and responsive interface, based on underscores with integration of skeleton 960 grid. Big Impresa is WooCommerce compatible, multilingual ready, RTL-Language support, retina-ready, SEO friendly and cross-browser compatible.";}i:4;O:8:"stdClass":10:{s:4:"name";s:7:"Base WP";s:4:"slug";s:7:"base-wp";s:7:"version";s:4:"1.29";s:11:"preview_url";s:29:"https://wp-themes.com/base-wp";s:6:"author";s:9:"iografica";s:14:"screenshot_url";s:60:"//ts.w.org/wp-content/themes/base-wp/screenshot.png?ver=1.29";s:6:"rating";d:90;s:11:"num_ratings";s:1:"4";s:8:"homepage";s:37:"https://wordpress.org/themes/base-wp/";s:11:"description";s:358:"Base WP is a basic blog or business WordPress theme, based on undescores with integration of skeleton 960 grid. Base WP is WooCommerce compatible, multilingual ready, RTL-Language support, retina-ready, SEO friendly and cross-browser compatible. For other themes and plugin or for support and documentation visit our website (http://www.iograficathemes.com).";}}}', 'no'),
(528, '_transient_timeout_base_3a5352ee26a3aba28bb01fe3e6792ca0', '1432986203', 'no'),
(529, '_transient_base_3a5352ee26a3aba28bb01fe3e6792ca0', 'O:8:"stdClass":14:{s:4:"name";s:8:"Store WP";s:4:"slug";s:8:"store-wp";s:7:"version";s:4:"1.13";s:11:"preview_url";s:30:"https://wp-themes.com/store-wp";s:6:"author";s:9:"iografica";s:14:"screenshot_url";s:61:"//ts.w.org/wp-content/themes/store-wp/screenshot.png?ver=1.13";s:6:"rating";d:0;s:11:"num_ratings";s:1:"0";s:10:"downloaded";i:7228;s:12:"last_updated";s:10:"2015-04-24";s:8:"homepage";s:38:"https://wordpress.org/themes/store-wp/";s:8:"sections";a:1:{s:11:"description";s:386:"Store WP is a powerful and flexible free WordPress theme that offer deep integration with WooCommerce. Store WP will make your website adaptable with any type of mobile devices or tablet and it is the perfect theme for your next WooCommerce project. The theme has included a product carousel slider to enhance your shop''s page and can be customized thanks to the advanced options panel.";}s:13:"download_link";s:54:"http://downloads.wordpress.org/theme/store-wp.1.13.zip";s:4:"tags";a:16:{s:4:"blue";s:4:"Blue";s:17:"custom-background";s:17:"Custom Background";s:13:"custom-header";s:13:"Custom Header";s:11:"custom-menu";s:11:"Custom Menu";s:15:"featured-images";s:15:"Featured Images";s:19:"full-width-template";s:19:"Full Width Template";s:4:"gray";s:4:"Gray";s:12:"microformats";s:12:"Microformats";s:10:"one-column";s:10:"One Column";s:12:"post-formats";s:12:"Post Formats";s:17:"responsive-layout";s:17:"Responsive Layout";s:13:"right-sidebar";s:13:"Right Sidebar";s:11:"sticky-post";s:11:"Sticky Post";s:13:"theme-options";s:13:"Theme Options";s:17:"translation-ready";s:17:"Translation Ready";s:11:"two-columns";s:11:"Two Columns";}}', 'no'),
(530, '_transient_timeout_base_7b2bbd0566dbd9afba085cdbb459ada5', '1432986203', 'no'),
(531, '_transient_base_7b2bbd0566dbd9afba085cdbb459ada5', 'O:8:"stdClass":14:{s:4:"name";s:7:"Onepage";s:4:"slug";s:7:"onepage";s:7:"version";s:4:"1.03";s:11:"preview_url";s:29:"https://wp-themes.com/onepage";s:6:"author";s:9:"iografica";s:14:"screenshot_url";s:60:"//ts.w.org/wp-content/themes/onepage/screenshot.png?ver=1.03";s:6:"rating";d:0;s:11:"num_ratings";s:1:"0";s:10:"downloaded";i:5259;s:12:"last_updated";s:10:"2015-04-23";s:8:"homepage";s:37:"https://wordpress.org/themes/onepage/";s:8:"sections";a:1:{s:11:"description";s:474:"Onepage is a responsive Wordpress theme with a one page home design. In the home page of the the theme you can display all the essential features of your website. Onepage is a responsive Wordpress theme with a one page home page design. In the home page of the the theme you can display all the essential features of your website. Onepage is flessible, lightweight and it comes with a clean and flat design. Onepage is fully customizable thanks to the options page built in.";}s:13:"download_link";s:53:"http://downloads.wordpress.org/theme/onepage.1.03.zip";s:4:"tags";a:16:{s:17:"custom-background";s:17:"Custom Background";s:13:"custom-header";s:13:"Custom Header";s:11:"custom-menu";s:11:"Custom Menu";s:15:"featured-images";s:15:"Featured Images";s:19:"full-width-template";s:19:"Full Width Template";s:5:"light";s:5:"Light";s:10:"one-column";s:10:"One Column";s:12:"post-formats";s:12:"Post Formats";s:3:"red";s:3:"Red";s:17:"responsive-layout";s:17:"Responsive Layout";s:13:"right-sidebar";s:13:"Right Sidebar";s:11:"sticky-post";s:11:"Sticky Post";s:13:"theme-options";s:13:"Theme Options";s:17:"translation-ready";s:17:"Translation Ready";s:11:"two-columns";s:11:"Two Columns";s:5:"white";s:5:"White";}}', 'no'),
(532, '_transient_timeout_base_a64f3197e68e769231d411e4cab2f372', '1432986204', 'no'),
(533, '_transient_base_a64f3197e68e769231d411e4cab2f372', 'O:8:"stdClass":14:{s:4:"name";s:8:"Clearsky";s:4:"slug";s:8:"clearsky";s:7:"version";s:4:"1.15";s:11:"preview_url";s:30:"https://wp-themes.com/clearsky";s:6:"author";s:9:"iografica";s:14:"screenshot_url";s:61:"//ts.w.org/wp-content/themes/clearsky/screenshot.png?ver=1.15";s:6:"rating";d:60;s:11:"num_ratings";s:1:"2";s:10:"downloaded";i:8296;s:12:"last_updated";s:10:"2015-04-25";s:8:"homepage";s:38:"https://wordpress.org/themes/clearsky/";s:8:"sections";a:1:{s:11:"description";s:455:"Clearsky is a multi-purpose responsive WordPress masonry theme. Masonry is a popular and efficient style layout that permit you to show all types of visual elements quickly. Clearsky is fully responsive, flexible, WooCommerce compatible, retina ready, Search Engine Optimized and comes with an advanced option panel in order to make your life easier and build your site very quickly. The theme comes with a mobile ready carousel slider to show your posts.";}s:13:"download_link";s:54:"http://downloads.wordpress.org/theme/clearsky.1.15.zip";s:4:"tags";a:15:{s:4:"blue";s:4:"Blue";s:17:"custom-background";s:17:"Custom Background";s:13:"custom-header";s:13:"Custom Header";s:11:"custom-menu";s:11:"Custom Menu";s:15:"featured-images";s:15:"Featured Images";s:19:"full-width-template";s:19:"Full Width Template";s:10:"one-column";s:10:"One Column";s:12:"post-formats";s:12:"Post Formats";s:17:"responsive-layout";s:17:"Responsive Layout";s:13:"right-sidebar";s:13:"Right Sidebar";s:11:"sticky-post";s:11:"Sticky Post";s:13:"theme-options";s:13:"Theme Options";s:17:"translation-ready";s:17:"Translation Ready";s:11:"two-columns";s:11:"Two Columns";s:5:"white";s:5:"White";}}', 'no'),
(534, '_transient_timeout_base_156d904604d4b5ec163c86921709cb9c', '1432986204', 'no'),
(535, '_transient_base_156d904604d4b5ec163c86921709cb9c', 'O:8:"stdClass":14:{s:4:"name";s:11:"Big Impresa";s:4:"slug";s:11:"big-impresa";s:7:"version";s:4:"1.18";s:11:"preview_url";s:33:"https://wp-themes.com/big-impresa";s:6:"author";s:9:"iografica";s:14:"screenshot_url";s:64:"//ts.w.org/wp-content/themes/big-impresa/screenshot.png?ver=1.18";s:6:"rating";d:100;s:11:"num_ratings";s:1:"2";s:10:"downloaded";i:14629;s:12:"last_updated";s:10:"2015-03-04";s:8:"homepage";s:41:"https://wordpress.org/themes/big-impresa/";s:8:"sections";a:1:{s:11:"description";s:282:"Big Impresa is a basic business WordPress theme with clean and responsive interface, based on underscores with integration of skeleton 960 grid. Big Impresa is WooCommerce compatible, multilingual ready, RTL-Language support, retina-ready, SEO friendly and cross-browser compatible.";}s:13:"download_link";s:57:"http://downloads.wordpress.org/theme/big-impresa.1.18.zip";s:4:"tags";a:16:{s:4:"blue";s:4:"Blue";s:17:"custom-background";s:17:"Custom Background";s:13:"custom-header";s:13:"Custom Header";s:11:"custom-menu";s:11:"Custom Menu";s:15:"featured-images";s:15:"Featured Images";s:19:"full-width-template";s:19:"Full Width Template";s:4:"gray";s:4:"Gray";s:10:"one-column";s:10:"One Column";s:12:"post-formats";s:12:"Post Formats";s:17:"responsive-layout";s:17:"Responsive Layout";s:13:"right-sidebar";s:13:"Right Sidebar";s:20:"rtl-language-support";s:20:"RTL Language Support";s:11:"sticky-post";s:11:"Sticky Post";s:13:"theme-options";s:13:"Theme Options";s:17:"translation-ready";s:17:"Translation Ready";s:11:"two-columns";s:11:"Two Columns";}}', 'no'),
(536, '_transient_timeout_base_0cc1f5957f5f584e38fa163b366408bb', '1432986205', 'no'),
(537, '_transient_base_0cc1f5957f5f584e38fa163b366408bb', 'O:8:"stdClass":14:{s:4:"name";s:7:"Base WP";s:4:"slug";s:7:"base-wp";s:7:"version";s:4:"1.29";s:11:"preview_url";s:29:"https://wp-themes.com/base-wp";s:6:"author";s:9:"iografica";s:14:"screenshot_url";s:60:"//ts.w.org/wp-content/themes/base-wp/screenshot.png?ver=1.29";s:6:"rating";d:90;s:11:"num_ratings";s:1:"4";s:10:"downloaded";i:44330;s:12:"last_updated";s:10:"2015-03-16";s:8:"homepage";s:37:"https://wordpress.org/themes/base-wp/";s:8:"sections";a:1:{s:11:"description";s:358:"Base WP is a basic blog or business WordPress theme, based on undescores with integration of skeleton 960 grid. Base WP is WooCommerce compatible, multilingual ready, RTL-Language support, retina-ready, SEO friendly and cross-browser compatible. For other themes and plugin or for support and documentation visit our website (http://www.iograficathemes.com).";}s:13:"download_link";s:53:"http://downloads.wordpress.org/theme/base-wp.1.29.zip";s:4:"tags";a:15:{s:17:"custom-background";s:17:"Custom Background";s:13:"custom-header";s:13:"Custom Header";s:11:"custom-menu";s:11:"Custom Menu";s:15:"featured-images";s:15:"Featured Images";s:19:"full-width-template";s:19:"Full Width Template";s:10:"one-column";s:10:"One Column";s:6:"orange";s:6:"Orange";s:12:"post-formats";s:12:"Post Formats";s:17:"responsive-layout";s:17:"Responsive Layout";s:13:"right-sidebar";s:13:"Right Sidebar";s:11:"sticky-post";s:11:"Sticky Post";s:13:"theme-options";s:13:"Theme Options";s:17:"translation-ready";s:17:"Translation Ready";s:11:"two-columns";s:11:"Two Columns";s:5:"white";s:5:"White";}}', 'no'),
(1024, '_transient_dwqa_answer_count_for_127', '0', 'no'),
(1417, '_transient_timeout_dwqa_answer_count_for_70', '1434034044', 'no'),
(1415, '_transient_timeout_dwqa_latest_answer_for_70', '1434034044', 'no'),
(1416, '_transient_dwqa_latest_answer_for_70', 'O:7:"WP_Post":24:{s:2:"ID";i:84;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2015-06-03 09:49:55";s:13:"post_date_gmt";s:19:"2015-06-03 08:49:55";s:12:"post_content";s:249:"Voir le graphique suivant \r\n<a href="http://127.0.0.1/docoeur/wp-content/uploads/2015/05/graphique.png"><img src="http://127.0.0.1/docoeur/wp-content/uploads/2015/05/graphique-300x163.png" alt="graphique" width="300" height="163" /></a>";s:10:"post_title";s:77:"répondreQuels sont les aliments bons et les aliments mauvais pour le coeur ?";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:76:"repondrequels-sont-les-aliments-bons-et-les-aliments-mauvais-pour-le-coeur-2";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2015-06-03 09:49:55";s:17:"post_modified_gmt";s:19:"2015-06-03 08:49:55";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:120:"http://127.0.0.1/docoeur/dwqa-answer/repondrequels-sont-les-aliments-bons-et-les-aliments-mauvais-pour-le-coeur-2/";s:10:"menu_order";i:0;s:9:"post_type";s:11:"dwqa-answer";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}', 'no'),
(1020, '_transient_timeout_dwqa_latest_answer_for_127', '1433768413', 'no'),
(958, '_transient_timeout_dwqa_latest_answer_for_122', '1433527604', 'no'),
(959, '_transient_dwqa_latest_answer_for_122', '', 'no'),
(960, '_transient_timeout_dwqa_answer_count_for_122', '1433527604', 'no'),
(961, '_transient_dwqa_answer_count_for_122', '0', 'no'),
(962, '_transient_timeout_dwqa_latest_answer_for_121', '1433527604', 'no'),
(963, '_transient_dwqa_latest_answer_for_121', '', 'no'),
(964, '_transient_timeout_dwqa_answer_count_for_121', '1433527604', 'no'),
(1278, '_site_transient_timeout_browser_43d1522b603688bda36781305c878d46', '1434534365', 'yes'),
(1279, '_site_transient_browser_43d1522b603688bda36781305c878d46', 'a:9:{s:8:"platform";s:9:"Macintosh";s:4:"name";s:7:"Firefox";s:7:"version";s:4:"38.0";s:10:"update_url";s:23:"http://www.firefox.com/";s:7:"img_src";s:50:"http://s.wordpress.org/images/browsers/firefox.png";s:11:"img_src_ssl";s:49:"https://wordpress.org/images/browsers/firefox.png";s:15:"current_version";s:2:"16";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(1418, '_transient_dwqa_answer_count_for_70', '3', 'no'),
(965, '_transient_dwqa_answer_count_for_121', '0', 'no'),
(1231, '_site_transient_browser_2589a220583546006658f54ada687b45', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"43.0.2357.81";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(1133, 'updraft_dropbox', 'a:5:{s:6:"appkey";N;s:6:"secret";N;s:6:"folder";N;s:16:"tk_request_token";N;s:15:"tk_access_token";N;}', 'yes'),
(1170, 'updraft_lastmessage', 'La sauvegarde s''est correctement effectué et est complète (juin 10 15:30:03)', 'yes'),
(1047, 'updraftplus_unlocked_fd', '1', 'yes'),
(1048, 'updraftplus_last_lock_time_fd', '2015-06-10 14:30:03', 'yes'),
(1049, 'updraftplus_semaphore_fd', '0', 'yes'),
(1064, 'dbprefix_old_dbprefix', 'docoeur_', 'yes'),
(1065, 'dbprefix_new', 'dcHyv_', 'yes'),
(1101, '_transient_timeout_dwqa_latest_answer_for_128', '1433855584', 'no'),
(1072, 'updraftplus_unlocked_d', '1', 'yes'),
(1073, 'updraftplus_last_lock_time_d', '2015-06-08 13:16:26', 'yes'),
(1074, 'updraftplus_semaphore_d', '0', 'yes'),
(1108, '_transient_timeout_dwqa_answer_count_for_128', '1433855589', 'no'),
(1109, '_transient_dwqa_answer_count_for_128', '1', 'no'),
(1211, 'updraft_backup_history', 'a:2:{i:1433946603;a:5:{s:2:"db";s:49:"backup_2015-06-10-1530_docoeur_3dd0fbb3fcc8-db.gz";s:7:"db-size";i:44162;s:9:"checksums";a:1:{s:4:"sha1";a:1:{s:3:"db0";s:40:"f2d0a954035589c234c99f1f0b362a356081ef0a";}}s:5:"nonce";s:12:"3dd0fbb3fcc8";s:7:"service";a:1:{i:0;s:0:"";}}i:1433855520;a:11:{s:7:"service";s:4:"none";s:7:"db-size";i:42455;s:5:"nonce";s:12:"adaa20e66212";s:6:"others";a:1:{i:0;s:52:"backup_2015-06-09-1412_WP_v2_adaa20e66212-others.zip";}s:11:"others-size";i:515505;s:6:"themes";a:1:{i:0;s:52:"backup_2015-06-09-1412_WP_v2_adaa20e66212-themes.zip";}s:11:"themes-size";i:4002535;s:7:"uploads";a:1:{i:0;s:53:"backup_2015-06-09-1412_WP_v2_adaa20e66212-uploads.zip";}s:12:"uploads-size";i:506867;s:7:"plugins";a:1:{i:0;s:53:"backup_2015-06-09-1412_WP_v2_adaa20e66212-plugins.zip";}s:12:"plugins-size";i:8103401;}}', 'yes'),
(1176, 'updraft_last_backup', 'a:5:{s:11:"backup_time";i:1433946603;s:12:"backup_array";a:3:{s:2:"db";s:49:"backup_2015-06-10-1530_docoeur_3dd0fbb3fcc8-db.gz";s:7:"db-size";i:44162;s:9:"checksums";a:1:{s:4:"sha1";a:1:{s:3:"db0";s:40:"f2d0a954035589c234c99f1f0b362a356081ef0a";}}}s:7:"success";i:1;s:6:"errors";a:0:{}s:12:"backup_nonce";s:12:"3dd0fbb3fcc8";}', 'yes'),
(1177, 'updraft_starttime_files', '14:32', 'yes'),
(1178, 'updraft_starttime_db', '14:32', 'yes'),
(1179, 'updraft_startday_db', '0', 'yes'),
(1180, 'updraft_startday_files', '0', 'yes'),
(1181, 'updraft_sftp_settings', '', 'yes'),
(1130, 'updraft_remotesites', '', 'yes'),
(1131, 'updraft_migrator_localkeys', '', 'yes'),
(1132, 'updraft_autobackup_default', '0', 'yes'),
(1134, 'updraft_googledrive', '', 'yes'),
(1135, 'updraftplus_tmp_googledrive_access_token', '', 'yes'),
(1136, 'updraftplus_dismissedautobackup', '', 'yes'),
(1137, 'updraftplus_dismissedexpiry', '', 'yes'),
(1138, 'updraftplus_dismisseddashnotice', '', 'yes'),
(1142, 'updraft_retain', '1', 'yes'),
(1143, 'updraft_retain_db', '1', 'yes'),
(1144, 'updraft_encryptionphrase', '', 'yes'),
(1145, 'updraft_service', '', 'yes'),
(1146, 'updraft_dropbox_appkey', '', 'yes'),
(1147, 'updraft_dropbox_secret', '', 'yes'),
(1148, 'updraft_googledrive_clientid', '', 'yes'),
(1149, 'updraft_googledrive_secret', '', 'yes'),
(1150, 'updraft_googledrive_remotepath', '', 'yes'),
(1151, 'updraft_ftp_login', '', 'yes'),
(1152, 'updraft_ftp_pass', '', 'yes'),
(1153, 'updraft_ftp_remote_path', '', 'yes'),
(1154, 'updraft_server_address', '', 'yes'),
(1155, 'updraft_dir', '', 'yes'),
(1156, 'updraft_email', '', 'yes'),
(1157, 'updraft_delete_local', '0', 'yes'),
(1158, 'updraft_debug_mode', '0', 'yes'),
(1159, 'updraft_include_plugins', '0', 'yes'),
(1160, 'updraft_include_themes', '0', 'yes'),
(1161, 'updraft_include_uploads', '0', 'yes'),
(1162, 'updraft_include_others', '0', 'yes'),
(1163, 'updraft_include_wpcore', '0', 'yes'),
(1164, 'updraft_include_wpcore_exclude', '', 'yes'),
(1165, 'updraft_include_more', '0', 'yes'),
(1166, 'updraft_include_blogs', '', 'yes'),
(1167, 'updraft_include_mu-plugins', '', 'yes'),
(1168, 'updraft_include_others_exclude', '', 'yes'),
(1169, 'updraft_include_uploads_exclude', '', 'yes'),
(1171, 'updraft_googledrive_token', '', 'yes'),
(1172, 'updraft_dropboxtk_request_token', '', 'yes'),
(1173, 'updraft_dropboxtk_access_token', '', 'yes'),
(1174, 'updraft_dropbox_folder', '', 'yes'),
(1175, 'updraft_adminlocking', '', 'yes'),
(1182, 'updraft_s3', '', 'yes'),
(1183, 'updraft_s3generic', '', 'yes'),
(1184, 'updraft_dreamhost', '', 'yes'),
(1185, 'updraft_s3generic_login', '', 'yes'),
(1186, 'updraft_s3generic_pass', '', 'yes'),
(1187, 'updraft_s3generic_remote_path', '', 'yes'),
(1188, 'updraft_s3generic_endpoint', '', 'yes'),
(1189, 'updraft_webdav_settings', '', 'yes'),
(1190, 'updraft_openstack', '', 'yes'),
(1191, 'updraft_bitcasa', 'a:0:{}', 'yes'),
(1192, 'updraft_copycom', 'a:0:{}', 'yes'),
(1193, 'updraft_onedrive', 'a:0:{}', 'yes'),
(1194, 'updraft_cloudfiles', '', 'yes'),
(1195, 'updraft_cloudfiles_user', '', 'yes'),
(1196, 'updraft_cloudfiles_apikey', '', 'yes'),
(1197, 'updraft_cloudfiles_path', '', 'yes'),
(1198, 'updraft_cloudfiles_authurl', '', 'yes'),
(1199, 'updraft_ssl_useservercerts', '0', 'yes'),
(1200, 'updraft_ssl_disableverify', '0', 'yes'),
(1201, 'updraft_s3_login', '', 'yes'),
(1202, 'updraft_s3_pass', '', 'yes'),
(1203, 'updraft_s3_remote_path', '', 'yes'),
(1204, 'updraft_dreamobjects_login', '', 'yes'),
(1205, 'updraft_dreamobjects_pass', '', 'yes'),
(1206, 'updraft_dreamobjects_remote_path', '', 'yes'),
(1207, 'updraft_report_warningsonly', 'a:0:{}', 'yes'),
(1208, 'updraft_report_wholebackup', 'a:0:{}', 'yes'),
(1209, 'updraft_log_syslog', '0', 'yes'),
(1210, 'updraft_extradatabases', '', 'yes'),
(1214, 'updraftplus_unlocked_', '1', 'yes'),
(1215, 'updraftplus_last_lock_time_', '2015-06-09 14:52:58', 'yes'),
(1216, 'updraftplus_semaphore_', '0', 'yes'),
(1397, '_site_transient_update_plugins', 'O:8:"stdClass":4:{s:12:"last_checked";i:1434008429;s:8:"response";a:2:{s:33:"nav-menu-roles/nav-menu-roles.php";O:8:"stdClass":6:{s:2:"id";s:5:"34808";s:4:"slug";s:14:"nav-menu-roles";s:6:"plugin";s:33:"nav-menu-roles/nav-menu-roles.php";s:11:"new_version";s:5:"1.7.0";s:3:"url";s:45:"https://wordpress.org/plugins/nav-menu-roles/";s:7:"package";s:62:"http://downloads.wordpress.org/plugin/nav-menu-roles.1.7.0.zip";}s:27:"updraftplus/updraftplus.php";O:8:"stdClass":6:{s:2:"id";s:5:"31679";s:4:"slug";s:11:"updraftplus";s:6:"plugin";s:27:"updraftplus/updraftplus.php";s:11:"new_version";s:6:"1.10.3";s:3:"url";s:42:"https://wordpress.org/plugins/updraftplus/";s:7:"package";s:60:"http://downloads.wordpress.org/plugin/updraftplus.1.10.3.zip";}}s:12:"translations";a:0:{}s:9:"no_update";a:7:{s:19:"akismet/akismet.php";O:8:"stdClass":6:{s:2:"id";s:2:"15";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"3.1.2";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:55:"http://downloads.wordpress.org/plugin/akismet.3.1.2.zip";}s:41:"dw-question-answer/dw-question-answer.php";O:8:"stdClass":6:{s:2:"id";s:5:"45382";s:4:"slug";s:18:"dw-question-answer";s:6:"plugin";s:41:"dw-question-answer/dw-question-answer.php";s:11:"new_version";s:5:"1.3.3";s:3:"url";s:49:"https://wordpress.org/plugins/dw-question-answer/";s:7:"package";s:66:"http://downloads.wordpress.org/plugin/dw-question-answer.1.3.3.zip";}s:33:"theme-my-login/theme-my-login.php";O:8:"stdClass":6:{s:2:"id";s:4:"7109";s:4:"slug";s:14:"theme-my-login";s:6:"plugin";s:33:"theme-my-login/theme-my-login.php";s:11:"new_version";s:6:"6.3.12";s:3:"url";s:45:"https://wordpress.org/plugins/theme-my-login/";s:7:"package";s:63:"http://downloads.wordpress.org/plugin/theme-my-login.6.3.12.zip";}s:37:"user-role-editor/user-role-editor.php";O:8:"stdClass":6:{s:2:"id";s:5:"13697";s:4:"slug";s:16:"user-role-editor";s:6:"plugin";s:37:"user-role-editor/user-role-editor.php";s:11:"new_version";s:6:"4.18.4";s:3:"url";s:47:"https://wordpress.org/plugins/user-role-editor/";s:7:"package";s:58:"http://downloads.wordpress.org/plugin/user-role-editor.zip";}s:53:"wp-roles-at-registration/wp-roles-at-registration.php";O:8:"stdClass":6:{s:2:"id";s:5:"17304";s:4:"slug";s:24:"wp-roles-at-registration";s:6:"plugin";s:53:"wp-roles-at-registration/wp-roles-at-registration.php";s:11:"new_version";s:4:"0.20";s:3:"url";s:55:"https://wordpress.org/plugins/wp-roles-at-registration/";s:7:"package";s:71:"http://downloads.wordpress.org/plugin/wp-roles-at-registration.0.20.zip";}s:25:"wp-user-frontend/wpuf.php";O:8:"stdClass":6:{s:2:"id";s:5:"19815";s:4:"slug";s:16:"wp-user-frontend";s:6:"plugin";s:25:"wp-user-frontend/wpuf.php";s:11:"new_version";s:5:"1.3.2";s:3:"url";s:47:"https://wordpress.org/plugins/wp-user-frontend/";s:7:"package";s:58:"http://downloads.wordpress.org/plugin/wp-user-frontend.zip";}s:24:"wp-users-media/index.php";O:8:"stdClass":6:{s:2:"id";s:5:"50409";s:4:"slug";s:14:"wp-users-media";s:6:"plugin";s:24:"wp-users-media/index.php";s:11:"new_version";s:5:"2.0.2";s:3:"url";s:45:"https://wordpress.org/plugins/wp-users-media/";s:7:"package";s:62:"http://downloads.wordpress.org/plugin/wp-users-media.2.0.2.zip";}}}', 'yes'),
(1230, '_site_transient_timeout_browser_2589a220583546006658f54ada687b45', '1434490682', 'yes');

-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_postmeta`
--

CREATE TABLE IF NOT EXISTS `dcHyv_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=370 ;

--
-- Contenu de la table `dcHyv_postmeta`
--

INSERT INTO `dcHyv_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(12, 10, '_menu_item_object', 'custom'),
(9, 10, '_menu_item_type', 'custom'),
(10, 10, '_menu_item_menu_item_parent', '0'),
(11, 10, '_menu_item_object_id', '10'),
(13, 10, '_menu_item_target', ''),
(14, 10, '_menu_item_classes', 'a:1:{i:0;s:8:"fa-hover";}'),
(15, 10, '_menu_item_xfn', ''),
(16, 10, '_menu_item_url', 'http://127.0.0.1/docoeur/'),
(37, 9, '_edit_last', '1'),
(18, 11, '_menu_item_type', 'post_type'),
(19, 11, '_menu_item_menu_item_parent', '0'),
(20, 11, '_menu_item_object_id', '9'),
(21, 11, '_menu_item_object', 'page'),
(22, 11, '_menu_item_target', ''),
(23, 11, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(24, 11, '_menu_item_xfn', ''),
(25, 11, '_menu_item_url', ''),
(26, 11, '_menu_item_orphaned', '1432730612'),
(27, 12, '_menu_item_type', 'post_type'),
(28, 12, '_menu_item_menu_item_parent', '0'),
(29, 12, '_menu_item_object_id', '8'),
(30, 12, '_menu_item_object', 'page'),
(31, 12, '_menu_item_target', ''),
(32, 12, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(33, 12, '_menu_item_xfn', ''),
(34, 12, '_menu_item_url', ''),
(36, 9, '_edit_lock', '1432734750:1'),
(38, 9, '_wp_page_template', 'default'),
(39, 8, '_edit_lock', '1432900093:1'),
(40, 8, '_edit_last', '1'),
(41, 8, '_wp_page_template', 'page-nosidebar.php'),
(42, 15, '_menu_item_type', 'taxonomy'),
(43, 15, '_menu_item_menu_item_parent', '0'),
(44, 15, '_menu_item_object_id', '4'),
(45, 15, '_menu_item_object', 'category'),
(46, 15, '_menu_item_target', ''),
(47, 15, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(48, 15, '_menu_item_xfn', ''),
(49, 15, '_menu_item_url', ''),
(61, 17, '_edit_lock', '1432899959:1'),
(51, 16, '_menu_item_type', 'taxonomy'),
(52, 16, '_menu_item_menu_item_parent', '0'),
(53, 16, '_menu_item_object_id', '5'),
(54, 16, '_menu_item_object', 'category'),
(55, 16, '_menu_item_target', ''),
(56, 16, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(57, 16, '_menu_item_xfn', ''),
(58, 16, '_menu_item_url', ''),
(60, 17, '_edit_last', '1'),
(62, 17, '_wp_page_template', 'page-nosidebar.php'),
(63, 19, '_edit_last', '1'),
(64, 19, '_edit_lock', '1432901316:1'),
(65, 19, '_wp_page_template', 'default'),
(66, 21, '_edit_last', '1'),
(67, 21, '_edit_lock', '1433929873:1'),
(68, 21, '_wp_page_template', 'default'),
(69, 23, '_edit_last', '1'),
(70, 23, '_edit_lock', '1433162278:1'),
(71, 23, '_wp_page_template', 'page-nosidebar.php'),
(97, 29, '_menu_item_target', ''),
(96, 29, '_menu_item_object', 'custom'),
(95, 29, '_menu_item_object_id', '29'),
(94, 29, '_menu_item_menu_item_parent', '28'),
(93, 29, '_menu_item_type', 'custom'),
(176, 41, '_tml_action', 'logout'),
(83, 28, '_menu_item_type', 'custom'),
(84, 28, '_menu_item_menu_item_parent', '0'),
(85, 28, '_menu_item_object_id', '28'),
(86, 28, '_menu_item_object', 'custom'),
(87, 28, '_menu_item_target', ''),
(88, 28, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(89, 28, '_menu_item_xfn', ''),
(90, 28, '_menu_item_url', ''),
(92, 28, '_nav_menu_role', 'out'),
(98, 29, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(99, 29, '_menu_item_xfn', ''),
(100, 29, '_menu_item_url', 'http://127.0.0.1/docoeur/login'),
(111, 29, '_nav_menu_role', 'out'),
(102, 30, '_menu_item_type', 'custom'),
(103, 30, '_menu_item_menu_item_parent', '28'),
(104, 30, '_menu_item_object_id', '30'),
(105, 30, '_menu_item_object', 'custom'),
(106, 30, '_menu_item_target', ''),
(107, 30, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(108, 30, '_menu_item_xfn', ''),
(109, 30, '_menu_item_url', 'http://127.0.0.1/docoeur/register'),
(112, 30, '_nav_menu_role', 'out'),
(113, 31, '_menu_item_type', 'custom'),
(114, 31, '_menu_item_menu_item_parent', '0'),
(115, 31, '_menu_item_object_id', '31'),
(116, 31, '_menu_item_object', 'custom'),
(117, 31, '_menu_item_target', ''),
(118, 31, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(119, 31, '_menu_item_xfn', ''),
(120, 31, '_menu_item_url', ''),
(167, 31, '_nav_menu_role', 'in'),
(122, 32, '_menu_item_type', 'custom'),
(123, 32, '_menu_item_menu_item_parent', '31'),
(124, 32, '_menu_item_object_id', '32'),
(125, 32, '_menu_item_object', 'custom'),
(126, 32, '_menu_item_target', ''),
(127, 32, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(128, 32, '_menu_item_xfn', ''),
(129, 32, '_menu_item_url', 'http://127.0.0.1/docoeur/logout'),
(172, 32, '_nav_menu_role', 'in'),
(317, 129, '_wp_trash_meta_time', '1433834010'),
(316, 129, '_wp_trash_meta_status', 'publish'),
(314, 129, '_question', '128'),
(140, 34, '_menu_item_type', 'custom'),
(141, 34, '_menu_item_menu_item_parent', '31'),
(142, 34, '_menu_item_object_id', '34'),
(143, 34, '_menu_item_object', 'custom'),
(144, 34, '_menu_item_target', ''),
(145, 34, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(146, 34, '_menu_item_xfn', ''),
(147, 34, '_menu_item_url', 'http://127.0.0.1/docoeur/profil'),
(168, 34, '_nav_menu_role', 'in'),
(149, 35, '_menu_item_type', 'custom'),
(150, 35, '_menu_item_menu_item_parent', '31'),
(151, 35, '_menu_item_object_id', '35'),
(152, 35, '_menu_item_object', 'custom'),
(153, 35, '_menu_item_target', ''),
(154, 35, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(155, 35, '_menu_item_xfn', ''),
(156, 35, '_menu_item_url', 'http://127.0.0.1/docoeur/new-post'),
(169, 35, '_nav_menu_role', 'in'),
(158, 36, '_menu_item_type', 'custom'),
(159, 36, '_menu_item_menu_item_parent', '31'),
(160, 36, '_menu_item_object_id', '36'),
(161, 36, '_menu_item_object', 'custom'),
(162, 36, '_menu_item_target', ''),
(163, 36, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(164, 36, '_menu_item_xfn', ''),
(165, 36, '_menu_item_url', 'http://127.0.0.1/docoeur/wp-admin'),
(171, 36, '_nav_menu_role', 'a:2:{i:0;s:13:"administrator";i:1;s:10:"moderateur";}'),
(175, 40, '_tml_action', 'login'),
(178, 43, '_tml_action', 'lostpassword'),
(177, 42, '_tml_action', 'register'),
(179, 44, '_tml_action', 'resetpass'),
(181, 40, '_edit_lock', '1433934295:1'),
(182, 40, '_edit_last', '1'),
(183, 40, '_wp_page_template', 'page-nosidebar.php'),
(184, 42, '_edit_lock', '1432900288:1'),
(185, 42, '_edit_last', '1'),
(186, 42, '_wp_page_template', 'page-nosidebar.php'),
(187, 41, '_edit_last', '1'),
(188, 41, '_edit_lock', '1432900261:1'),
(189, 43, '_edit_last', '1'),
(190, 43, '_edit_lock', '1432901111:1'),
(191, 44, '_edit_last', '1'),
(192, 44, '_edit_lock', '1432901305:1'),
(244, 70, '_edit_lock', '1433319391:1'),
(245, 81, '_wp_attached_file', '2015/05/amandes.jpg'),
(196, 41, '_wp_page_template', 'page-nosidebar.php'),
(249, 70, '_dwqa_followers', '4'),
(248, 82, '_question', '70'),
(199, 63, '_wp_attached_file', '2015/05/omega3-differents.jpg'),
(200, 63, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:140;s:6:"height";i:223;s:4:"file";s:29:"2015/05/omega3-differents.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"omega3-differents-140x150.jpg";s:5:"width";i:140;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(201, 64, '_thumbnail_id', '63'),
(202, 64, 'wpuf_order_id', '49955686348ed2ee'),
(203, 64, '_edit_lock', '1432904557:1'),
(204, 64, '_edit_last', '1'),
(206, 66, '_wp_attached_file', '2015/05/6226_hypertension.jpg'),
(207, 66, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:283;s:4:"file";s:29:"2015/05/6226_hypertension.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"6226_hypertension-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"6226_hypertension-300x283.jpg";s:5:"width";i:300;s:6:"height";i:283;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";d:18;s:6:"credit";s:0:"";s:6:"camera";s:13:"Canon EOS-1DS";s:7:"caption";s:0:"";s:17:"created_timestamp";i:1148464436;s:9:"copyright";s:0:"";s:12:"focal_length";s:3:"131";s:3:"iso";s:3:"100";s:13:"shutter_speed";s:5:"0.008";s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(208, 67, '_thumbnail_id', '66'),
(209, 67, 'wpuf_order_id', '706556866f8b083e'),
(210, 67, '_edit_lock', '1433929632:3'),
(211, 67, '_edit_last', '1'),
(213, 69, '_menu_item_type', 'custom'),
(214, 69, '_menu_item_menu_item_parent', '31'),
(215, 69, '_menu_item_object_id', '69'),
(216, 69, '_menu_item_object', 'custom'),
(217, 69, '_menu_item_target', ''),
(218, 69, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(219, 69, '_menu_item_xfn', ''),
(220, 69, '_menu_item_url', 'http://127.0.0.1/docoeur/dashboard'),
(222, 69, '_nav_menu_role', 'in'),
(223, 70, '_dwqa_status', 'answered'),
(224, 70, '_dwqa_views', '186'),
(225, 70, '_dwqa_votes', '0'),
(226, 70, '_dwqa_answers_count', '0'),
(229, 72, '_question', '70'),
(231, 70, '_dwqa_followers', '2'),
(318, 130, '_wp_attached_file', '2015/06/Banniere2.jpg'),
(233, 74, '_wp_attached_file', '2015/06/Defenses-Tech-Info-CPNV-2015.pdf'),
(234, 17, '_oembed_6ae85143689e4619f6a967fe60bfa08e', '{{unknown}}'),
(246, 81, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:250;s:6:"height";i:350;s:4:"file";s:19:"2015/05/amandes.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"amandes-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"amandes-214x300.jpg";s:5:"width";i:214;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(247, 70, '_dwqa_answered_time', '1433321395'),
(250, 83, '_wp_attached_file', '2015/05/graphique.png'),
(251, 83, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:646;s:6:"height";i:350;s:4:"file";s:21:"2015/05/graphique.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"graphique-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:21:"graphique-300x163.png";s:5:"width";i:300;s:6:"height";i:163;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(252, 84, '_question', '70'),
(253, 70, '_dwqa_followers', '1'),
(254, 85, '_wp_attached_file', '2015/05/amandes1.jpg'),
(255, 85, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:250;s:6:"height";i:350;s:4:"file";s:20:"2015/05/amandes1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"amandes1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"amandes1-214x300.jpg";s:5:"width";i:214;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(265, 93, '_wp_attached_file', '2015/06/1432664173_sign-error.png'),
(266, 93, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:128;s:6:"height";i:128;s:4:"file";s:33:"2015/06/1432664173_sign-error.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(267, 92, '_edit_lock', '1433337170:3'),
(268, 95, '_edit_lock', '1433423347:1'),
(269, 96, '_edit_lock', '1433507091:1'),
(270, 96, '_edit_last', '1'),
(271, 96, '_wp_page_template', 'default'),
(272, 114, '_edit_lock', '1433426133:1'),
(273, 114, '_edit_last', '1'),
(274, 114, '_wp_page_template', 'default'),
(275, 118, '_menu_item_type', 'post_type'),
(276, 118, '_menu_item_menu_item_parent', '0'),
(277, 118, '_menu_item_object_id', '96'),
(278, 118, '_menu_item_object', 'page'),
(279, 118, '_menu_item_target', ''),
(280, 118, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(281, 118, '_menu_item_xfn', ''),
(282, 118, '_menu_item_url', ''),
(319, 130, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1500;s:6:"height";i:150;s:4:"file";s:21:"2015/06/Banniere2.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"Banniere2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"Banniere2-300x30.jpg";s:5:"width";i:300;s:6:"height";i:30;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:22:"Banniere2-1024x102.jpg";s:5:"width";i:1024;s:6:"height";i:102;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:1;}}'),
(308, 70, '_dwqa_best_answer', '82'),
(320, 131, '_wp_attached_file', '2015/06/cropped-Banniere2.jpg'),
(321, 131, '_wp_attachment_context', 'custom-header'),
(322, 131, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1500;s:6:"height";i:150;s:4:"file";s:29:"2015/06/cropped-Banniere2.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"cropped-Banniere2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:28:"cropped-Banniere2-300x30.jpg";s:5:"width";i:300;s:6:"height";i:30;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:30:"cropped-Banniere2-1024x102.jpg";s:5:"width";i:1024;s:6:"height";i:102;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(323, 131, '_wp_attachment_custom_header_last_used_base-wp-child', '1433854905'),
(324, 131, '_wp_attachment_is_custom_header', 'base-wp-child'),
(325, 132, '_wp_attached_file', '2015/06/wallpaper-1332251.jpg'),
(326, 132, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2560;s:6:"height";i:1440;s:4:"file";s:29:"2015/06/wallpaper-1332251.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"wallpaper-1332251-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"wallpaper-1332251-300x169.jpg";s:5:"width";i:300;s:6:"height";i:169;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:30:"wallpaper-1332251-1024x576.jpg";s:5:"width";i:1024;s:6:"height";i:576;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";d:2;s:6:"credit";s:15:"Andy Hutchinson";s:6:"camera";s:14:"Canon EOS 550D";s:7:"caption";s:0:"";s:17:"created_timestamp";i:1295640666;s:9:"copyright";s:15:"Andy Hutchinson";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"100";s:13:"shutter_speed";s:6:"0.0005";s:5:"title";s:0:"";s:11:"orientation";i:1;}}'),
(339, 140, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:76;s:6:"height";i:76;s:4:"file";s:28:"2015/06/apple-icon-76x76.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(334, 137, '_wp_attached_file', '2015/06/Evaluation-diplome-Exemple.pdf'),
(336, 139, '_wp_attached_file', '2015/06/apple-icon-57x57.png'),
(337, 139, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:57;s:6:"height";i:57;s:4:"file";s:28:"2015/06/apple-icon-57x57.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(338, 140, '_wp_attached_file', '2015/06/apple-icon-76x76.png'),
(329, 134, '_thumbnail_id', '132'),
(330, 134, 'wpuf_order_id', '9635577f4fe62d43'),
(331, 134, '_edit_lock', '1434024210:1'),
(332, 134, '_edit_last', '1'),
(340, 141, '_wp_attached_file', '2015/06/apple-icon-120x120.png'),
(341, 141, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:120;s:6:"height";i:120;s:4:"file";s:30:"2015/06/apple-icon-120x120.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(342, 142, '_wp_attached_file', '2015/06/apple-icon-144x144.png'),
(343, 142, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:144;s:6:"height";i:144;s:4:"file";s:30:"2015/06/apple-icon-144x144.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(344, 143, '_wp_attached_file', '2015/06/apple-icon-152x152.png'),
(345, 143, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:152;s:6:"height";i:152;s:4:"file";s:30:"2015/06/apple-icon-152x152.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:30:"apple-icon-152x152-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(346, 144, '_wp_attached_file', '2015/06/favicon.ico'),
(347, 145, '_wp_attached_file', '2015/06/Favicon.jpg'),
(348, 145, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:344;s:6:"height";i:344;s:4:"file";s:19:"2015/06/Favicon.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"Favicon-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"Favicon-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:1;}}'),
(349, 146, '_wp_attached_file', '2015/06/Hydrangeas.jpg'),
(350, 146, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:22:"2015/06/Hydrangeas.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"Hydrangeas-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"Hydrangeas-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"Hydrangeas-1024x768.jpg";s:5:"width";i:1024;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:11:"Amish Patel";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:1206376913;s:9:"copyright";s:24:"© Microsoft Corporation";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(351, 147, '_wp_attached_file', '2015/06/valves-cardiaques.jpg'),
(352, 147, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:380;s:6:"height";i:338;s:4:"file";s:29:"2015/06/valves-cardiaques.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"valves-cardiaques-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"valves-cardiaques-300x267.jpg";s:5:"width";i:300;s:6:"height";i:267;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(353, 148, 'wpuf_order_id', '721557846a872c66'),
(354, 148, '_edit_lock', '1433945806:4'),
(355, 148, '_edit_last', '4'),
(357, 150, '_dwqa_status', 'answered'),
(358, 150, '_dwqa_views', '29'),
(359, 150, '_dwqa_votes', '0'),
(360, 150, '_dwqa_answers_count', '0'),
(361, 150, '_dwqa_answered_time', '1433946561'),
(364, 153, '_question', '150'),
(365, 150, '_dwqa_followers', '4'),
(367, 17, '_oembed_5d962da040e384f8b66043884cd8ef90', '<iframe width="700" height="394" src="https://www.youtube.com/embed/xe1LrMqURuw?feature=oembed" frameborder="0" allowfullscreen></iframe>'),
(368, 17, '_oembed_time_5d962da040e384f8b66043884cd8ef90', '1433971485'),
(369, 150, '_dwqa_best_answer', '153');

-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_posts`
--

CREATE TABLE IF NOT EXISTS `dcHyv_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=154 ;

--
-- Contenu de la table `dcHyv_posts`
--

INSERT INTO `dcHyv_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(21, 1, '2015-05-28 09:52:35', '2015-05-28 08:52:35', '[wpuf_editprofile]', 'Profil', '', 'publish', 'closed', 'closed', '', 'profile', '', '', '2015-05-29 07:57:31', '2015-05-29 06:57:31', '', 0, 'http://127.0.0.1/docoeur/?page_id=21', 0, 'page', '', 0),
(8, 1, '2015-05-27 12:55:03', '2015-05-27 11:55:03', '[dwqa-list-questions]', 'Questions', '', 'publish', 'open', 'open', '', 'questions', '', '', '2015-05-29 12:48:34', '2015-05-29 11:48:34', '', 0, 'http://127.0.0.1/docoeur/index.php/dwqa-questions/', 0, 'page', '', 0),
(9, 1, '2015-05-27 12:55:03', '2015-05-27 11:55:03', '[dwqa-submit-question-form]', 'Poser une Question', '', 'publish', 'open', 'open', '', 'poser-une-question', '', '', '2015-05-27 14:54:47', '2015-05-27 13:54:47', '', 0, 'http://127.0.0.1/docoeur/index.php/dwqa-ask-question/', 0, 'page', '', 0),
(10, 1, '2015-05-27 13:44:08', '2015-05-27 12:44:08', 'Accueil description', '<i class="fa fa-home fa-2x"></i>', '', 'publish', 'open', 'open', '', 'accueil', '', '', '2015-06-08 09:17:55', '2015-06-08 08:17:55', '', 0, 'http://127.0.0.1/docoeur/?p=10', 1, 'nav_menu_item', '', 0),
(11, 1, '2015-05-27 13:43:32', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'open', 'open', '', '', '', '', '2015-05-27 13:43:32', '0000-00-00 00:00:00', '', 0, 'http://127.0.0.1/docoeur/?p=11', 1, 'nav_menu_item', '', 0),
(12, 1, '2015-05-27 13:44:08', '2015-05-27 12:44:08', '', '<i class="fa fa-question-circle"></i> Questions', '', 'publish', 'open', 'open', '', 'questions', '', '', '2015-06-08 09:17:55', '2015-06-08 08:17:55', '', 0, 'http://127.0.0.1/docoeur/?p=12', 2, 'nav_menu_item', '', 0),
(13, 1, '2015-05-27 14:54:47', '2015-05-27 13:54:47', '[dwqa-submit-question-form]', 'Poser une Question', '', 'inherit', 'open', 'open', '', '9-revision-v1', '', '', '2015-05-27 14:54:47', '2015-05-27 13:54:47', '', 9, 'http://127.0.0.1/docoeur/index.php/2015/05/27/9-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2015-05-27 14:55:16', '2015-05-27 13:55:16', '[dwqa-list-questions]', 'Questions', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2015-05-27 14:55:16', '2015-05-27 13:55:16', '', 8, 'http://127.0.0.1/docoeur/index.php/2015/05/27/8-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2015-05-29 12:28:58', '2015-05-29 11:28:58', '[theme-my-login-page show_reg_link=0]', 'Log In', '', 'inherit', 'open', 'open', '', '40-revision-v1', '', '', '2015-05-29 12:28:58', '2015-05-29 11:28:58', '', 40, 'http://127.0.0.1/docoeur/2015/05/29/40-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2015-05-28 09:51:59', '2015-05-28 08:51:59', '[wpuf_edit]', 'Modification', '', 'publish', 'closed', 'closed', '', 'edit', '', '', '2015-05-28 09:51:59', '2015-05-28 08:51:59', '', 0, 'http://127.0.0.1/docoeur/?page_id=19', 0, 'page', '', 0),
(20, 1, '2015-05-28 09:51:59', '2015-05-28 08:51:59', '[wpuf_edit]', 'Modification', '', 'inherit', 'open', 'open', '', '19-revision-v1', '', '', '2015-05-28 09:51:59', '2015-05-28 08:51:59', '', 19, 'http://127.0.0.1/docoeur/index.php/2015/05/28/19-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2015-05-27 15:01:24', '2015-05-27 14:01:24', 'Contient les articles écrits par les spécialistes', '<i class="fa fa-stethoscope"></i> Espace Spécialiste', '', 'publish', 'open', 'open', '', 'espace-specialiste', '', '', '2015-06-08 09:17:55', '2015-06-08 08:17:55', '', 0, 'http://127.0.0.1/docoeur/?p=15', 3, 'nav_menu_item', '', 0),
(16, 1, '2015-05-27 15:01:24', '2015-05-27 14:01:24', 'Contient les articles écrits par les Membres et les Spécialistes', '<i class="fa fa-heartbeat"></i> Récits vécus', '', 'publish', 'open', 'open', '', '16', '', '', '2015-06-08 09:17:55', '2015-06-08 08:17:55', '', 0, 'http://127.0.0.1/docoeur/?p=16', 4, 'nav_menu_item', '', 0),
(17, 1, '2015-05-28 09:49:35', '2015-05-28 08:49:35', '[wpuf_addpost]', 'Nouvelle Publication', '', 'publish', 'closed', 'closed', '', 'new-post', '', '', '2015-05-29 12:47:32', '2015-05-29 11:47:32', '', 0, 'http://127.0.0.1/docoeur/?page_id=17', 0, 'page', '', 0),
(18, 1, '2015-05-28 09:49:35', '2015-05-28 08:49:35', '[wpuf_addpost]', 'Nouvelle Publication', '', 'inherit', 'open', 'open', '', '17-revision-v1', '', '', '2015-05-28 09:49:35', '2015-05-28 08:49:35', '', 17, 'http://127.0.0.1/docoeur/index.php/2015/05/28/17-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2015-05-28 09:52:35', '2015-05-28 08:52:35', '[wpuf_editprofile]', 'Profil', '', 'inherit', 'open', 'open', '', '21-revision-v1', '', '', '2015-05-28 09:52:35', '2015-05-28 08:52:35', '', 21, 'http://127.0.0.1/docoeur/index.php/2015/05/28/21-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2015-05-28 09:53:03', '2015-05-28 08:53:03', '[wpuf_dashboard]', 'Tableau de bord', '', 'publish', 'closed', 'closed', '', 'dashboard', '', '', '2015-06-01 13:37:58', '2015-06-01 12:37:58', '', 0, 'http://127.0.0.1/docoeur/?page_id=23', 0, 'page', '', 0),
(24, 1, '2015-05-28 09:53:03', '2015-05-28 08:53:03', '[wpuf_dashboard]', 'Tableau de bord', '', 'inherit', 'open', 'open', '', '23-revision-v1', '', '', '2015-05-28 09:53:03', '2015-05-28 08:53:03', '', 23, 'http://127.0.0.1/docoeur/index.php/2015/05/28/23-revision-v1/', 0, 'revision', '', 0),
(95, 1, '2015-06-04 10:03:51', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-04 10:03:51', '0000-00-00 00:00:00', '', 0, 'http://127.0.0.1/docoeur/?p=95', 0, 'post', '', 0),
(29, 1, '2015-05-28 10:54:34', '2015-05-28 09:54:34', '', '<i class="fa fa-user"></i> Se Connecter', '', 'publish', 'open', 'open', '', 'se-connecter', '', '', '2015-06-08 09:17:55', '2015-06-08 08:17:55', '', 0, 'http://127.0.0.1/docoeur/?p=29', 7, 'nav_menu_item', '', 0),
(28, 1, '2015-05-28 10:52:57', '2015-05-28 09:52:57', '', '<i class="fa fa-lock"></i> Compte', '', 'publish', 'open', 'open', '', 'compte', '', '', '2015-06-08 09:17:55', '2015-06-08 08:17:55', '', 0, 'http://127.0.0.1/docoeur/?p=28', 6, 'nav_menu_item', '', 0),
(30, 1, '2015-05-28 10:54:34', '2015-05-28 09:54:34', '', '<i class="fa fa-plus-circle"></i> Créer un Compte', '', 'publish', 'open', 'open', '', 'creer-un-compte', '', '', '2015-06-08 09:17:55', '2015-06-08 08:17:55', '', 0, 'http://127.0.0.1/docoeur/?p=30', 8, 'nav_menu_item', '', 0),
(31, 1, '2015-05-28 10:58:39', '2015-05-28 09:58:39', '', '<i class="fa fa-unlock-alt"></i> Compte', '', 'publish', 'open', 'open', '', 'compte-2', '', '', '2015-06-08 09:17:55', '2015-06-08 08:17:55', '', 0, 'http://127.0.0.1/docoeur/?p=31', 9, 'nav_menu_item', '', 0),
(32, 1, '2015-05-28 10:58:39', '2015-05-28 09:58:39', '', '<i class="fa fa-sign-out"></i> Déconnexion', '', 'publish', 'open', 'open', '', 'deconnexion', '', '', '2015-06-08 09:17:55', '2015-06-08 08:17:55', '', 0, 'http://127.0.0.1/docoeur/?p=32', 14, 'nav_menu_item', '', 0),
(34, 1, '2015-05-28 10:58:39', '2015-05-28 09:58:39', '', '<i class="fa fa-user"></i> Profil - [current-username]', '', 'publish', 'open', 'open', '', 'profil', '', '', '2015-06-08 09:17:55', '2015-06-08 08:17:55', '', 0, 'http://127.0.0.1/docoeur/?p=34', 10, 'nav_menu_item', '', 0),
(35, 1, '2015-05-28 10:58:39', '2015-05-28 09:58:39', '', '<i class="fa fa-pencil-square-o"></i> Nouvelle Publication', '', 'publish', 'open', 'open', '', 'nouvelle-publication', '', '', '2015-06-08 09:17:55', '2015-06-08 08:17:55', '', 0, 'http://127.0.0.1/docoeur/?p=35', 11, 'nav_menu_item', '', 0),
(36, 1, '2015-05-28 10:58:39', '2015-05-28 09:58:39', '', '<i class="fa fa-cogs"></i> Tableau de Bord', '', 'publish', 'open', 'open', '', 'tableau-de-bord', '', '', '2015-06-08 09:17:55', '2015-06-08 08:17:55', '', 0, 'http://127.0.0.1/docoeur/?p=36', 13, 'nav_menu_item', '', 0),
(40, 1, '2015-05-29 08:07:07', '2015-05-29 07:07:07', '[theme-my-login show_reg_link=0 show_pass_link=0 show_log_link=0]', 'Connexion', '', 'publish', 'closed', 'closed', '', 'login', '', '', '2015-05-29 12:50:50', '2015-05-29 11:50:50', '', 0, 'http://127.0.0.1/docoeur/login/', 0, 'page', '', 0),
(38, 1, '2015-05-29 07:53:44', '2015-05-29 06:53:44', '[wpuf_editprofile]\r\n\r\n[current-username]', 'Profil', '', 'inherit', 'open', 'open', '', '21-revision-v1', '', '', '2015-05-29 07:53:44', '2015-05-29 06:53:44', '', 21, 'http://127.0.0.1/docoeur/2015/05/29/21-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2015-05-29 07:57:31', '2015-05-29 06:57:31', '[wpuf_editprofile]', 'Profil', '', 'inherit', 'open', 'open', '', '21-revision-v1', '', '', '2015-05-29 07:57:31', '2015-05-29 06:57:31', '', 21, 'http://127.0.0.1/docoeur/2015/05/29/21-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2015-05-29 08:07:07', '2015-05-29 07:07:07', '[theme-my-login]', 'Déconnexion', '', 'publish', 'closed', 'closed', '', 'logout', '', '', '2015-05-29 12:51:01', '2015-05-29 11:51:01', '', 0, 'http://127.0.0.1/docoeur/logout/', 0, 'page', '', 0),
(42, 1, '2015-05-29 08:07:07', '2015-05-29 07:07:07', '[theme-my-login show_log_link=0 show_pass_link=0]', 'Créer un Compte', '', 'publish', 'closed', 'closed', '', 'register', '', '', '2015-05-29 12:51:28', '2015-05-29 11:51:28', '', 0, 'http://127.0.0.1/docoeur/register/', 0, 'page', '', 0),
(43, 1, '2015-05-29 08:07:07', '2015-05-29 07:07:07', '[theme-my-login]', 'Mot de Passe Perdu', '', 'publish', 'closed', 'closed', '', 'lostpassword', '', '', '2015-05-29 12:35:10', '2015-05-29 11:35:10', '', 0, 'http://127.0.0.1/docoeur/lostpassword/', 0, 'page', '', 0),
(44, 1, '2015-05-29 08:07:07', '2015-05-29 07:07:07', '[theme-my-login]', 'Réinitialiser le Mot de Passe', '', 'publish', 'closed', 'closed', '', 'resetpass', '', '', '2015-05-29 12:35:55', '2015-05-29 11:35:55', '', 0, 'http://127.0.0.1/docoeur/resetpass/', 0, 'page', '', 0),
(91, 3, '2015-06-03 14:02:52', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-03 14:02:52', '0000-00-00 00:00:00', '', 0, 'http://127.0.0.1/docoeur/?p=91', 0, 'post', '', 0),
(92, 3, '2015-06-03 14:02:57', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-03 14:02:57', '0000-00-00 00:00:00', '', 0, 'http://127.0.0.1/docoeur/?p=92', 0, 'post', '', 0),
(47, 1, '2015-05-29 12:29:26', '2015-05-29 11:29:26', '[theme-my-login show_reg_link=0]', 'Log In', '', 'inherit', 'open', 'open', '', '40-revision-v1', '', '', '2015-05-29 12:29:26', '2015-05-29 11:29:26', '', 40, 'http://127.0.0.1/docoeur/2015/05/29/40-revision-v1/', 0, 'revision', '', 0),
(48, 1, '2015-05-29 12:30:30', '2015-05-29 11:30:30', '[theme-my-login show_reg_link=0 show_]', 'Log In', '', 'inherit', 'open', 'open', '', '40-autosave-v1', '', '', '2015-05-29 12:30:30', '2015-05-29 11:30:30', '', 40, 'http://127.0.0.1/docoeur/2015/05/29/40-autosave-v1/', 0, 'revision', '', 0),
(49, 1, '2015-05-29 12:31:39', '2015-05-29 11:31:39', '[theme-my-login show_reg_link=0 show_pass_link=0]', 'Log In', '', 'inherit', 'open', 'open', '', '40-revision-v1', '', '', '2015-05-29 12:31:39', '2015-05-29 11:31:39', '', 40, 'http://127.0.0.1/docoeur/2015/05/29/40-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2015-05-29 12:32:16', '2015-05-29 11:32:16', '[theme-my-login show_reg_link=0 show_pass_link=0]', 'Register', '', 'inherit', 'open', 'open', '', '42-revision-v1', '', '', '2015-05-29 12:32:16', '2015-05-29 11:32:16', '', 42, 'http://127.0.0.1/docoeur/2015/05/29/42-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2015-05-29 12:32:46', '2015-05-29 11:32:46', '[theme-my-login show_log_link=0 show_pass_link=0]', 'Register', '', 'inherit', 'open', 'open', '', '42-revision-v1', '', '', '2015-05-29 12:32:46', '2015-05-29 11:32:46', '', 42, 'http://127.0.0.1/docoeur/2015/05/29/42-revision-v1/', 0, 'revision', '', 0),
(51, 1, '2015-05-29 12:32:33', '2015-05-29 11:32:33', '[theme-my-login show_login_link=0 show_pass_link=0]', 'Register', '', 'inherit', 'open', 'open', '', '42-revision-v1', '', '', '2015-05-29 12:32:33', '2015-05-29 11:32:33', '', 42, 'http://127.0.0.1/docoeur/2015/05/29/42-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2015-05-29 12:34:00', '2015-05-29 11:34:00', '[theme-my-login show_reg_link=0 show_pass_link=0 show_log_link=0]', 'Log In', '', 'inherit', 'open', 'open', '', '40-revision-v1', '', '', '2015-05-29 12:34:00', '2015-05-29 11:34:00', '', 40, 'http://127.0.0.1/docoeur/2015/05/29/40-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2015-05-29 12:34:40', '2015-05-29 11:34:40', '[theme-my-login show_reg_link=0 show_pass_link=0 show_log_link=0]', 'Connexion', '', 'inherit', 'open', 'open', '', '40-revision-v1', '', '', '2015-05-29 12:34:40', '2015-05-29 11:34:40', '', 40, 'http://127.0.0.1/docoeur/2015/05/29/40-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2015-05-29 12:34:52', '2015-05-29 11:34:52', '[theme-my-login]', 'Déconnexion', '', 'inherit', 'open', 'open', '', '41-revision-v1', '', '', '2015-05-29 12:34:52', '2015-05-29 11:34:52', '', 41, 'http://127.0.0.1/docoeur/2015/05/29/41-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2015-05-29 12:35:10', '2015-05-29 11:35:10', '[theme-my-login]', 'Mot de Passe Perdu', '', 'inherit', 'open', 'open', '', '43-revision-v1', '', '', '2015-05-29 12:35:10', '2015-05-29 11:35:10', '', 43, 'http://127.0.0.1/docoeur/2015/05/29/43-revision-v1/', 0, 'revision', '', 0),
(57, 1, '2015-05-29 12:35:35', '2015-05-29 11:35:35', '[theme-my-login show_log_link=0 show_pass_link=0]', 'Créer un Compte', '', 'inherit', 'open', 'open', '', '42-revision-v1', '', '', '2015-05-29 12:35:35', '2015-05-29 11:35:35', '', 42, 'http://127.0.0.1/docoeur/2015/05/29/42-revision-v1/', 0, 'revision', '', 0),
(58, 1, '2015-05-29 12:35:55', '2015-05-29 11:35:55', '[theme-my-login]', 'Réinitialiser le Mot de Passe', '', 'inherit', 'open', 'open', '', '44-revision-v1', '', '', '2015-05-29 12:35:55', '2015-05-29 11:35:55', '', 44, 'http://127.0.0.1/docoeur/2015/05/29/44-revision-v1/', 0, 'revision', '', 0),
(89, 3, '2015-06-03 10:31:49', '2015-06-03 09:31:49', '<strong>Alors que les dégâts du tabagisme restent surtout associés à la survenue de cancers, son influence sur le système cardiovasculaire est très importante. Le tabac est ainsi tenu pour responsable de 24 % des décès dans ce domaine. C''est donc un facteur particulièrement aggravant pour les personnes souffrant d''hypertension artérielle.</strong>\r\n<div class="doc-block-pub doc-block-pub-middle"></div>\r\n<img class=" doc-right alignleft" title="Hypertension et tabagisme" src="http://cdn3-doctissimo.ladmedia.fr/var/doctissimo/storage/images/media/images/fr/www/hta_tabac/594885-1-fre-FR/hta_tabac.jpg" alt="Hypertension et tabagisme" width="136" height="189" />Les Français ne connaissent pas bien l''impact de la consommation régulière de tabac sur le risque cardiovasculaire. Même les gros fumeurs semblent être mal informés. Selon un sondage réalisé par le Comité Français de Lutte contre l''Hypertension artérielle, 74 % des hypertendus pensent que le stress est l''unique cause de leurs problèmes de santé. Pire encore, des études concordantes révèlent que près de la moitié des fumeurs poursuivent leur consommation de tabac alors qu''ils souffrent d''hypertension artérielle (HTA), voire même après avoir subi un pontage ou une angioplastie coronarienne.\r\n<h2>Une prise de risque importante</h2>\r\nLes risques ne sont pourtant pas négligeables. L''American Heart Association (Association américaine contre les maladies cardiovasculaires) estime qu''aux Etats-Unis, 30 % des morts liés à l''insuffisance coronaire sont liés au tabagisme. L''âge et la dose inhalée influent directement sur l''importance du risque. Pour moins de 5 cigarettes par jour, le risque est multiplié par 1,4 ; au-delà de 10 cigarettes/jour par 2,4 et par 2,8 entre 20 et 40 cigarettes par jour, tandis qu''il n''est que de 0,7 pour un non-fumeur.\r\n\r\nLe tabagisme passif a également des effets néfastes. De 1982 à 1992, des scientifiques américains <sup>1</sup> ont suivi 32 000 femmes qui n''avaient jamais fumé et n''avaient jamais été victimes d''une maladie cardiovasculaire, d''une attaque cardiaque ou d''un cancer. Les résultats démontrent que l''exposition au tabagisme passif au domicile ou au travail multiplie par près de deux, le risque de développer une maladie coronarienne.\r\n<h2>Une augmentation de la pression artérielle</h2>\r\nLa nicotine agit directement sur les artères en augmentant de façon transitoire la pression artérielle et la fréquence cardiaque. Ce rétrécissement des artères est particulièrement perceptible lors de la première cigarette de la journée. L''effet se dissipe ensuite au bout d''une trentaine de minutes, mais la pression augmente progressivement tout au long de la journée, pour retrouver son niveau de base pendant le sommeil. La baisse de tension serait cependant moins marquée chez les fumeurs normotendus (c''est-à-dire ne souffrant pas d''HTA), que chez les non-fumeurs <sup>2</sup>.\r\n<h2>Jamais trop tard pour arrêter</h2>\r\nL''arrêt du tabac fait baisser petit à petit le risque coronarien, mais trois ans sont nécessaires pour retrouver le niveau d''un non-fumeur. Ce délai peut paraître long mais l''effort est payant.', 'Cigarette et hypertension : pas de fumée sans feu', '', 'inherit', 'open', 'open', '', '67-revision-v1', '', '', '2015-06-03 10:31:49', '2015-06-03 09:31:49', '', 67, 'http://127.0.0.1/docoeur/2015/06/03/67-revision-v1/', 0, 'revision', '', 0),
(85, 3, '2015-06-03 10:01:11', '2015-06-03 09:01:11', '', 'amandes', '', 'inherit', 'open', 'open', '', 'amandes-2', '', '', '2015-06-03 10:01:11', '2015-06-03 09:01:11', '', 70, 'http://127.0.0.1/docoeur/wp-content/uploads/2015/05/amandes1.jpg', 0, 'attachment', 'image/jpeg', 0),
(76, 4, '2015-06-03 08:09:23', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-03 08:09:23', '0000-00-00 00:00:00', '', 0, 'http://127.0.0.1/docoeur/?p=76', 0, 'post', '', 0),
(81, 4, '2015-06-03 09:19:54', '2015-06-03 08:19:54', '', 'amandes', '', 'inherit', 'open', 'open', '', 'amandes', '', '', '2015-06-03 09:19:54', '2015-06-03 08:19:54', '', 70, 'http://127.0.0.1/docoeur/wp-content/uploads/2015/05/amandes.jpg', 0, 'attachment', 'image/jpeg', 0),
(82, 4, '2015-06-03 09:20:31', '2015-06-03 08:20:31', 'Plusieurs études cliniques[1]-[2] ont démontré les effets de la consommation d’amandes sur la diminution de la concentration de <strong>cholestérol sanguin</strong>2, en particulier sur le cholestérol LDL (« mauvais » cholestérol). Selon des données épidémiologiques[3], une consommation quotidienne de 30 g d’amandes pourrait réduire de 45 % le risque de <strong>maladies cardiovasculaires</strong> lorsque ces aliments remplacent des aliments riches en acides gras saturés.<br /> <img src="http://127.0.0.1/docoeur/wp-content/uploads/2015/05/amandes-214x300.jpg" alt="amandes" width="134" height="187" />', 'répondreQuels sont les aliments bons et les aliments mauvais pour le coeur ?', '', 'publish', 'open', 'open', '', 'repondrequels-sont-les-aliments-bons-et-les-aliments-mauvais-pour-le-coeur', '', '', '2015-06-03 09:21:57', '2015-06-03 08:21:57', '', 0, 'http://127.0.0.1/docoeur/dwqa-answer/repondrequels-sont-les-aliments-bons-et-les-aliments-mauvais-pour-le-coeur/', 0, 'dwqa-answer', '', 0),
(63, 2, '2015-05-29 14:01:51', '2015-05-29 13:01:51', '', 'omega3-differents', '', 'inherit', 'open', 'open', '', 'omega3-differents', '', '', '2015-05-29 14:02:00', '2015-05-29 13:02:00', '', 64, 'http://127.0.0.1/docoeur/?attachment_id=63', 0, 'attachment', 'image/jpeg', 0),
(64, 2, '2015-05-29 14:03:40', '2015-05-29 13:03:40', '<strong>On parle beaucoup des oméga 3 et les bénéfices les plus fantaisistes leur sont souvent attribués. En réalité, les oméga 3 ont surtout un rôle fondamental dans la prévention des maladies cardiovasculaires, et de nombreuses études le prouvent. Tour d''horizon.</strong>\r\n<h2>La découverte de l''intérêt des oméga 3</h2>\r\n<img class=" doc-right alignright" title="Oméga 3 - Santé" src="http://cdn2-doctissimo.ladmedia.fr/var/doctissimo/storage/images/media/images/fr/www/omega3_allies_sante/521859-1-fre-FR/omega3_allies_sante.jpg" alt="Oméga 3 - Santé" width="135" height="184" />Récente ? La grande mode des oméga 3 ne date pourtant pas d''hier. Le point de départ de cette découverte est tout simplement basé sur les habitudes des esquimaux. Les scientifiques ont constaté que ces derniers avaient un taux d''infarctus et autres maladies cardiovasculaires extrêmement bas. En recherchant dans leur environnement direct la cause de cette excellente santé, leur regard s''est rapidement porté sur leur alimentation et plus particulièrement sur la consommation de poisson et d''huiles de poisson. Il a alors été démontré que les heureux responsables étaient les oméga 3\r\n\r\nLa confirmation des bienfaits des oméga 3 sur la santé cardiovasculaire se retrouve dans l''analyse d''un groupe de pêcheurs japonais de l''île d''Okinawa <sup>1</sup>qui consomment de grandes quantités de ces poissons mais aussi chez les adeptes du régime crétois. Une grande étude d''intervention en prévention secondaire d''infarctus, réalisée sur 10 ans entre 1988 et 1997 à Lyon <sup>2</sup>, a permis de comparer deux groupes à l''alimentation bien différente. Le groupe qui avait mangé à la "crétoise" c''est-à-dire avec un apport de matières grasses composé d''huile d''olive et de margarine à base d''huile de colza a vu, en 27 mois, une réduction de 73% des événements coronariens récidivants et des décès cardiaques. Cette étude confirme de manière spectaculaire le rôle protecteur de l''ALA.\r\n<h2>Mais comment agissent-ils sur la bonne santé du système cardiovasculaire ?</h2>\r\nLes oméga 3 ont un effet bénéfique sur notre système cardiovasculaire, plus personne n''en doute mais comment cela fonctionne-t-il ? Dans le rapport de l''AFSSA <sup>3</sup>, les études scientifiques sont passées au crible. On y démontre que l''enrichissement en oméga 3 peut conduire à une diminution de la pression artérielle chez les personnes qui souffrent d''hypertension mais aussi à une diminution du taux de triglycérides dans le sang (chez les patients avec un taux trop élevé) .\r\n\r\nEt les études qui confirment ces bons résultats sont nombreuses : en 2000, on démontrait qu''une concentration élevée d''oméga 3 dans le sang abaissait de 44 % le risque d''accident coronarien <sup>4</sup> en prévention primaire. Les oméga 3 auraient également un effet bénéfique sur le rythme cardiaque et de la fluidification du sang. Ce qui est certain c''est la consommation d''oméga 3 est un véritable geste de prévention.\r\n<h2>Consommation d''oméga 3, où en sommes nous en France ?</h2>\r\nL''avis de l''AFSSA est clair : le niveau de consommation d''oméga 3 des Français est trop bas. Deux études, INCA et SU.VI.MAX, ont servi d''appui à la rédaction du rapport, il en ressort que l''apport moyen en acide alpha-linolénique est très faible (0,1 à 0,4 % de l''apport énergétique total) et plus alarmant encore que la quasi totalité des individus ont des apports insuffisants pour couvrir les besoins quotidiens (qui se situent à 0,8 % de l''apport énergétique total). La conclusion du rapport vient alors tout naturellement : il faut augmenter notre consommation d''oméga 3.\r\n\r\nPour y parvenir, c''est simple, les huiles de soja ou de colza, les margarines riches en oméga 3 ou encore le poisson gras doivent trouver ou retrouver le chemin de notre assiette régulièrement.\r\n\r\n<dl class="doc-auteur"><dt>Juliette Lauzanne - Doctissomo.fr</dt></dl>', 'Les oméga 3, de nombreux atouts pour votre santé cardiovasculaire', '', 'publish', 'open', 'open', '', 'les-omega-3-de-nombreux-atouts-pour-votre-sante-cardiovasculaire', '', '', '2015-05-29 14:03:40', '2015-05-29 13:03:40', '', 0, 'http://127.0.0.1/docoeur/?p=64', 0, 'post', '', 3),
(65, 1, '2015-05-29 14:03:40', '2015-05-29 13:03:40', '<strong>On parle beaucoup des oméga 3 et les bénéfices les plus fantaisistes leur sont souvent attribués. En réalité, les oméga 3 ont surtout un rôle fondamental dans la prévention des maladies cardiovasculaires, et de nombreuses études le prouvent. Tour d''horizon.</strong>\r\n<h2>La découverte de l''intérêt des oméga 3</h2>\r\n<img class=" doc-right alignright" title="Oméga 3 - Santé" src="http://cdn2-doctissimo.ladmedia.fr/var/doctissimo/storage/images/media/images/fr/www/omega3_allies_sante/521859-1-fre-FR/omega3_allies_sante.jpg" alt="Oméga 3 - Santé" width="135" height="184" />Récente ? La grande mode des oméga 3 ne date pourtant pas d''hier. Le point de départ de cette découverte est tout simplement basé sur les habitudes des esquimaux. Les scientifiques ont constaté que ces derniers avaient un taux d''infarctus et autres maladies cardiovasculaires extrêmement bas. En recherchant dans leur environnement direct la cause de cette excellente santé, leur regard s''est rapidement porté sur leur alimentation et plus particulièrement sur la consommation de poisson et d''huiles de poisson. Il a alors été démontré que les heureux responsables étaient les oméga 3\r\n\r\nLa confirmation des bienfaits des oméga 3 sur la santé cardiovasculaire se retrouve dans l''analyse d''un groupe de pêcheurs japonais de l''île d''Okinawa <sup>1</sup>qui consomment de grandes quantités de ces poissons mais aussi chez les adeptes du régime crétois. Une grande étude d''intervention en prévention secondaire d''infarctus, réalisée sur 10 ans entre 1988 et 1997 à Lyon <sup>2</sup>, a permis de comparer deux groupes à l''alimentation bien différente. Le groupe qui avait mangé à la "crétoise" c''est-à-dire avec un apport de matières grasses composé d''huile d''olive et de margarine à base d''huile de colza a vu, en 27 mois, une réduction de 73% des événements coronariens récidivants et des décès cardiaques. Cette étude confirme de manière spectaculaire le rôle protecteur de l''ALA.\r\n<h2>Mais comment agissent-ils sur la bonne santé du système cardiovasculaire ?</h2>\r\nLes oméga 3 ont un effet bénéfique sur notre système cardiovasculaire, plus personne n''en doute mais comment cela fonctionne-t-il ? Dans le rapport de l''AFSSA <sup>3</sup>, les études scientifiques sont passées au crible. On y démontre que l''enrichissement en oméga 3 peut conduire à une diminution de la pression artérielle chez les personnes qui souffrent d''hypertension mais aussi à une diminution du taux de triglycérides dans le sang (chez les patients avec un taux trop élevé) .\r\n\r\nEt les études qui confirment ces bons résultats sont nombreuses : en 2000, on démontrait qu''une concentration élevée d''oméga 3 dans le sang abaissait de 44 % le risque d''accident coronarien <sup>4</sup> en prévention primaire. Les oméga 3 auraient également un effet bénéfique sur le rythme cardiaque et de la fluidification du sang. Ce qui est certain c''est la consommation d''oméga 3 est un véritable geste de prévention.\r\n<h2>Consommation d''oméga 3, où en sommes nous en France ?</h2>\r\nL''avis de l''AFSSA est clair : le niveau de consommation d''oméga 3 des Français est trop bas. Deux études, INCA et SU.VI.MAX, ont servi d''appui à la rédaction du rapport, il en ressort que l''apport moyen en acide alpha-linolénique est très faible (0,1 à 0,4 % de l''apport énergétique total) et plus alarmant encore que la quasi totalité des individus ont des apports insuffisants pour couvrir les besoins quotidiens (qui se situent à 0,8 % de l''apport énergétique total). La conclusion du rapport vient alors tout naturellement : il faut augmenter notre consommation d''oméga 3.\r\n\r\nPour y parvenir, c''est simple, les huiles de soja ou de colza, les margarines riches en oméga 3 ou encore le poisson gras doivent trouver ou retrouver le chemin de notre assiette régulièrement.\r\n\r\n<dl class="doc-auteur"><dt>Juliette Lauzanne - Doctissomo.fr</dt></dl>', 'Les oméga 3, de nombreux atouts pour votre santé cardiovasculaire', '', 'inherit', 'open', 'open', '', '64-revision-v1', '', '', '2015-05-29 14:03:40', '2015-05-29 13:03:40', '', 64, 'http://127.0.0.1/docoeur/2015/05/29/64-revision-v1/', 0, 'revision', '', 0),
(66, 3, '2015-05-29 14:16:02', '2015-05-29 13:16:02', '', '6226_hypertension', '', 'inherit', 'open', 'open', '', '6226_hypertension', '', '', '2015-05-29 14:17:44', '2015-05-29 13:17:44', '', 67, 'http://127.0.0.1/docoeur/?attachment_id=66', 0, 'attachment', 'image/jpeg', 0),
(67, 3, '2015-05-29 14:18:24', '2015-05-29 13:18:24', '<strong>Alors que les dégâts du tabagisme restent surtout associés à la survenue de cancers, son influence sur le système cardiovasculaire est très importante. Le tabac est ainsi tenu pour responsable de 24 % des décès dans ce domaine. C''est donc un facteur particulièrement aggravant pour les personnes souffrant d''hypertension artérielle.</strong>\r\n<div class="doc-block-pub doc-block-pub-middle"></div>\r\n<img class=" doc-right alignleft" title="Hypertension et tabagisme" src="http://cdn3-doctissimo.ladmedia.fr/var/doctissimo/storage/images/media/images/fr/www/hta_tabac/594885-1-fre-FR/hta_tabac.jpg" alt="Hypertension et tabagisme" width="136" height="189" />Les Français ne connaissent pas bien l''impact de la consommation régulière de tabac sur le risque cardiovasculaire. Même les gros fumeurs semblent être mal informés. Selon un sondage réalisé par le Comité Français de Lutte contre l''Hypertension artérielle, 74 % des hypertendus pensent que le stress est l''unique cause de leurs problèmes de santé. Pire encore, des études concordantes révèlent que près de la moitié des fumeurs poursuivent leur consommation de tabac alors qu''ils souffrent d''hypertension artérielle (HTA), voire même après avoir subi un pontage ou une angioplastie coronarienne.\r\n<h2>Une prise de risque importante</h2>\r\nLes risques ne sont pourtant pas négligeables. L''American Heart Association (Association américaine contre les maladies cardiovasculaires) estime qu''aux Etats-Unis, 30 % des morts liés à l''insuffisance coronaire sont liés au tabagisme. L''âge et la dose inhalée influent directement sur l''importance du risque. Pour moins de 5 cigarettes par jour, le risque est multiplié par 1,4 ; au-delà de 10 cigarettes/jour par 2,4 et par 2,8 entre 20 et 40 cigarettes par jour, tandis qu''il n''est que de 0,7 pour un non-fumeur.\r\n\r\nLe tabagisme passif a également des effets néfastes. De 1982 à 1992, des scientifiques américains <sup>1</sup> ont suivi 32 000 femmes qui n''avaient jamais fumé et n''avaient jamais été victimes d''une maladie cardiovasculaire, d''une attaque cardiaque ou d''un cancer. Les résultats démontrent que l''exposition au tabagisme passif au domicile ou au travail multiplie par près de deux, le risque de développer une maladie coronarienne.\r\n<h2>Une augmentation de la pression artérielle</h2>\r\nLa nicotine agit directement sur les artères en augmentant de façon transitoire la pression artérielle et la fréquence cardiaque. Ce rétrécissement des artères est particulièrement perceptible lors de la première cigarette de la journée. L''effet se dissipe ensuite au bout d''une trentaine de minutes, mais la pression augmente progressivement tout au long de la journée, pour retrouver son niveau de base pendant le sommeil. La baisse de tension serait cependant moins marquée chez les fumeurs normotendus (c''est-à-dire ne souffrant pas d''HTA), que chez les non-fumeurs <sup>2</sup>.\r\n<h2>Jamais trop tard pour arrêter</h2>\r\nL''arrêt du tabac fait baisser petit à petit le risque coronarien, mais trois ans sont nécessaires pour retrouver le niveau d''un non-fumeur. Ce délai peut paraître long mais l''effort est payant.', 'Cigarette et hypertension : pas de fumée sans feu', '', 'publish', 'open', 'open', '', 'cigarette-et-hypertension-pas-de-fumee-sans-feu', '', '', '2015-06-04 15:54:26', '2015-06-04 14:54:26', '', 0, 'http://127.0.0.1/docoeur/?p=67', 0, 'post', '', 0),
(68, 1, '2015-05-29 14:18:24', '2015-05-29 13:18:24', '<strong>Alors que les dégâts du tabagisme restent surtout associés à la survenue de cancers, son influence sur le système cardiovasculaire est importante. Le tabac est ainsi tenu pour responsable de 24 % des décès dans ce domaine. C''est donc un facteur particulièrement aggravant pour les personnes souffrant d''hypertension artérielle.</strong>\r\n<div class="doc-block-pub doc-block-pub-middle"></div>\r\n<img class=" doc-right alignleft" title="Hypertension et tabagisme" src="http://cdn3-doctissimo.ladmedia.fr/var/doctissimo/storage/images/media/images/fr/www/hta_tabac/594885-1-fre-FR/hta_tabac.jpg" alt="Hypertension et tabagisme" width="136" height="189" />Les Français ne connaissent pas bien l''impact de la consommation régulière de tabac sur le risque cardiovasculaire. Même les gros fumeurs semblent être mal informés. Selon un sondage réalisé par le Comité Français de Lutte contre l''Hypertension artérielle, 74 % des hypertendus pensent que le stress est l''unique cause de leurs problèmes de santé. Pire encore, des études concordantes révèlent que près de la moitié des fumeurs poursuivent leur consommation de tabac alors qu''ils souffrent d''hypertension artérielle (HTA), voire même après avoir subi un pontage ou une angioplastie coronarienne.\r\n<h2>Une prise de risque importante</h2>\r\nLes risques ne sont pourtant pas négligeables. L''American Heart Association (Association américaine contre les maladies cardiovasculaires) estime qu''aux Etats-Unis, 30 % des morts liés à l''insuffisance coronaire sont liés au tabagisme. L''âge et la dose inhalée influent directement sur l''importance du risque. Pour moins de 5 cigarettes par jour, le risque est multiplié par 1,4 ; au-delà de 10 cigarettes/jour par 2,4 et par 2,8 entre 20 et 40 cigarettes par jour, tandis qu''il n''est que de 0,7 pour un non-fumeur.\r\n\r\nLe tabagisme passif a également des effets néfastes. De 1982 à 1992, des scientifiques américains <sup>1</sup> ont suivi 32 000 femmes qui n''avaient jamais fumé et n''avaient jamais été victimes d''une maladie cardiovasculaire, d''une attaque cardiaque ou d''un cancer. Les résultats démontrent que l''exposition au tabagisme passif au domicile ou au travail multiplie par près de deux, le risque de développer une maladie coronarienne.\r\n<h2>Une augmentation de la pression artérielle</h2>\r\nLa nicotine agit directement sur les artères en augmentant de façon transitoire la pression artérielle et la fréquence cardiaque. Ce rétrécissement des artères est particulièrement perceptible lors de la première cigarette de la journée. L''effet se dissipe ensuite au bout d''une trentaine de minutes, mais la pression augmente progressivement tout au long de la journée, pour retrouver son niveau de base pendant le sommeil. La baisse de tension serait cependant moins marquée chez les fumeurs normotendus (c''est-à-dire ne souffrant pas d''HTA), que chez les non-fumeurs <sup>2</sup>.\r\n<h2>Jamais trop tard pour arrêter</h2>\r\nL''arrêt du tabac fait baisser petit à petit le risque coronarien, mais trois ans sont nécessaires pour retrouver le niveau d''un non-fumeur. Ce délai peut paraître long mais l''effort est payant.', 'Cigarette et hypertension : pas de fumée sans feu', '', 'inherit', 'open', 'open', '', '67-revision-v1', '', '', '2015-05-29 14:18:24', '2015-05-29 13:18:24', '', 67, 'http://127.0.0.1/docoeur/2015/05/29/67-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2015-05-29 14:22:25', '2015-05-29 13:22:25', '', '<i class="fa fa-newspaper-o"></i> Mes Articles', '', 'publish', 'open', 'open', '', 'mes-articles', '', '', '2015-06-08 09:17:55', '2015-06-08 08:17:55', '', 0, 'http://127.0.0.1/docoeur/?p=69', 12, 'nav_menu_item', '', 0),
(70, 3, '2015-05-29 15:46:21', '2015-05-29 14:46:21', 'Bonjour, y-a-t-il des aliments à bannir complètement car dangereux pour le coeur ? Je suis cardiaque ! <br /> Merci pour vos réponses', 'Quels sont les aliments bons et les aliments mauvais pour le coeur ?', '', 'publish', 'open', 'open', '', 'quels-sont-les-aliments-bons-et-le-aliments-mauvais-pour-le-coeur', '', '', '2015-06-03 09:15:55', '2015-06-03 08:15:55', '', 0, 'http://127.0.0.1/docoeur/question/quels-sont-les-aliments-bons-et-le-aliments-mauvais-pour-le-coeur/', 0, 'dwqa-question', '', 0),
(126, 1, '2015-06-08 07:49:43', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-08 07:49:43', '0000-00-00 00:00:00', '', 0, 'http://127.0.0.1/docoeur/?p=126', 0, 'post', '', 0),
(72, 2, '2015-06-01 11:59:28', '2015-06-01 10:59:28', 'Bonjour,<br /> Plusieurs études cliniques\r\n<ol>\r\n<li> démontrent que la teneur en phytostérols des amandes à raison de 34 mg pour 30 g soit 25 amandes, fait diminuer la concentration de « mauvais » cholestérol (LDL) dans l’organisme. Une hypercholestérolémie peut entraîner des troubles cardiaques car le « mauvais » cholestérol, après avoir approvisionné les organes, se dépose dans les artères et, en cas de surplus, risque de les boucher. Selon des données épidémiologiques</li>\r\n<li>une consommation quotidienne de 30 g d’amandes réduirait le risque de maladies cardio-vasculaires de 45%</li>\r\n</ol>\r\n&nbsp;', 'répondreQuels sont les aliments bons et le aliments mauvais pour le coeur ?', '', 'publish', 'open', 'open', '', 'repondrequels-sont-les-aliments-bons-et-le-aliments-mauvais-pour-le-coeur', '', '', '2015-06-01 15:19:56', '2015-06-01 14:19:56', '', 0, 'http://127.0.0.1/docoeur/dwqa-answer/repondrequels-sont-les-aliments-bons-et-le-aliments-mauvais-pour-le-coeur/', 0, 'dwqa-answer', '', 0),
(130, 1, '2015-06-09 14:01:18', '2015-06-09 13:01:18', '', 'Banniere2', '', 'inherit', 'open', 'open', '', 'banniere2', '', '', '2015-06-09 14:01:18', '2015-06-09 13:01:18', '', 0, 'http://127.0.0.1/docoeur/wp-content/uploads/2015/06/Banniere2.jpg', 0, 'attachment', 'image/jpeg', 0),
(74, 3, '2015-06-02 08:59:12', '2015-06-02 07:59:12', '', 'Defenses Tech Info CPNV 2015', '', 'inherit', 'open', 'open', '', 'defenses-tech-info-cpnv-2015', '', '', '2015-06-02 08:59:12', '2015-06-02 07:59:12', '', 0, 'http://127.0.0.1/docoeur/?attachment_id=74', 0, 'attachment', 'application/pdf', 0),
(93, 3, '2015-06-03 14:03:06', '2015-06-03 13:03:06', '', '1432664173_sign-error', '', 'inherit', 'open', 'open', '', '1432664173_sign-error', '', '', '2015-06-03 14:03:06', '2015-06-03 13:03:06', '', 92, 'http://127.0.0.1/docoeur/wp-content/uploads/2015/06/1432664173_sign-error.png', 0, 'attachment', 'image/png', 0),
(83, 1, '2015-06-03 09:49:46', '2015-06-03 08:49:46', '', 'graphique', '', 'inherit', 'open', 'open', '', 'graphique', '', '', '2015-06-03 09:49:46', '2015-06-03 08:49:46', '', 70, 'http://127.0.0.1/docoeur/wp-content/uploads/2015/05/graphique.png', 0, 'attachment', 'image/png', 0),
(84, 1, '2015-06-03 09:49:55', '2015-06-03 08:49:55', 'Voir le graphique suivant \r\n<a href="http://127.0.0.1/docoeur/wp-content/uploads/2015/05/graphique.png"><img src="http://127.0.0.1/docoeur/wp-content/uploads/2015/05/graphique-300x163.png" alt="graphique" width="300" height="163" /></a>', 'répondreQuels sont les aliments bons et les aliments mauvais pour le coeur ?', '', 'publish', 'open', 'open', '', 'repondrequels-sont-les-aliments-bons-et-les-aliments-mauvais-pour-le-coeur-2', '', '', '2015-06-03 09:49:55', '2015-06-03 08:49:55', '', 0, 'http://127.0.0.1/docoeur/dwqa-answer/repondrequels-sont-les-aliments-bons-et-les-aliments-mauvais-pour-le-coeur-2/', 0, 'dwqa-answer', '', 0),
(94, 2, '2015-06-03 15:00:25', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-03 15:00:25', '0000-00-00 00:00:00', '', 0, 'http://127.0.0.1/docoeur/?p=94', 0, 'post', '', 0),
(96, 1, '2015-06-04 14:41:31', '2015-06-04 13:41:31', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n</br>\r\n<a href="/wp_v2/faq-en-general" class="active">En Général</a> | <a href="/wp_v2/faq-technique">Technique</a>\r\n</br></br>\r\n\r\nSur l''ensemble du site, vous rencontrerez des icônes permettant d''identifier le role de l''utilisateur. \r\nVoici leur signification : \r\n<i class="fa fa-user blue"></i> Membre | <i class="fa fa-stethoscope green"></i> Spécialiste | <i class="fa fa-certificate orange"></i> Modérateur | <i class="fa fa-star yellow"></i> Administrateur\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Comment participer à la vie du site ?\r\n<i class="fa fa-info"></i>    Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Quelle est la différence entre un Membre et un Spécialiste ?\r\n<i class="fa fa-info"></i>    Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    J''ai une question, à qui puis-je m''adresser ?\r\n<i class="fa fa-info"></i>    La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.\r\n\r\n<hr />', 'FAQ', '', 'publish', 'open', 'open', '', 'faq-en-general', '', '', '2015-06-05 13:27:06', '2015-06-05 12:27:06', '', 0, 'http://127.0.0.1/docoeur/?page_id=96', 0, 'page', '', 0),
(97, 1, '2015-06-04 14:22:41', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-04 14:22:41', '0000-00-00 00:00:00', '', 0, 'http://127.0.0.1/docoeur/?page_id=97', 0, 'page', '', 0),
(98, 1, '2015-06-04 14:22:54', '2015-06-04 13:22:54', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n\r\n<hr />\r\n\r\n<p>Comment participer à la vie du site ? </p>\r\n<p>Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a></p>\r\n\r\n<p>Quelle est la différence entre un Membre et un Spécialiste ?</p>\r\n<p>Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"</p>\r\n<p>Un Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"</p>\r\n', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:22:54', '2015-06-04 13:22:54', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(99, 1, '2015-06-04 14:28:13', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-06-04 14:28:13', '0000-00-00 00:00:00', '', 0, 'http://127.0.0.1/docoeur/?page_id=99', 0, 'page', '', 0),
(100, 1, '2015-06-04 14:28:34', '2015-06-04 13:28:34', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n\r\n<hr />\r\n\r\n<p><i class="fa fa-comment-o"></i>Comment participer à la vie du site ? </p>\r\n<p><i class="fa fa-info">Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a></p>\r\n\r\n<p>Quelle est la différence entre un Membre et un Spécialiste ?</p>\r\n<p>Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"</p>\r\n<p>Un Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"</p>\r\n\r\n<p>J''ai une question, à qui puis-je m''adresser ?</p>\r\n<p>La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.</p>', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:28:34', '2015-06-04 13:28:34', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(102, 1, '2015-06-04 14:30:45', '2015-06-04 13:30:45', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i> Comment participer à la vie du site ?\r\n\r\n<i class="fa fa-info"> Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a> </i>\r\n\r\nQuelle est la différence entre un Membre et un Spécialiste ?\r\n\r\nUn Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\nJ''ai une question, à qui puis-je m''adresser ?\r\n\r\nLa page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:30:45', '2015-06-04 13:30:45', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(101, 1, '2015-06-04 14:29:30', '2015-06-04 13:29:30', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n\r\n<hr />\r\n\r\n<p>\r\n<i class="fa fa-comment-o"></i> Comment participer à la vie du site ? </p>\r\n<p>\r\n<i class="fa fa-info"> Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n</p>\r\n\r\n<p>Quelle est la différence entre un Membre et un Spécialiste ?</p>\r\n<p>Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"</p>\r\n<p>Un Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"</p>\r\n\r\n<p>J''ai une question, à qui puis-je m''adresser ?</p>\r\n<p>La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.</p>', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:29:30', '2015-06-04 13:29:30', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `dcHyv_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(103, 1, '2015-06-04 14:31:24', '2015-06-04 13:31:24', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o fa-2x"></i> Comment participer à la vie du site ?\r\n\r\n<i class="fa fa-info fa-2x"> Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a> </i>\r\n\r\nQuelle est la différence entre un Membre et un Spécialiste ?\r\n\r\nUn Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\nJ''ai une question, à qui puis-je m''adresser ?\r\n\r\nLa page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:31:24', '2015-06-04 13:31:24', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(104, 1, '2015-06-04 14:31:52', '2015-06-04 13:31:52', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o fa-2x"></i> Comment participer à la vie du site ?\r\n\r\n<i class="fa fa-info fa-2x"></i> Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n\r\nQuelle est la différence entre un Membre et un Spécialiste ?\r\n\r\nUn Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\nJ''ai une question, à qui puis-je m''adresser ?\r\n\r\nLa page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:31:52', '2015-06-04 13:31:52', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(105, 1, '2015-06-04 14:33:25', '2015-06-04 13:33:25', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Comment participer à la vie du site ?\r\n\r\n<i class="fa fa-info"></i>    Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n\r\ni class="fa fa-comment-o"></i>    Quelle est la différence entre un Membre et un Spécialiste ?\r\n\r\n<i class="fa fa-info"></i>    Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\ni class="fa fa-comment-o"></i>    J''ai une question, à qui puis-je m''adresser ?\r\n\r\n<i class="fa fa-info"></i>    La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:33:25', '2015-06-04 13:33:25', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(106, 1, '2015-06-04 14:33:59', '2015-06-04 13:33:59', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Comment participer à la vie du site ?\r\n\r\n<i class="fa fa-info"></i>    Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n\r\n<i class="fa fa-comment-o"></i>    Quelle est la différence entre un Membre et un Spécialiste ?\r\n\r\n<i class="fa fa-info"></i>    Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\n<i class="fa fa-comment-o"></i>    J''ai une question, à qui puis-je m''adresser ?\r\n\r\n<i class="fa fa-info"></i>    La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:33:59', '2015-06-04 13:33:59', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(107, 1, '2015-06-04 14:34:59', '2015-06-04 13:34:59', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n\r\n\r\n\r\n<i class="fa fa-comment-o"></i>    Comment participer à la vie du site ?\r\n\r\n<i class="fa fa-info"></i>    Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n<hr />\r\n<i class="fa fa-comment-o"></i>    Quelle est la différence entre un Membre et un Spécialiste ?\r\n<hr />\r\n<i class="fa fa-info"></i>    Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n<hr />\r\n<i class="fa fa-comment-o"></i>    J''ai une question, à qui puis-je m''adresser ?\r\n\r\n<i class="fa fa-info"></i>    La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.\r\n<hr />', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:34:59', '2015-06-04 13:34:59', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(108, 1, '2015-06-04 14:35:54', '2015-06-04 13:35:54', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n\r\n\r\n\r\n<i class="fa fa-comment-o"></i>    Comment participer à la vie du site ?\r\n<i class="fa fa-info"></i>    Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Quelle est la différence entre un Membre et un Spécialiste ?\r\n<i class="fa fa-info"></i>    Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    J''ai une question, à qui puis-je m''adresser ?\r\n<i class="fa fa-info"></i>    La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.\r\n\r\n<hr />', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:35:54', '2015-06-04 13:35:54', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(109, 1, '2015-06-04 14:37:31', '2015-06-04 13:37:31', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n\r\n</br></br>\r\n\r\n<i class="fa fa-comment-o"></i>    Comment participer à la vie du site ?\r\n<i class="fa fa-info"></i>    Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Quelle est la différence entre un Membre et un Spécialiste ?\r\n<i class="fa fa-info"></i>    Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    J''ai une question, à qui puis-je m''adresser ?\r\n<i class="fa fa-info"></i>    La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.\r\n\r\n<hr />', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:37:31', '2015-06-04 13:37:31', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(110, 1, '2015-06-04 14:40:47', '2015-06-04 13:40:47', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n<a href="/wp_v2/faq-en-general" class="active">En Général</a> | <a href="/wp_v2/faq-technique">Techniques</a>\r\n</br></br>\r\n\r\n<i class="fa fa-comment-o"></i>    Comment participer à la vie du site ?\r\n<i class="fa fa-info"></i>    Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Quelle est la différence entre un Membre et un Spécialiste ?\r\n<i class="fa fa-info"></i>    Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    J''ai une question, à qui puis-je m''adresser ?\r\n<i class="fa fa-info"></i>    La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.\r\n\r\n<hr />', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:40:47', '2015-06-04 13:40:47', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(111, 1, '2015-06-04 14:41:26', '2015-06-04 13:41:26', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n<br>\r\n<a href="/wp_v2/faq-en-general" class="active">En Général</a> | <a href="/wp_v2/faq-technique">Techniques</a>\r\n</br></br>\r\n\r\n<i class="fa fa-comment-o"></i>    Comment participer à la vie du site ?\r\n<i class="fa fa-info"></i>    Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Quelle est la différence entre un Membre et un Spécialiste ?\r\n<i class="fa fa-info"></i>    Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    J''ai une question, à qui puis-je m''adresser ?\r\n<i class="fa fa-info"></i>    La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.\r\n\r\n<hr />', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:41:26', '2015-06-04 13:41:26', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(113, 1, '2015-06-04 14:42:41', '2015-06-04 13:42:41', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n</br>\r\n<a href="/wp_v2/faq-en-general" class="active">En Général</a> | <a href="/wp_v2/faq-technique">Technique</a>\r\n</br></br>\r\n\r\n<i class="fa fa-comment-o"></i>    Comment participer à la vie du site ?\r\n<i class="fa fa-info"></i>    Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Quelle est la différence entre un Membre et un Spécialiste ?\r\n<i class="fa fa-info"></i>    Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    J''ai une question, à qui puis-je m''adresser ?\r\n<i class="fa fa-info"></i>    La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.\r\n\r\n<hr />', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:42:41', '2015-06-04 13:42:41', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(112, 1, '2015-06-04 14:41:57', '2015-06-04 13:41:57', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n</br>\r\n<a href="/wp_v2/faq-en-general" class="active">En Général</a> | <a href="/wp_v2/faq-technique">Techniques</a>\r\n</br></br>\r\n\r\n<i class="fa fa-comment-o"></i>    Comment participer à la vie du site ?\r\n<i class="fa fa-info"></i>    Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Quelle est la différence entre un Membre et un Spécialiste ?\r\n<i class="fa fa-info"></i>    Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    J''ai une question, à qui puis-je m''adresser ?\r\n<i class="fa fa-info"></i>    La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.\r\n\r\n<hr />', 'FàQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:41:57', '2015-06-04 13:41:57', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(114, 1, '2015-06-04 14:56:27', '2015-06-04 13:56:27', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n</br>\r\n<a href="/wp_v2/faq-en-general">En Général</a> | <a href="/wp_v2/faq-technique" class="active">Technique</a>\r\n</br></br>\r\n\r\n<i class="fa fa-comment-o"></i>  Question ?\r\n<i class="fa fa-info"></i>   Réponse ! \r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Question ?\r\n<i class="fa fa-info"></i>    Réponse !\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Question ? ?\r\n<i class="fa fa-info"></i>    Réponse !\r\n\r\n<hr />', 'FAQ Technique', '', 'publish', 'open', 'open', '', 'faq-technique', '', '', '2015-06-04 14:57:32', '2015-06-04 13:57:32', '', 0, 'http://127.0.0.1/docoeur/?page_id=114', 0, 'page', '', 0),
(115, 1, '2015-06-04 14:56:04', '2015-06-04 13:56:04', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n</br>\r\n<a href="/wp_v2/faq-en-general">En Général</a> | <a href="/wp_v2/faq-technique class="active"">Technique</a>\r\n</br></br>\r\n\r\n<i class="fa fa-comment-o"></i>  Question ?\r\n<i class="fa fa-info"></i>   Réponse ! \r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Question ?\r\n<i class="fa fa-info"></i>    Réponse !\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Question ? ?\r\n<i class="fa fa-info"></i>    Réponse !\r\n\r\n<hr />', 'FAQ Technique', '', 'inherit', 'open', 'open', '', '114-revision-v1', '', '', '2015-06-04 14:56:04', '2015-06-04 13:56:04', '', 114, 'http://127.0.0.1/docoeur/2015/06/04/114-revision-v1/', 0, 'revision', '', 0),
(116, 1, '2015-06-04 14:56:52', '2015-06-04 13:56:52', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n</br>\r\n<a href="/wp_v2/faq-en-general">En Général</a> | <a href="/wp_v2/faq-technique class="active">Technique</a>\r\n</br></br>\r\n\r\n<i class="fa fa-comment-o"></i>  Question ?\r\n<i class="fa fa-info"></i>   Réponse ! \r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Question ?\r\n<i class="fa fa-info"></i>    Réponse !\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Question ? ?\r\n<i class="fa fa-info"></i>    Réponse !\r\n\r\n<hr />', 'FAQ Technique', '', 'inherit', 'open', 'open', '', '114-revision-v1', '', '', '2015-06-04 14:56:52', '2015-06-04 13:56:52', '', 114, 'http://127.0.0.1/docoeur/2015/06/04/114-revision-v1/', 0, 'revision', '', 0),
(117, 1, '2015-06-04 14:57:32', '2015-06-04 13:57:32', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n</br>\r\n<a href="/wp_v2/faq-en-general">En Général</a> | <a href="/wp_v2/faq-technique" class="active">Technique</a>\r\n</br></br>\r\n\r\n<i class="fa fa-comment-o"></i>  Question ?\r\n<i class="fa fa-info"></i>   Réponse ! \r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Question ?\r\n<i class="fa fa-info"></i>    Réponse !\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Question ? ?\r\n<i class="fa fa-info"></i>    Réponse !\r\n\r\n<hr />', 'FAQ Technique', '', 'inherit', 'open', 'open', '', '114-revision-v1', '', '', '2015-06-04 14:57:32', '2015-06-04 13:57:32', '', 114, 'http://127.0.0.1/docoeur/2015/06/04/114-revision-v1/', 0, 'revision', '', 0),
(118, 1, '2015-06-04 14:59:26', '2015-06-04 13:59:26', '', '<i class="fa fa-life-ring"></i> FAQ', '', 'publish', 'open', 'open', '', 'faq', '', '', '2015-06-08 09:17:55', '2015-06-08 08:17:55', '', 0, 'http://127.0.0.1/docoeur/?p=118', 5, 'nav_menu_item', '', 0),
(119, 1, '2015-06-04 14:59:38', '2015-06-04 13:59:38', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n</br>\r\n<a href="/wp_v2/faq-en-general" class="active">En Général</a> | <a href="/wp_v2/faq-technique">Technique</a>\r\n</br></br>\r\n\r\n<i class="fa fa-comment-o"></i>    Comment participer à la vie du site ?\r\n<i class="fa fa-info"></i>    Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Quelle est la différence entre un Membre et un Spécialiste ?\r\n<i class="fa fa-info"></i>    Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    J''ai une question, à qui puis-je m''adresser ?\r\n<i class="fa fa-info"></i>    La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.\r\n\r\n<hr />', 'FAQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-04 14:59:38', '2015-06-04 13:59:38', '', 96, 'http://127.0.0.1/docoeur/2015/06/04/96-revision-v1/', 0, 'revision', '', 0),
(123, 1, '2015-06-05 13:25:55', '2015-06-05 12:25:55', '<h1>Foire aux Questions</h1>\n<h3>Questions les plus fréquentes</h3>\n</br>\n<a href="/wp_v2/faq-en-general" class="active">En Général</a> | <a href="/wp_v2/faq-technique">Technique</a>\n</br></br>\n\nSur l''ensemble du site, vous rencontrerez des icônes permettant d''identifier l\n\n<i class="fa fa-comment-o"></i>    Comment participer à la vie du site ?\n<i class="fa fa-info"></i>    Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\n\n<hr />\n\n<i class="fa fa-comment-o"></i>    Quelle est la différence entre un Membre et un Spécialiste ?\n<i class="fa fa-info"></i>    Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\n\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\n\n<hr />\n\n<i class="fa fa-comment-o"></i>    J''ai une question, à qui puis-je m''adresser ?\n<i class="fa fa-info"></i>    La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.\n\n<hr />', 'FAQ', '', 'inherit', 'open', 'open', '', '96-autosave-v1', '', '', '2015-06-05 13:25:55', '2015-06-05 12:25:55', '', 96, 'http://127.0.0.1/docoeur/2015/06/05/96-autosave-v1/', 0, 'revision', '', 0),
(124, 1, '2015-06-05 13:26:29', '2015-06-05 12:26:29', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n</br>\r\n<a href="/wp_v2/faq-en-general" class="active">En Général</a> | <a href="/wp_v2/faq-technique">Technique</a>\r\n</br></br>\r\n\r\nSur l''ensemble du site, vous rencontrerez des icônes permettant d''identifier le role de l''utilisateur. \r\nVoici leur signification : \r\n<i class="fa fa-user blue"></i> Membre | <i class="fa fa-stethoscope green"></i> Spécialiste | <i class="fa fa-certificate orange"></i> Modérateur | <i class="fa fa-star yellow"></i> Administrateur\r\n\r\n<i class="fa fa-comment-o"></i>    Comment participer à la vie du site ?\r\n<i class="fa fa-info"></i>    Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Quelle est la différence entre un Membre et un Spécialiste ?\r\n<i class="fa fa-info"></i>    Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    J''ai une question, à qui puis-je m''adresser ?\r\n<i class="fa fa-info"></i>    La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.\r\n\r\n<hr />', 'FAQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-05 13:26:29', '2015-06-05 12:26:29', '', 96, 'http://127.0.0.1/docoeur/2015/06/05/96-revision-v1/', 0, 'revision', '', 0),
(125, 1, '2015-06-05 13:27:06', '2015-06-05 12:27:06', '<h1>Foire aux Questions</h1>\r\n<h3>Questions les plus fréquentes</h3>\r\n</br>\r\n<a href="/wp_v2/faq-en-general" class="active">En Général</a> | <a href="/wp_v2/faq-technique">Technique</a>\r\n</br></br>\r\n\r\nSur l''ensemble du site, vous rencontrerez des icônes permettant d''identifier le role de l''utilisateur. \r\nVoici leur signification : \r\n<i class="fa fa-user blue"></i> Membre | <i class="fa fa-stethoscope green"></i> Spécialiste | <i class="fa fa-certificate orange"></i> Modérateur | <i class="fa fa-star yellow"></i> Administrateur\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Comment participer à la vie du site ?\r\n<i class="fa fa-info"></i>    Afin de publier des billets et poser des questions, vous devez posséder un compte utilisateur. Pour cela, vous pouvez vous <a href="http://127.0.0.1/docoeur/login/">connecter</a> ou vous <a href="http://127.0.0.1/docoeur/register/">enregistrer</a>\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    Quelle est la différence entre un Membre et un Spécialiste ?\r\n<i class="fa fa-info"></i>    Un Spécialiste est un professionnel de la santé en partenariat avec notre site. Son identité à été confirmée par notre équipe de modérateur et il est donc à même de répondre de manière approfondie aux questions.De plus, Leurs témoignages peuvent être visible sur les pages "Récits Vécus" et "Espace Spécialiste"\r\n\r\nUn Membre est une personne lambda souhaitant apporter sa contribution à la communauté. Ses articles ne seront visibles que dans la partie "Récits Vécus"\r\n\r\n<hr />\r\n\r\n<i class="fa fa-comment-o"></i>    J''ai une question, à qui puis-je m''adresser ?\r\n<i class="fa fa-info"></i>    La page question vous permet d''interagir avec l''ensemble de la communauté de manière à rapidement trouver une réponse à votre interrogation.\r\n\r\n<hr />', 'FAQ', '', 'inherit', 'open', 'open', '', '96-revision-v1', '', '', '2015-06-05 13:27:06', '2015-06-05 12:27:06', '', 96, 'http://127.0.0.1/docoeur/2015/06/05/96-revision-v1/', 0, 'revision', '', 0),
(131, 1, '2015-06-09 14:01:26', '2015-06-09 13:01:26', 'http://127.0.0.1/docoeur/wp-content/uploads/2015/06/cropped-Banniere2.jpg', 'cropped-Banniere2.jpg', '', 'inherit', 'open', 'open', '', 'cropped-banniere2-jpg', '', '', '2015-06-09 14:01:26', '2015-06-09 13:01:26', '', 0, 'http://127.0.0.1/docoeur/wp-content/uploads/2015/06/cropped-Banniere2.jpg', 0, 'attachment', 'image/jpeg', 0),
(129, 4, '2015-06-09 08:13:04', '2015-06-09 07:13:04', 'Oui', 'répondreQuestion test', '', 'trash', 'open', 'open', '', 'repondrequestion-test', '', '', '2015-06-09 08:13:30', '2015-06-09 07:13:30', '', 0, 'http://127.0.0.1/docoeur/dwqa-answer/repondrequestion-test/', 0, 'dwqa-answer', '', 0),
(132, 3, '2015-06-10 09:25:01', '2015-06-10 08:25:01', '', 'wallpaper-1332251', '', 'inherit', 'open', 'open', '', 'wallpaper-1332251', '', '', '2015-06-10 09:33:11', '2015-06-10 08:33:11', '', 134, 'http://127.0.0.1/docoeur/?attachment_id=132', 0, 'attachment', 'image/jpeg', 0),
(134, 3, '2015-06-10 09:30:16', '2015-06-10 08:30:16', 'L''insuffisance cardiaque est la première cause de mortalité cardiovasculaire devant l''infarctus et l''hypertension artérielle. 14 millions d''européens en souffrent, et on estime qu''ils seront deux fois plus d''ici 2020. Pourtant, la maladie évolue dans l''indifférence la plus totale.\r\n\r\n<img class="alignright" src="https://www.croi.ie/sites/default/files/field/image/heart-attack-03.jpg" alt="" width="277" height="179" />\r\n\r\n500 000 personnes souffrent d''insuffisance cardiaque en France et 32 000 en meurent chaque année. Diagnostics et traitements sont insuffisants. Ce sont des milliers de décès qui pourraient être évités.\r\n\r\nL''insuffisance cardiaque, peu connue du grand public\r\n\r\nA la question "savez-vous ce qu''est l''insuffisance cardiaque ?" Les Français répondent "non" en majorité. Tels sont les résultats d''une étude européenne : "l''étude Shape". Réalisée sur 15 000 personnes, l''étude montre que 86 % des personnes interrogées déclarent avoir entendu parler de la maladie mais en ignorent les symptômes. "Alors que le nombre de malades ne cesse d''augmenter, il est impératif de remédier à la trop grande méconnaissance de cette maladie grave" s''insurge Alain Cohen-Solal, professeur de cardiologie. L''insuffisance cardiaque est responsable de trop nombreux morts. 40 % des insuffisants cardiaques décèdent dans l''année qui suit leur première hospitalisation et 60 % dans les cinq ans qui suivent le diagnostic.\r\n\r\n&nbsp;\r\n\r\nQu''est ce que l''insuffisance cardiaque ?\r\n\r\nInsuffisance cardiaque coeur santé maladies cardiovasculaires Les symptômes de cette maladie sont très handicapants et seulement 5 % des européens ont su les décrire : fatigue extrême, oedème des chevilles, essoufflement à l''effort et au repos. Des signes qui ne trompent pas et qui sont liés à la défaillance du muscle cardiaque.\r\n\r\nUne défaillance qui apparaît lentement. Après plusieurs années d''évolution de la maladie, le coeur retarde l''arrivée des symptômes en se dilatant pour accroître l''afflux sanguin, en s''épaississant pour pouvoir se contracter plus puissamment, et en accélérant le rythme cardiaque. Mais au bout d''un certain temps, il ne peut plus compenser le manque : le muscle cardiaque n''est plus aussi énergique qu''à l''habitude. Il envoie moins vite et moins bien le sang au reste du corps. La défaillance du muscle cardiaque peut être due à de multiples causes dont les deux plus fréquentes sont la maladie coronaire (faisant notamment suite à un infarctus) et l''hypertension artérielle. Aussi il est important de ne pas négliger les principaux facteurs de risque : l''hypercholestérolémie, le tabagisme, l''hypertension artérielle, le diabète et l''obésité. "Des facteurs qu''il ne faut pas prendre à la légère. comme les signes précurseurs de la maladie lorsqu''ils apparaissent" précise Le Pr. Solal. Les plus fréquents sont des difficultés à respirer à l''effort comme au repos, des oedèmes au niveau des chevilles et une fatigue constante. Les autres symptômes, moins évidents, sont une accélération du rythme cardiaque (tachycardie), un état de confusion, lié à l''oxygénation insuffisante du cerveau, ou encore une perte d''appétit.\r\n\r\n&nbsp;', 'Insuffisance et crise cardiaque', '', 'pending', 'open', 'open', '', 'insuffisance-et-crise-cardiaque', '', '', '2015-06-11 13:05:50', '2015-06-11 12:05:50', '', 0, 'http://127.0.0.1/docoeur/?p=134', 0, 'post', '', 0),
(137, 3, '2015-06-10 09:33:07', '2015-06-10 08:33:07', '', 'Evaluation diplome - Exemple', '', 'inherit', 'open', 'open', '', 'evaluation-diplome-exemple', '', '', '2015-06-10 09:33:11', '2015-06-10 08:33:11', '', 134, 'http://127.0.0.1/docoeur/?attachment_id=137', 0, 'attachment', 'application/pdf', 0),
(135, 4, '2015-06-10 09:29:05', '2015-06-10 08:29:05', 'L''insuffisance cardiaque est la première cause de mortalité cardiovasculaire devant l''infarctus et l''hypertension artérielle. 14 millions d''européens en souffrent, et on estime qu''ils seront deux fois plus d''ici 2020. Pourtant, la maladie évolue dans l''indifférence la plus totale.\r\n\r\n<img class="alignright" src="https://www.croi.ie/sites/default/files/field/image/heart-attack-03.jpg" alt="" width="277" height="179" />\r\n\r\n500 000 personnes souffrent d''insuffisance cardiaque en France et 32 000 en meurent chaque année. Diagnostics et traitements sont insuffisants. Ce sont des milliers de décès qui pourraient être évités.\r\n\r\nL''insuffisance cardiaque, peu connue du grand public\r\n\r\nA la question "savez-vous ce qu''est l''insuffisance cardiaque ?" Les Français répondent "non" en majorité. Tels sont les résultats d''une étude européenne : "l''étude Shape". Réalisée sur 15 000 personnes, l''étude montre que 86 % des personnes interrogées déclarent avoir entendu parler de la maladie mais en ignorent les symptômes. "Alors que le nombre de malades ne cesse d''augmenter, il est impératif de remédier à la trop grande méconnaissance de cette maladie grave" s''insurge Alain Cohen-Solal, professeur de cardiologie. L''insuffisance cardiaque est responsable de trop nombreux morts. 40 % des insuffisants cardiaques décèdent dans l''année qui suit leur première hospitalisation et 60 % dans les cinq ans qui suivent le diagnostic.\r\n\r\n&nbsp;\r\n\r\nQu''est ce que l''insuffisance cardiaque ?\r\n\r\nInsuffisance cardiaque coeur santé maladies cardiovasculaires Les symptômes de cette maladie sont très handicapants et seulement 5 % des européens ont su les décrire : fatigue extrême, oedème des chevilles, essoufflement à l''effort et au repos. Des signes qui ne trompent pas et qui sont liés à la défaillance du muscle cardiaque.\r\n\r\nUne défaillance qui apparaît lentement. Après plusieurs années d''évolution de la maladie, le coeur retarde l''arrivée des symptômes en se dilatant pour accroître l''afflux sanguin, en s''épaississant pour pouvoir se contracter plus puissamment, et en accélérant le rythme cardiaque. Mais au bout d''un certain temps, il ne peut plus compenser le manque : le muscle cardiaque n''est plus aussi énergique qu''à l''habitude. Il envoie moins vite et moins bien le sang au reste du corps. La défaillance du muscle cardiaque peut être due à de multiples causes dont les deux plus fréquentes sont la maladie coronaire (faisant notamment suite à un infarctus) et l''hypertension artérielle. Aussi il est important de ne pas négliger les principaux facteurs de risque : l''hypercholestérolémie, le tabagisme, l''hypertension artérielle, le diabète et l''obésité. "Des facteurs qu''il ne faut pas prendre à la légère. comme les signes précurseurs de la maladie lorsqu''ils apparaissent" précise Le Pr. Solal. Les plus fréquents sont des difficultés à respirer à l''effort comme au repos, des oedèmes au niveau des chevilles et une fatigue constante. Les autres symptômes, moins évidents, sont une accélération du rythme cardiaque (tachycardie), un état de confusion, lié à l''oxygénation insuffisante du cerveau, ou encore une perte d''appétit.', 'Insuffisance et crise cardiaque', '', 'inherit', 'open', 'open', '', '134-autosave-v1', '', '', '2015-06-10 09:29:05', '2015-06-10 08:29:05', '', 134, 'http://127.0.0.1/docoeur/2015/06/10/134-autosave-v1/', 0, 'revision', '', 0),
(136, 4, '2015-06-10 09:30:16', '2015-06-10 08:30:16', 'L''insuffisance cardiaque est la première cause de mortalité cardiovasculaire devant l''infarctus et l''hypertension artérielle. 14 millions d''européens en souffrent, et on estime qu''ils seront deux fois plus d''ici 2020. Pourtant, la maladie évolue dans l''indifférence la plus totale.\r\n\r\n<img class="alignright" src="https://www.croi.ie/sites/default/files/field/image/heart-attack-03.jpg" alt="" width="277" height="179" />\r\n\r\n500 000 personnes souffrent d''insuffisance cardiaque en France et 32 000 en meurent chaque année. Diagnostics et traitements sont insuffisants. Ce sont des milliers de décès qui pourraient être évités.\r\n\r\nL''insuffisance cardiaque, peu connue du grand public\r\n\r\nA la question "savez-vous ce qu''est l''insuffisance cardiaque ?" Les Français répondent "non" en majorité. Tels sont les résultats d''une étude européenne : "l''étude Shape". Réalisée sur 15 000 personnes, l''étude montre que 86 % des personnes interrogées déclarent avoir entendu parler de la maladie mais en ignorent les symptômes. "Alors que le nombre de malades ne cesse d''augmenter, il est impératif de remédier à la trop grande méconnaissance de cette maladie grave" s''insurge Alain Cohen-Solal, professeur de cardiologie. L''insuffisance cardiaque est responsable de trop nombreux morts. 40 % des insuffisants cardiaques décèdent dans l''année qui suit leur première hospitalisation et 60 % dans les cinq ans qui suivent le diagnostic.\r\n\r\n&nbsp;\r\n\r\nQu''est ce que l''insuffisance cardiaque ?\r\n\r\nInsuffisance cardiaque coeur santé maladies cardiovasculaires Les symptômes de cette maladie sont très handicapants et seulement 5 % des européens ont su les décrire : fatigue extrême, oedème des chevilles, essoufflement à l''effort et au repos. Des signes qui ne trompent pas et qui sont liés à la défaillance du muscle cardiaque.\r\n\r\nUne défaillance qui apparaît lentement. Après plusieurs années d''évolution de la maladie, le coeur retarde l''arrivée des symptômes en se dilatant pour accroître l''afflux sanguin, en s''épaississant pour pouvoir se contracter plus puissamment, et en accélérant le rythme cardiaque. Mais au bout d''un certain temps, il ne peut plus compenser le manque : le muscle cardiaque n''est plus aussi énergique qu''à l''habitude. Il envoie moins vite et moins bien le sang au reste du corps. La défaillance du muscle cardiaque peut être due à de multiples causes dont les deux plus fréquentes sont la maladie coronaire (faisant notamment suite à un infarctus) et l''hypertension artérielle. Aussi il est important de ne pas négliger les principaux facteurs de risque : l''hypercholestérolémie, le tabagisme, l''hypertension artérielle, le diabète et l''obésité. "Des facteurs qu''il ne faut pas prendre à la légère. comme les signes précurseurs de la maladie lorsqu''ils apparaissent" précise Le Pr. Solal. Les plus fréquents sont des difficultés à respirer à l''effort comme au repos, des oedèmes au niveau des chevilles et une fatigue constante. Les autres symptômes, moins évidents, sont une accélération du rythme cardiaque (tachycardie), un état de confusion, lié à l''oxygénation insuffisante du cerveau, ou encore une perte d''appétit.', 'Insuffisance et crise cardiaque', '', 'inherit', 'open', 'open', '', '134-revision-v1', '', '', '2015-06-10 09:30:16', '2015-06-10 08:30:16', '', 134, 'http://127.0.0.1/docoeur/2015/06/10/134-revision-v1/', 0, 'revision', '', 0),
(138, 3, '2015-06-10 09:33:11', '2015-06-10 08:33:11', 'L''insuffisance cardiaque est la première cause de mortalité cardiovasculaire devant l''infarctus et l''hypertension artérielle. 14 millions d''européens en souffrent, et on estime qu''ils seront deux fois plus d''ici 2020. Pourtant, la maladie évolue dans l''indifférence la plus totale.\r\n\r\n<img class="alignright" src="https://www.croi.ie/sites/default/files/field/image/heart-attack-03.jpg" alt="" width="277" height="179" />\r\n\r\n500 000 personnes souffrent d''insuffisance cardiaque en France et 32 000 en meurent chaque année. Diagnostics et traitements sont insuffisants. Ce sont des milliers de décès qui pourraient être évités.\r\n\r\nL''insuffisance cardiaque, peu connue du grand public\r\n\r\nA la question "savez-vous ce qu''est l''insuffisance cardiaque ?" Les Français répondent "non" en majorité. Tels sont les résultats d''une étude européenne : "l''étude Shape". Réalisée sur 15 000 personnes, l''étude montre que 86 % des personnes interrogées déclarent avoir entendu parler de la maladie mais en ignorent les symptômes. "Alors que le nombre de malades ne cesse d''augmenter, il est impératif de remédier à la trop grande méconnaissance de cette maladie grave" s''insurge Alain Cohen-Solal, professeur de cardiologie. L''insuffisance cardiaque est responsable de trop nombreux morts. 40 % des insuffisants cardiaques décèdent dans l''année qui suit leur première hospitalisation et 60 % dans les cinq ans qui suivent le diagnostic.\r\n\r\n&nbsp;\r\n\r\nQu''est ce que l''insuffisance cardiaque ?\r\n\r\nInsuffisance cardiaque coeur santé maladies cardiovasculaires Les symptômes de cette maladie sont très handicapants et seulement 5 % des européens ont su les décrire : fatigue extrême, oedème des chevilles, essoufflement à l''effort et au repos. Des signes qui ne trompent pas et qui sont liés à la défaillance du muscle cardiaque.\r\n\r\nUne défaillance qui apparaît lentement. Après plusieurs années d''évolution de la maladie, le coeur retarde l''arrivée des symptômes en se dilatant pour accroître l''afflux sanguin, en s''épaississant pour pouvoir se contracter plus puissamment, et en accélérant le rythme cardiaque. Mais au bout d''un certain temps, il ne peut plus compenser le manque : le muscle cardiaque n''est plus aussi énergique qu''à l''habitude. Il envoie moins vite et moins bien le sang au reste du corps. La défaillance du muscle cardiaque peut être due à de multiples causes dont les deux plus fréquentes sont la maladie coronaire (faisant notamment suite à un infarctus) et l''hypertension artérielle. Aussi il est important de ne pas négliger les principaux facteurs de risque : l''hypercholestérolémie, le tabagisme, l''hypertension artérielle, le diabète et l''obésité. "Des facteurs qu''il ne faut pas prendre à la légère. comme les signes précurseurs de la maladie lorsqu''ils apparaissent" précise Le Pr. Solal. Les plus fréquents sont des difficultés à respirer à l''effort comme au repos, des oedèmes au niveau des chevilles et une fatigue constante. Les autres symptômes, moins évidents, sont une accélération du rythme cardiaque (tachycardie), un état de confusion, lié à l''oxygénation insuffisante du cerveau, ou encore une perte d''appétit.\r\n\r\n&nbsp;', 'Insuffisance et crise cardiaque', '', 'inherit', 'open', 'open', '', '134-revision-v1', '', '', '2015-06-10 09:33:11', '2015-06-10 08:33:11', '', 134, 'http://127.0.0.1/docoeur/2015/06/10/134-revision-v1/', 0, 'revision', '', 0),
(139, 1, '2015-06-10 12:16:47', '2015-06-10 11:16:47', '', 'apple-icon-57x57', '', 'inherit', 'open', 'open', '', 'apple-icon-57x57', '', '', '2015-06-10 12:16:47', '2015-06-10 11:16:47', '', 0, 'http://127.0.0.1/docoeur/wp-content/uploads/2015/06/apple-icon-57x57.png', 0, 'attachment', 'image/png', 0),
(140, 1, '2015-06-10 12:16:48', '2015-06-10 11:16:48', '', 'apple-icon-76x76', '', 'inherit', 'open', 'open', '', 'apple-icon-76x76', '', '', '2015-06-10 12:16:48', '2015-06-10 11:16:48', '', 0, 'http://127.0.0.1/docoeur/wp-content/uploads/2015/06/apple-icon-76x76.png', 0, 'attachment', 'image/png', 0),
(141, 1, '2015-06-10 12:16:48', '2015-06-10 11:16:48', '', 'apple-icon-120x120', '', 'inherit', 'open', 'open', '', 'apple-icon-120x120', '', '', '2015-06-10 12:16:48', '2015-06-10 11:16:48', '', 0, 'http://127.0.0.1/docoeur/wp-content/uploads/2015/06/apple-icon-120x120.png', 0, 'attachment', 'image/png', 0),
(142, 1, '2015-06-10 12:16:49', '2015-06-10 11:16:49', '', 'apple-icon-144x144', '', 'inherit', 'open', 'open', '', 'apple-icon-144x144', '', '', '2015-06-10 12:16:49', '2015-06-10 11:16:49', '', 0, 'http://127.0.0.1/docoeur/wp-content/uploads/2015/06/apple-icon-144x144.png', 0, 'attachment', 'image/png', 0),
(143, 1, '2015-06-10 12:16:49', '2015-06-10 11:16:49', '', 'apple-icon-152x152', '', 'inherit', 'open', 'open', '', 'apple-icon-152x152', '', '', '2015-06-10 12:16:49', '2015-06-10 11:16:49', '', 0, 'http://127.0.0.1/docoeur/wp-content/uploads/2015/06/apple-icon-152x152.png', 0, 'attachment', 'image/png', 0),
(144, 1, '2015-06-10 12:16:50', '2015-06-10 11:16:50', '', 'favicon', '', 'inherit', 'open', 'open', '', 'favicon', '', '', '2015-06-10 12:16:50', '2015-06-10 11:16:50', '', 0, 'http://127.0.0.1/docoeur/wp-content/uploads/2015/06/favicon.ico', 0, 'attachment', 'image/x-icon', 0),
(145, 1, '2015-06-10 12:18:03', '2015-06-10 11:18:03', '', 'Favicon', '', 'inherit', 'open', 'open', '', 'favicon-2', '', '', '2015-06-10 12:18:03', '2015-06-10 11:18:03', '', 0, 'http://127.0.0.1/docoeur/wp-content/uploads/2015/06/Favicon.jpg', 0, 'attachment', 'image/jpeg', 0),
(146, 3, '2015-06-10 15:13:02', '2015-06-10 14:13:02', '', 'Hydrangeas', '', 'inherit', 'open', 'open', '', 'hydrangeas', '', '', '2015-06-10 15:13:02', '2015-06-10 14:13:02', '', 0, 'http://127.0.0.1/docoeur/?attachment_id=146', 0, 'attachment', 'image/jpeg', 0),
(147, 5, '2015-06-10 15:15:53', '2015-06-10 14:15:53', '', 'valves-cardiaques', '', 'inherit', 'open', 'open', '', 'valves-cardiaques', '', '', '2015-06-10 15:16:08', '2015-06-10 14:16:08', '', 148, 'http://127.0.0.1/docoeur/?attachment_id=147', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `dcHyv_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(148, 5, '2015-06-10 15:19:04', '2015-06-10 14:19:04', '<strong>Quatre valves orchestrent la circulation sanguine au niveau du coeur. Mais lorsque ces clapets sont en panne et que le traitement médicamenteux n''est pas suffisant, une opération s''impose. Cette chirurgie bénéficie depuis peu d''un progrès important : une valve aortique sans suture.</strong>\r\n\r\nFort des bons résultats obtenus lors des essais cliniques, la première valve aortique sans suture a été implantée chez un patient souffrant d''athérosclérose et présentant un rétrécissement de l''aorte se traduisant par un<span class="Apple-converted-space"> </span><a href="http://www.doctissimo.fr/html/dossiers/maladies_cardiovasculaires/articles/12336-souffle-au-coeur.htm" target="_self">souffle au coeur</a>. Retour sur cette intervention, avec le Pr François Laborde, chef du département de chirurgie cardiaque à l''Institut Mutualiste Montsouris (Paris), qui a réalisé l''intervention sous l''oeil des caméras.\r\n<h2>2 % des Français ont une valvulopathie</h2>\r\nLes valves cardiaques sont des structures élastiques du coeur. Ces 4 valves agissent comme de petits clapets qui séparent les différentes cavités pour que le sang n''y circule que dans un sens, sans retour en arrière. Ces valves peuvent être atteintes de deux manières : elle ne s''ouvre pas assez et empêche le sang de circuler, on parle de rétrécissement de valve ou de sténose (46 % des cas) ; elle ne se ferme pas, le manque de continence provoque une fuite appelée insuffisance (12 % des cas) ; et enfin, elle peut présenter les deux types de dysfonctionnements (42 %). Environ 2 % de la population adulte est porteuse d''une<span class="Apple-converted-space"> </span><a href="http://www.doctissimo.fr/html/sante/encyclopedie/sa_1100_valvulopa01.htm" target="_self">valvulopathie</a>.\r\n\r\n&nbsp;', 'Pose de la 1ère valve aortique sans suture', '', 'publish', 'open', 'open', '', 'pose-de-la-1ere-valve-aortique-sans-suture', '', '', '2015-06-10 15:19:04', '2015-06-10 14:19:04', '', 0, 'http://127.0.0.1/docoeur/?p=148', 0, 'post', '', 0),
(149, 4, '2015-06-10 15:18:24', '2015-06-10 14:18:24', '<strong>Quatre valves orchestrent la circulation sanguine au niveau du coeur. Mais lorsque ces clapets sont en panne et que le traitement médicamenteux n''est pas suffisant, une opération s''impose. Cette chirurgie bénéficie depuis peu d''un progrès important : une valve aortique sans suture.</strong>\r\n\r\nFort des bons résultats obtenus lors des essais cliniques, la première valve aortique sans suture a été implantée chez un patient souffrant d''athérosclérose et présentant un rétrécissement de l''aorte se traduisant par un<span class="Apple-converted-space"> </span><a href="http://www.doctissimo.fr/html/dossiers/maladies_cardiovasculaires/articles/12336-souffle-au-coeur.htm" target="_self">souffle au coeur</a>. Retour sur cette intervention, avec le Pr François Laborde, chef du département de chirurgie cardiaque à l''Institut Mutualiste Montsouris (Paris), qui a réalisé l''intervention sous l''oeil des caméras.\r\n<h2>2 % des Français ont une valvulopathie</h2>\r\nLes valves cardiaques sont des structures élastiques du coeur. Ces 4 valves agissent comme de petits clapets qui séparent les différentes cavités pour que le sang n''y circule que dans un sens, sans retour en arrière. Ces valves peuvent être atteintes de deux manières : elle ne s''ouvre pas assez et empêche le sang de circuler, on parle de rétrécissement de valve ou de sténose (46 % des cas) ; elle ne se ferme pas, le manque de continence provoque une fuite appelée insuffisance (12 % des cas) ; et enfin, elle peut présenter les deux types de dysfonctionnements (42 %). Environ 2 % de la population adulte est porteuse d''une<span class="Apple-converted-space"> </span><a href="http://www.doctissimo.fr/html/sante/encyclopedie/sa_1100_valvulopa01.htm" target="_self">valvulopathie</a>.\r\n\r\n&nbsp;', 'Pose de la 1ère valve aortique sans suture', '', 'inherit', 'open', 'open', '', '148-revision-v1', '', '', '2015-06-10 15:18:24', '2015-06-10 14:18:24', '', 148, 'http://127.0.0.1/docoeur/2015/06/10/148-revision-v1/', 0, 'revision', '', 0),
(150, 3, '2015-06-10 15:23:09', '2015-06-10 14:23:09', 'Bonjour, est-il dangereux de faire du sport quelques heures après un don de sang ?', 'Est-il dangereux de faire du sport quelques heures après un don de sang?', '', 'publish', 'open', 'open', '', 'est-il-dangereux-de-faire-du-sport-quelques-heures-apres-un-don-de-sang', '', '', '2015-06-11 09:48:43', '2015-06-11 08:48:43', '', 0, 'http://127.0.0.1/docoeur/question/est-il-dangereux-de-faire-du-sport-quelques-heures-apres-un-don-de-sang/', 0, 'dwqa-question', '', 0),
(153, 4, '2015-06-10 15:29:21', '2015-06-10 14:29:21', 'Difficile à dire cela doit dépendre des personnes, un spécialiste peu tîl vérifier mes propos svp ?', 'répondreEst-il dangereux de faire du sport quelques heures après un don de sang?', '', 'publish', 'open', 'open', '', 'repondreest-il-dangereux-de-faire-du-sport-quelques-heures-apres-un-don-de-sang', '', '', '2015-06-10 15:56:14', '2015-06-10 14:56:14', '', 0, 'http://127.0.0.1/docoeur/dwqa-answer/repondreest-il-dangereux-de-faire-du-sport-quelques-heures-apres-un-don-de-sang/', 0, 'dwqa-answer', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_terms`
--

CREATE TABLE IF NOT EXISTS `dcHyv_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Contenu de la table `dcHyv_terms`
--

INSERT INTO `dcHyv_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Non classé', 'non-classe', 0),
(2, 'Questions', 'questions', 0),
(3, 'Menu 1', 'menu-1', 0),
(4, 'Espace Spécialiste', 'espace-specialiste', 0),
(5, 'Récits vécus', 'recits-vecus', 0),
(6, 'coeur', 'coeur', 0),
(7, 'omega3', 'omega3', 0),
(8, 'santé', 'sante', 0),
(9, 'cholesterol', 'cholesterol', 0),
(10, 'coeur hypertension', 'coeur-hypertension', 0),
(11, 'hypertension', 'hypertension', 0),
(12, 'alimentation', 'alimentation', 0),
(13, 'maladie cardiaque', 'maladie-cardiaque', 0),
(14, 'post-format-quote', 'post-format-quote', 0),
(15, 'captacha', 'captacha', 0),
(16, 'don de sang', 'don-de-sang', 0);

-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_term_relationships`
--

CREATE TABLE IF NOT EXISTS `dcHyv_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `dcHyv_term_relationships`
--

INSERT INTO `dcHyv_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(10, 3, 0),
(12, 3, 0),
(15, 3, 0),
(16, 3, 0),
(29, 3, 0),
(28, 3, 0),
(30, 3, 0),
(31, 3, 0),
(34, 3, 0),
(35, 3, 0),
(36, 3, 0),
(32, 3, 0),
(64, 4, 0),
(64, 6, 0),
(64, 7, 0),
(64, 8, 0),
(64, 9, 0),
(118, 3, 0),
(69, 3, 0),
(67, 6, 0),
(67, 11, 0),
(70, 12, 0),
(70, 13, 0),
(67, 5, 0),
(134, 5, 0),
(148, 4, 0),
(150, 2, 0),
(150, 16, 0);

-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `dcHyv_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Contenu de la table `dcHyv_term_taxonomy`
--

INSERT INTO `dcHyv_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'dwqa-question_category', '', 0, 1),
(3, 3, 'nav_menu', '', 0, 14),
(4, 4, 'category', 'Cette section contient les articles publiés par les Spécialistes de la santé.', 0, 2),
(5, 5, 'category', 'Ici, vous pouvez lire des articles rédigés autant par les Membres que des Spécialistes.  A coté du nom de l''auteur de l''article, vous retrouverez une icône permettant d''identifier son statut. Plus de détails <a href="/faq-en-general">ici</a>.', 0, 1),
(6, 6, 'post_tag', '', 0, 2),
(7, 7, 'post_tag', '', 0, 1),
(8, 8, 'post_tag', '', 0, 1),
(9, 9, 'post_tag', '', 0, 1),
(10, 10, 'post_tag', '', 0, 0),
(11, 11, 'post_tag', '', 0, 1),
(12, 12, 'dwqa-question_tag', '', 0, 1),
(13, 13, 'dwqa-question_tag', '', 0, 1),
(14, 14, 'post_format', '', 0, 0),
(15, 15, 'dwqa-question_tag', '', 0, 0),
(16, 16, 'dwqa-question_tag', '', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_usermeta`
--

CREATE TABLE IF NOT EXISTS `dcHyv_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=146 ;

--
-- Contenu de la table `dcHyv_usermeta`
--

INSERT INTO `dcHyv_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'dcHyv_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'dcHyv_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'wp360_locks,wp390_widgets,wp410_dfw,document,settings'),
(13, 1, 'show_welcome_panel', '0'),
(14, 1, 'session_tokens', 'a:5:{s:64:"1adaa8c0d8cbc2547b8e5543e6d540b9d5496a81fc9ba8bfbc7c7a39131cad03";a:4:{s:10:"expiration";i:1434035418;s:2:"ip";s:12:"10.192.189.9";s:2:"ua";s:120:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36";s:5:"login";i:1433862618;}s:64:"e0ed6b4bc617b50f9359d2373955161f1e0074fe2043cae584da418945c90af8";a:4:{s:10:"expiration";i:1434058676;s:2:"ip";s:15:"178.198.239.240";s:2:"ua";s:108:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36";s:5:"login";i:1433885876;}s:64:"1524a49a84b8957cf781dcacb621aa53bd391c2572a29f03bd2a057aba67c3a5";a:4:{s:10:"expiration";i:1434098391;s:2:"ip";s:12:"10.192.189.9";s:2:"ua";s:120:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36";s:5:"login";i:1433925591;}s:64:"f8056559ac647ba64dd2a5395fb68e26f3c2e1ac680f2637795356d421405240";a:4:{s:10:"expiration";i:1434184127;s:2:"ip";s:12:"10.192.189.9";s:2:"ua";s:121:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36";s:5:"login";i:1434011327;}s:64:"0f00a262ced4a3c2ceba1920e109dae81574d91fabd050a7ff1eaa30e672ef90";a:4:{s:10:"expiration";i:1434196714;s:2:"ip";s:12:"10.192.189.9";s:2:"ua";s:121:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36";s:5:"login";i:1434023914;}}'),
(15, 1, 'dcHyv_dashboard_quick_press_last_post_id', '126'),
(16, 1, 'dcHyv_user-settings', 'unfold=1&mfold=o&editor=tinymce&libraryContent=browse&hidetb=0&wplink=1'),
(17, 1, 'dcHyv_user-settings-time', '1434011342'),
(24, 2, 'nickname', 'specialiste1'),
(18, 1, 'managenav-menuscolumnshidden', 'a:3:{i:0;s:11:"link-target";i:1;s:3:"xfn";i:2;s:11:"description";}'),
(20, 1, 'nav_menu_recently_edited', '3'),
(19, 1, 'metaboxhidden_nav-menus', 'a:7:{i:0;s:8:"add-post";i:1;s:17:"add-dwqa-question";i:2;s:15:"add-dwqa-answer";i:3;s:12:"add-post_tag";i:4;s:15:"add-post_format";i:5;s:26:"add-dwqa-question_category";i:6;s:21:"add-dwqa-question_tag";}'),
(23, 1, 'theme_my_login_security', 'a:4:{s:9:"is_locked";b:0;s:15:"lock_expiration";i:0;s:10:"unlock_key";s:0:"";s:21:"failed_login_attempts";a:3:{i:0;a:2:{s:4:"time";i:1433942218;s:2:"ip";s:12:"10.192.189.9";}i:1;a:2:{s:4:"time";i:1433942236;s:2:"ip";s:12:"10.192.189.9";}i:2;a:2:{s:4:"time";i:1433942255;s:2:"ip";s:12:"10.192.189.9";}}}'),
(21, 1, 'closedpostboxes_page', 'a:0:{}'),
(22, 1, 'metaboxhidden_page', 'a:2:{i:0;s:10:"postcustom";i:1;s:9:"authordiv";}'),
(25, 2, 'first_name', ''),
(26, 2, 'last_name', ''),
(27, 2, 'description', ''),
(28, 2, 'rich_editing', 'true'),
(29, 2, 'comment_shortcuts', 'false'),
(30, 2, 'admin_color', 'fresh'),
(31, 2, 'use_ssl', '0'),
(32, 2, 'show_admin_bar_front', 'true'),
(33, 2, 'dcHyv_capabilities', 'a:1:{s:11:"specialiste";b:1;}'),
(34, 2, 'dcHyv_user_level', '0'),
(35, 2, 'default_password_nag', ''),
(37, 3, 'nickname', 'membre1'),
(38, 3, 'first_name', ''),
(39, 3, 'last_name', ''),
(40, 3, 'description', ''),
(41, 3, 'rich_editing', 'true'),
(42, 3, 'comment_shortcuts', 'false'),
(43, 3, 'admin_color', 'fresh'),
(44, 3, 'use_ssl', '0'),
(45, 3, 'show_admin_bar_front', 'false'),
(46, 3, 'dcHyv_capabilities', 'a:1:{s:6:"membre";b:1;}'),
(47, 3, 'dcHyv_user_level', '0'),
(48, 3, 'default_password_nag', ''),
(51, 4, 'nickname', 'moderateur1'),
(52, 4, 'first_name', ''),
(53, 4, 'last_name', ''),
(54, 4, 'description', ''),
(55, 4, 'rich_editing', 'true'),
(56, 4, 'comment_shortcuts', 'false'),
(50, 2, 'session_tokens', 'a:2:{s:64:"e4dad06f2354526b055e9539d26da13c03a80086ad92c12aa1eacc71a01fcda6";a:4:{s:10:"expiration";i:1433329022;s:2:"ip";s:17:"127.0.0.1/docoeur";s:2:"ua";s:82:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:38.0) Gecko/20100101 Firefox/38.0";s:5:"login";i:1433156222;}s:64:"72b825bf587b04356f89705828e831d327e5ce44790f12926b1de002a856e477";a:4:{s:10:"expiration";i:1433489421;s:2:"ip";s:17:"127.0.0.1/docoeur";s:2:"ua";s:82:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:38.0) Gecko/20100101 Firefox/38.0";s:5:"login";i:1433316621;}}'),
(57, 4, 'admin_color', 'fresh'),
(58, 4, 'use_ssl', '0'),
(59, 4, 'show_admin_bar_front', 'false'),
(60, 4, 'dcHyv_capabilities', 'a:1:{s:10:"moderateur";b:1;}'),
(61, 4, 'dcHyv_user_level', '0'),
(62, 4, 'default_password_nag', ''),
(68, 3, 'session_tokens', 'a:2:{s:64:"7a0bc5599b6d6f30a8cc9908ea8f06d8fcdcbc5542fc14a8edf027328503bcb4";a:4:{s:10:"expiration";i:1434098514;s:2:"ip";s:12:"10.192.189.9";s:2:"ua";s:82:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:38.0) Gecko/20100101 Firefox/38.0";s:5:"login";i:1433925714;}s:64:"b0e0fcf61b4b0a756bb3494e9fcd499b9cabd7585a6f34e27a49c6f3ce480e65";a:4:{s:10:"expiration";i:1434119156;s:2:"ip";s:12:"10.192.189.9";s:2:"ua";s:72:"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0";s:5:"login";i:1433946356;}}'),
(64, 4, 'wpuf_postlock', 'no'),
(65, 4, 'wpuf_lock_cause', ''),
(66, 4, 'wpuf_sub_validity', ''),
(67, 4, 'wpuf_sub_pcount', ''),
(69, 1, 'wp_media_library_mode', 'list'),
(71, 4, 'dcHyv_user-settings', 'hidetb=0&libraryContent=browse&editor_expand=off&mfold=o'),
(72, 4, 'dcHyv_user-settings-time', '1433942635'),
(73, 4, 'dcHyv_dashboard_quick_press_last_post_id', '76'),
(74, 3, 'wpuf_postlock', 'no'),
(75, 3, 'wpuf_lock_cause', ''),
(76, 3, 'wpuf_sub_validity', ''),
(77, 3, 'wpuf_sub_pcount', ''),
(78, 4, 'wp_media_library_mode', 'list'),
(79, 2, 'dcHyv_user-settings', 'libraryContent=browse'),
(80, 2, 'dcHyv_user-settings-time', '1433323834'),
(81, 3, 'dcHyv_user-settings', 'editor=tinymce&libraryContent=browse'),
(82, 3, 'dcHyv_user-settings-time', '1433929546'),
(83, 3, 'dismissed_wp_pointers', 'wp410_dfw,document'),
(84, 3, 'dcHyv_dashboard_quick_press_last_post_id', '91'),
(85, 2, 'dcHyv_dashboard_quick_press_last_post_id', '94'),
(87, 4, 'dismissed_wp_pointers', 'wp410_dfw,document,settings'),
(88, 1, 'closedpostboxes_post', 'a:0:{}'),
(89, 1, 'metaboxhidden_post', 'a:7:{i:0;s:12:"revisionsdiv";i:1;s:11:"postexcerpt";i:2;s:13:"trackbacksdiv";i:3;s:16:"commentstatusdiv";i:4;s:11:"commentsdiv";i:5;s:7:"slugdiv";i:6;s:9:"authordiv";}'),
(90, 1, 'meta-box-order_post', 'a:3:{s:4:"side";s:61:"submitdiv,formatdiv,categorydiv,tagsdiv-post_tag,postimagediv";s:6:"normal";s:96:"revisionsdiv,postexcerpt,trackbacksdiv,postcustom,commentstatusdiv,commentsdiv,slugdiv,authordiv";s:8:"advanced";s:0:"";}'),
(91, 1, 'screen_layout_post', '1'),
(92, 1, 'manageedit-dwqa-questioncolumnshidden', 'a:6:{i:0;s:12:"question-tag";i:1;s:8:"comments";i:2;s:4:"info";i:3;s:0:"";i:4;s:0:"";i:5;s:0:"";}'),
(93, 1, 'edit_dwqa-question_per_page', '20'),
(94, 5, 'nickname', 'House'),
(95, 5, 'first_name', 'Gregory'),
(96, 5, 'last_name', 'House'),
(97, 5, 'description', ''),
(98, 5, 'rich_editing', 'true'),
(99, 5, 'comment_shortcuts', 'false'),
(100, 5, 'admin_color', 'fresh'),
(101, 5, 'use_ssl', '0'),
(102, 5, 'show_admin_bar_front', 'false'),
(103, 5, 'dcHyv_capabilities', 'a:1:{s:11:"specialiste";b:1;}'),
(104, 5, 'dcHyv_user_level', '0'),
(105, 5, 'default_password_nag', ''),
(123, 6, 'first_name', ''),
(124, 6, 'last_name', ''),
(125, 6, 'description', ''),
(126, 6, 'rich_editing', 'true'),
(127, 6, 'comment_shortcuts', 'false'),
(122, 6, 'nickname', 'specialiste2'),
(128, 6, 'admin_color', 'fresh'),
(129, 6, 'use_ssl', '0'),
(130, 6, 'show_admin_bar_front', 'true'),
(131, 6, 'dcHyv_capabilities', 'a:1:{s:11:"specialiste";b:1;}'),
(132, 6, 'dcHyv_user_level', '0'),
(133, 6, 'default_password_nag', ''),
(134, 7, 'nickname', 'membre2'),
(135, 7, 'first_name', ''),
(136, 7, 'last_name', ''),
(137, 7, 'description', ''),
(138, 7, 'rich_editing', 'true'),
(139, 7, 'comment_shortcuts', 'false'),
(140, 7, 'admin_color', 'fresh'),
(141, 7, 'use_ssl', '0'),
(142, 7, 'show_admin_bar_front', 'true'),
(143, 7, 'dcHyv_capabilities', 'a:1:{s:6:"membre";b:1;}'),
(144, 7, 'dcHyv_user_level', '1'),
(145, 7, 'default_password_nag', '');

-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_users`
--

CREATE TABLE IF NOT EXISTS `dcHyv_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(64) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `dcHyv_users`
--

INSERT INTO `dcHyv_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BZraNYPoSgK3UB2UpoYngKascaUeOX1', 'admin', 'elie.turc@cpnv.ch', '', '2015-05-21 13:19:52', '', 0, 'admin'),
(2, 'specialiste1', '$P$BMUIz/x.OCAXqqiBFBpH/A/J0EvN4c1', 'specialiste1', 'specialiste1@test.ch', '', '2015-05-29 12:53:03', '', 0, 'specialiste1'),
(3, 'membre1', '$P$BtpwZiOPeXdy2kepNUTPfDrBuwTjGX0', 'membre1', 'membre1@test.ch', '', '2015-05-29 13:08:08', '', 0, 'membre1'),
(4, 'moderateur1', '$P$BHGvoQha90eROLRrnbffmJoUVaP7kW1', 'moderateur1', 'moderateur@test.ch', '', '2015-06-02 06:32:51', '', 0, 'moderateur1'),
(5, 'House', '$P$B/VfxgKM7BOM2ubqOejg4DaWdICnPz1', 'house', 'house@princeton.hospital.com', 'http://fr.wikipedia.org/wiki/Gregory_House', '2015-06-10 12:59:03', '', 0, 'House Gregory'),
(6, 'specialiste2', '$P$Bph24vKXunu5Hbe3M6t/xw1E9A6xl4/', 'specialiste2', 'specialiste2@test.ch', '', '2015-06-11 13:05:58', '', 0, 'specialiste2'),
(7, 'membre2', '$P$BXll2u0Aqsah8W4pKsXbhQQYV7JBuV0', 'membre2', 'membre2@test.ch', '', '2015-06-11 13:06:42', '', 0, 'membre2');

-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_wpuf_customfields`
--

CREATE TABLE IF NOT EXISTS `dcHyv_wpuf_customfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field` varchar(30) NOT NULL,
  `type` varchar(20) NOT NULL,
  `values` text NOT NULL,
  `label` varchar(200) NOT NULL,
  `desc` varchar(200) NOT NULL,
  `required` varchar(5) NOT NULL,
  `region` varchar(20) NOT NULL DEFAULT 'top',
  `order` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `dcHyv_wpuf_customfields`
--


-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_wpuf_subscription`
--

CREATE TABLE IF NOT EXISTS `dcHyv_wpuf_subscription` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `count` int(5) DEFAULT '0',
  `duration` int(5) NOT NULL DEFAULT '0',
  `cost` float NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `dcHyv_wpuf_subscription`
--


-- --------------------------------------------------------

--
-- Structure de la table `dcHyv_wpuf_transaction`
--

CREATE TABLE IF NOT EXISTS `dcHyv_wpuf_transaction` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending_payment',
  `cost` varchar(255) DEFAULT '',
  `post_id` bigint(20) DEFAULT NULL,
  `pack_id` bigint(20) DEFAULT NULL,
  `payer_first_name` longtext,
  `payer_last_name` longtext,
  `payer_email` longtext,
  `payment_type` longtext,
  `payer_address` longtext,
  `transaction_id` longtext,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `dcHyv_wpuf_transaction`
--


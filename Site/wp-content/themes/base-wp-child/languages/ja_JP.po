msgid ""
msgstr ""
"Project-Id-Version: Base WP v1.18\n"
"Report-Msgid-Bugs-To: http://wordpress.org/tags/base-wp\n"
"POT-Creation-Date: 2014-06-08 04:11:30+00:00\n"
"PO-Revision-Date: 2014-06-25 15:09:25+0000\n"
"Last-Translator: ISHIKAWA Koutarou <stein2nd@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.6.5\n"
"X-Poedit-Language: \n"
"X-Poedit-Country: \n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;\n"
"X-Poedit-Basepath: \n"
"X-Poedit-Bookmarks: \n"
"X-Poedit-SearchPath-0: .\n"
"X-Textdomain-Support: yes"

#: 404.php:15
#@ base
msgid "Oops! That page can&rsquo;t be found."
msgstr "おぉっとぉ！そのページが見つかりません。"

#: 404.php:20
#@ base
msgid "It looks like nothing was found at this location. Maybe try one of the links below or a search?"
msgstr "この場所では、何も見つけられなかったようです。下のリンクのいずれか、或いは検索を試してみませんか？"

#: 404.php:28
#@ base
msgid "Most Used Categories"
msgstr "最も使用されているカテゴリー"

#. translators: %1$s: smiley
#: 404.php:45
#, php-format
#@ base
msgid "Try looking in the monthly archives. %1$s"
msgstr "月別アーカイブで探してみましょう。%1$s"

#: archive.php:27
#, php-format
#@ base
msgid "Author: %s"
msgstr "著者: %s"

#: archive.php:30
#, php-format
#@ base
msgid "Day: %s"
msgstr "日: %s"

#: archive.php:33
#, php-format
#@ base
msgid "Month: %s"
msgstr "月: %s"

#: archive.php:33
#@ base
msgctxt "monthly archives date format"
msgid "F Y"
msgstr "F Y"

#: archive.php:36
#, php-format
#@ base
msgid "Year: %s"
msgstr "年: %s"

#: archive.php:36
#@ base
msgctxt "yearly archives date format"
msgid "Y"
msgstr "Y"

#: archive.php:39
#@ base
msgid "Asides"
msgstr "アサイド"

#: archive.php:42
#@ base
msgid "Galleries"
msgstr "ギャラリー"

#: archive.php:45
#@ base
msgid "Images"
msgstr "画像"

#: archive.php:48
#@ base
msgid "Videos"
msgstr "動画"

#: archive.php:51
#@ base
msgid "Quotes"
msgstr "引用"

#: archive.php:54
#@ base
msgid "Links"
msgstr "リンク"

#: archive.php:57
#@ base
msgid "Statuses"
msgstr "ステータス"

#: archive.php:60
#@ base
msgid "Audios"
msgstr "音声"

#: archive.php:63
#@ base
msgid "Chats"
msgstr "チャット"

#: archive.php:66
#: sidebar.php:17
#@ base
msgid "Archives"
msgstr "アーカイブ"

#: comments.php:28
#, php-format
#@ base
msgctxt "comments title"
msgid "One thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] "&ldquo;%2$s&rdquo;に関する%1$s件のコメント"

#: comments.php:35
#: comments.php:52
#@ base
msgid "Comment navigation"
msgstr "コメント・ナビゲーション"

#: comments.php:36
#: comments.php:53
#@ base
msgid "&larr; Older Comments"
msgstr "&larr; 古いコメント"

#: comments.php:37
#: comments.php:54
#@ base
msgid "Newer Comments &rarr;"
msgstr "新しいコメント &rarr;"

#: comments.php:64
#@ base
msgid "Comments are closed."
msgstr "コメントは受け付けていません。"

#: content/content-none.php:13
#@ base
msgid "Nothing Found"
msgstr "何も見つからない"

#: content/content-none.php:19
#, php-format
#@ base
msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr "初めての記事を公開する準備ができましたか？<a href=\"%1$s\">ここで始めよう</a>。"

#: content/content-none.php:23
#@ base
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr "検索語句と何も一致しません。別のキーワードでもう一度やり直してください。"

#: content/content-none.php:28
#@ base
msgid "It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help."
msgstr "我々ではお探しの物を見つけることが出来ないようです。検索ならお手伝いできると思います。"

#: content/content-page.php:22
#: content/content-pagenotitle.php:21
#: content/content-single.php:25
#: content.php:33
#@ base
msgid "Pages:"
msgstr "ページ:"

#: content/content-page.php:27
#: content/content-pagenotitle.php:26
#: content/content-search.php:53
#: content/content-single.php:65
#: content.php:67
#@ base
msgid "Edit"
msgstr "編集"

#. translators: used between list items, there is a space after the comma
#: content/content-search.php:30
#: content/content-search.php:40
#: content/content-single.php:34
#: content/content-single.php:37
#: content.php:44
#: content.php:54
#@ base
msgid ", "
msgstr ""

#: content/content-single.php:42
#, php-format
#@ base
msgid "This entry was tagged %2$s. Bookmark the <a href=\"%3$s\" rel=\"bookmark\">permalink</a>."
msgstr "このエントリーは%2$sとタグ付けされている。<a href=\"%3$s\" rel=\"bookmark\">パーマリンク</a>をブックマークする。"

#: content/content-single.php:44
#, php-format
#@ base
msgid "Bookmark the <a href=\"%3$s\" rel=\"bookmark\">permalink</a>."
msgstr "<a href=\"%3$s\" rel=\"bookmark\">パーマリンク</a>をブックマークする。"

#: content/content-single.php:50
#, php-format
#@ base
msgid "This entry was posted in %1$s and tagged %2$s. Bookmark the <a href=\"%3$s\" rel=\"bookmark\">permalink</a>."
msgstr "このエントリーは%1$sに投稿され、%2$sとタグ付けされている。<a href=\"%3$s\" rel=\"bookmark\">パーマリンク</a>をブックマークする。"

#: content/content-single.php:52
#, php-format
#@ base
msgid "This entry was posted in %1$s. Bookmark the <a href=\"%3$s\" rel=\"bookmark\">permalink</a>."
msgstr "このエントリーは%1$sに投稿された。<a href=\"%3$s\" rel=\"bookmark\">パーマリンク</a>をブックマークする。"

#: content.php:30
#@ base
msgid "Continue reading <span class=\"meta-nav\">&rarr;</span>"
msgstr "続きを読む <span class=\"meta-nav\">&rarr;</span>"

#: content/content-search.php:34
#: content.php:48
#, php-format
#@ base
msgid "Posted in %1$s"
msgstr "%1$sに投稿された"

#: content/content-search.php:44
#: content.php:58
#, php-format
#@ base
msgid "Tagged %1$s"
msgstr "タグ: %1$s"

#: content/content-search.php:50
#: content.php:64
#@ base
msgid "Leave a comment"
msgstr "コメントを残す"

#: content/content-search.php:50
#: content.php:64
#@ base
msgid "1 Comment"
msgstr "1件のコメント"

#: content/content-search.php:50
#: content.php:64
#@ base
msgid "% Comments"
msgstr "%件のコメント"

#: footer.php:20
#@ base
msgid "&copy;"
msgstr "&copy;"

#: footer.php:24
#, php-format
#@ base
msgid "Theme: %1$s by %2$s "
msgstr "テーマ: %1$s (%2$s 作成)"

#: footer.php:26
#@ base
msgid "Powered by "
msgstr "Powered by "

#: footer.php:27
#, php-format
#@ base
msgid "%s"
msgstr "%s"

#: functions.php:46
#@ base
msgid "Primary Menu"
msgstr "プライマリー・メニュー"

#: functions.php:75
#@ base
msgid "Sidebar"
msgstr "サイドバー"

#: functions.php:85
#@ base
msgid "First Footer Widget Area"
msgstr "第１フッター・ウィジェットエリア"

#: functions.php:87
#@ base
msgid "The first footer widget area"
msgstr "第１フッター・ウィジェットエリア"

#: functions.php:96
#@ base
msgid "Second Footer Widget Area"
msgstr "第２フッター・ウィジェットエリア"

#: functions.php:98
#@ base
msgid "The second footer widget area"
msgstr "第２フッター・ウィジェットエリア"

#: functions.php:107
#@ base
msgid "Third Footer Widget Area"
msgstr "第３フッター・ウィジェットエリア"

#: functions.php:109
#@ base
msgid "The third footer widget area"
msgstr "第３フッター・ウィジェットエリア"

#: functions.php:118
#@ base
msgid "Fourth Footer Widget Area"
msgstr "第４フッター・ウィジェットエリア"

#: functions.php:120
#@ base
msgid "The fourth footer widget area"
msgstr "第４フッター・ウィジェットエリア"

#: header.php:44
#@ base
msgid "Menu"
msgstr "メニュー"

#: header.php:45
#@ base
msgid "Skip to content"
msgstr "コンテンツへ移動"

#: inc/extras.php:63
#, php-format
#@ base
msgid "Page %s"
msgstr "ページ %s"

#: inc/includes/class-options-framework-admin.php:85
#: inc/includes/class-options-framework-admin.php:86
#: inc/includes/class-options-framework-admin.php:305
#@ base
msgid "Theme Options"
msgstr "テーマ・オプション"

#: inc/includes/class-options-framework-admin.php:171
#@ base
msgid "Save Options"
msgstr "オプションを保存"

#: inc/includes/class-options-framework-admin.php:172
#@ base
msgid "Restore Defaults"
msgstr "デフォルトをレストア"

#: inc/includes/class-options-framework-admin.php:172
#@ base
msgid "Click OK to reset. Any theme settings will be lost!"
msgstr "OKをクリックするとリセットします。テーマ設定が失われます！"

#: inc/includes/class-options-framework-admin.php:203
#@ base
msgid "Default options restored."
msgstr "デフォルトオプションがレストアされました。"

#: inc/includes/class-options-framework-admin.php:257
#@ base
msgid "Options saved."
msgstr "オプションが保存されました。"

#: inc/includes/class-options-media-uploader.php:63
#@ base
msgid "No file chosen"
msgstr "ファイルが選択されていません"

#: inc/includes/class-options-media-uploader.php:66
#: inc/includes/class-options-media-uploader.php:119
#@ base
msgid "Upload"
msgstr "アップロード"

#: inc/includes/class-options-media-uploader.php:68
#: inc/includes/class-options-media-uploader.php:120
#@ base
msgid "Remove"
msgstr "削除"

#: inc/includes/class-options-media-uploader.php:71
#@ base
msgid "Upgrade your version of WordPress for full media support."
msgstr "WordPressのバージョンをアップグレードして、全メディアに対応する。"

#: inc/includes/class-options-media-uploader.php:95
#@ base
msgid "View File"
msgstr "ファイルを表示"

#: inc/includes/class-options-sanitization.php:241
#@ base
msgid "No Repeat"
msgstr "繰り返さない"

#: inc/includes/class-options-sanitization.php:242
#@ base
msgid "Repeat Horizontally"
msgstr "水平方向に繰り返す"

#: inc/includes/class-options-sanitization.php:243
#@ base
msgid "Repeat Vertically"
msgstr "垂直方向に繰り返す"

#: inc/includes/class-options-sanitization.php:244
#@ base
msgid "Repeat All"
msgstr "水平方向と垂直方向に繰り返す"

#: inc/includes/class-options-sanitization.php:257
#@ base
msgid "Top Left"
msgstr "上左"

#: inc/includes/class-options-sanitization.php:258
#@ base
msgid "Top Center"
msgstr "上センター"

#: inc/includes/class-options-sanitization.php:259
#@ base
msgid "Top Right"
msgstr "上右"

#: inc/includes/class-options-sanitization.php:260
#@ base
msgid "Middle Left"
msgstr "中央左"

#: inc/includes/class-options-sanitization.php:261
#@ base
msgid "Middle Center"
msgstr "中央センター"

#: inc/includes/class-options-sanitization.php:262
#@ base
msgid "Middle Right"
msgstr "中央右"

#: inc/includes/class-options-sanitization.php:263
#@ base
msgid "Bottom Left"
msgstr "下左"

#: inc/includes/class-options-sanitization.php:264
#@ base
msgid "Bottom Center"
msgstr "下センター"

#: inc/includes/class-options-sanitization.php:265
#@ base
msgid "Bottom Right"
msgstr "下右"

#: inc/includes/class-options-sanitization.php:278
#@ base
msgid "Scroll Normally"
msgstr "通常にスクロールする"

#: inc/includes/class-options-sanitization.php:279
#@ base
msgid "Fixed in Place"
msgstr "位置を固定"

#: inc/includes/class-options-sanitization.php:353
#@ base
msgid "Normal"
msgstr "通常"

#: inc/includes/class-options-sanitization.php:354
#@ base
msgid "Italic"
msgstr "イタリック体"

#: inc/includes/class-options-sanitization.php:355
#@ base
msgid "Bold"
msgstr "ボールド体"

#: inc/includes/class-options-sanitization.php:356
#@ base
msgid "Bold Italic"
msgstr "ボールド・イタリック体"

#: inc/template-tags.php:21
#@ base
msgid "Posts navigation"
msgstr "記事ナビゲーション"

#: inc/template-tags.php:25
#@ base
msgid "<span class=\"meta-nav\">&larr;</span> Older posts"
msgstr "<span class=\"meta-nav\">&larr;</span> 古い記事"

#: inc/template-tags.php:29
#@ base
msgid "Newer posts <span class=\"meta-nav\">&rarr;</span>"
msgstr "新しい記事 <span class=\"meta-nav\">&rarr;</span>"

#: inc/template-tags.php:52
#@ base
msgid "Post navigation"
msgstr "記事ナビゲーション"

#: inc/template-tags.php:55
#, php-format
#@ base
msgctxt "Previous post link"
msgid "<span class=\"meta-nav\">&larr;</span> %title"
msgstr "<span class=\"meta-nav\">&larr;</span> %title"

#: inc/template-tags.php:56
#, php-format
#@ base
msgctxt "Next post link"
msgid "%title <span class=\"meta-nav\">&rarr;</span>"
msgstr "%title <span class=\"meta-nav\">&rarr;</span>"

#: options.php:65
#@ base
msgid "Link color"
msgstr "リンクの色"

#: options.php:116
#@ base
msgid "Footer text"
msgstr "フッターの文字列"

#: options.php:121
#@ base
msgid "Display credits link"
msgstr "クレジットリンクを表示する"

#: options.php:47
#@ base
msgid "Meta Slider"
msgstr "メタ・スライダー"

#: options.php:160
#@ base
msgid "Custom css"
msgstr "カスタムCSS"

#: options.php:165
#@ base
msgid "Footer code"
msgstr "フッター・コード"

#: options.php:41
#: options.php:193
#@ base
msgid "Logo Upload"
msgstr "ロゴ・アップロード"

#: options.php:211
#@ base
msgid "Favicon"
msgstr "Favicon"

#: options.php:232
#@ base
msgid "Upgrade to premium version"
msgstr "プレミアム版にアップグレードする"

#: options.php:234
#@ base
msgid "Upgrade to the premium version to get access to advanced options."
msgstr "プレミアム版にアップグレードして、詳細オプションにアクセスしましょう。"

#: options.php:238
#@ base
msgid "With premium version you have access to priority support."
msgstr "プレミアム版にすると、優先サポートを利用できます。"

#: options.php:239
#@ base
msgid "We offer a 7 day full refund if you are not happy with your purchase."
msgstr "ご満足戴けなかった場合には、7日間全額返金に応じます。"

#: search.php:16
#, php-format
#@ base
msgid "Search Results for: %s"
msgstr "検索結果: %s"

#: sidebar.php:24
#@ base
msgid "Meta"
msgstr "メタ"

#: inc/template-tags.php:82
#, php-format
#@ base
msgctxt "post date"
msgid "Posted on %s"
msgstr ""

#: inc/template-tags.php:87
#, php-format
#@ base
msgctxt "post author"
msgid "by %s"
msgstr ""

#: options.php:31
#@ base
msgid "General"
msgstr ""

#: options.php:35
#@ base
msgid "Add favicon"
msgstr ""

#: options.php:36
#@ base
msgid "Upload your favicon"
msgstr ""

#: options.php:42
#@ base
msgid "Upload your logo"
msgstr ""

#: options.php:51
#@ base
msgid "Style"
msgstr ""

#: options.php:55
#@ base
msgid "Header background"
msgstr ""

#: options.php:60
#@ base
msgid "Menu background color"
msgstr ""

#: options.php:70
#@ base
msgid "Link color on hover"
msgstr ""

#: options.php:75
#@ base
msgid "Footer background"
msgstr ""

#: options.php:79
#@ base
msgid "Blog &amp; Pages"
msgstr ""

#: options.php:83
#@ base
msgid "Pages comments"
msgstr ""

#: options.php:88
#@ base
msgid "Show blog and archive featured image"
msgstr ""

#: options.php:93
#@ base
msgid "Show post featured image"
msgstr ""

#: options.php:98
#@ base
msgid "Display post meta"
msgstr ""

#: options.php:103
#@ base
msgid "Index sidebar"
msgstr ""

#: options.php:108
#@ base
msgid "Post sidebar"
msgstr ""

#: options.php:112
#@ base
msgid "Footer"
msgstr ""

#: options.php:125
#@ base
msgid "Social"
msgstr ""

#: options.php:129
#@ base
msgid "Facebook url"
msgstr ""

#: options.php:130
#@ base
msgid "Add the url of your Facebook page"
msgstr ""

#: options.php:136
#@ base
msgid "Twitter url"
msgstr ""

#: options.php:137
#@ base
msgid "Add the url of your Twitter page"
msgstr ""

#: options.php:143
#@ base
msgid "Google plus url"
msgstr ""

#: options.php:144
#@ base
msgid "Add the url of your Google Plus page"
msgstr ""

#: options.php:150
#@ base
msgid "Pinterest url"
msgstr ""

#: options.php:151
#@ base
msgid "Add the url of your Pinterest page"
msgstr ""

#: options.php:156
#@ base
msgid "Advanced"
msgstr ""

#: options.php:28
#@ base
msgid "Only available in premium version"
msgstr ""


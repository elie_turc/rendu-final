<?php
/**
 * @package base
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
       <h1 class="entry-title" itemprop="name"><?php the_title(); ?></h1>
        <?php if(get_post_type()=="post"): ?>
       <div class="entry-meta"> <?php //Only for the posts, not he questions ?>
        <?php   base_posted_on(); //Display the date and author ?>
        <?php   $icon = get_icon("right"); //Get the role icon?>
        <?php   echo $icon; //Display role icon?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content" itemprop="articleBody">
    <?php 
	if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  	the_post_thumbnail('full', array('itemprop' => 'image'));} 
	?>
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'base' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-meta">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'base' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'base' ) );

			if ( ! base_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( 'Tags: <i class="fa fa-tag"></i> %2$s', 'base' );
				} else {
					$meta_text = __( 'Pas de tag associé', 'base' );
				}

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					if(strpos($category_list, "Espace Spécialiste"))
					{
						$meta_text = __( 'Catégorie: <i class="fa fa-stethoscope"></i> %1$s </br> Tags: <i class="fa fa-tags"></i> %2$s', 'base' );
					}
					elseif (strpos($category_list, "Récits vécus")) 
					{
						$meta_text = __( 'Catégorie: <i class="fa fa-heartbeat"></i> %1$s </br> Tags: <i class="fa fa-tags"></i> %2$s', 'base' );
					}
					else
					{
						$meta_text = __( 'Catégorie: %1$s </br> Tags: <i class="fa fa-tags"></i> %2$s', 'base' );						
					}
				} else {
					$meta_text = __( 'Catégorie: %1$s', 'base' );
				}

			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink()
			);
		?>
		<?php edit_post_link( __( 'Edit', 'base' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->

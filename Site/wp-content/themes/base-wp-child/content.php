<?php
/**
 * @package base
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title">
        	<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
        </h1>
	<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php base_posted_on(); ?>
			<?php $icon = get_icon("right"); //Get the icone role ?>
			<?php echo $icon; //Display the icone role ?>
		</div><!-- .entry-meta -->
	<?php endif; ?>
    </header><!-- .entry-header -->

	<div class="entry-content">
    	<?php if ( has_post_thumbnail()) : ?>
   				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
   			<?php the_post_thumbnail(); ?></a>
 		<?php endif; ?>
            
		<?php the_excerpt( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'base' ) ); ?>
		<a href="<?php echo get_permalink(); ?>"> Lire la Suite...</a>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'base' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-meta">
		<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
			<?php
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( __( ', ', 'base' ) );
				if ( $categories_list && base_categorized_blog() ) :
			?>
			<span class="cat-links">
				<?php printf( __( 'Category:', 'base' ) ); ?>
				<?php
				if(strpos($categories_list, "Espace Spécialiste"))
					{
                        printf( '<i class="fa fa-stethoscope"></i> '.$categories_list );
					}
					elseif (strpos($categories_list, "Récits vécus")) 
					{
                        printf( '<i class="fa fa-heartbeat"></i> '.$categories_list );
                        
                    }
					else
					{
						printf( __( 'Category: %1$s', 'base' ), $categories_list );
					}
                ?>
			</span>
            </br>
			<?php endif; // End if categories ?>

			<?php
				/* translators: used between list items, there is a space after the comma */
				$tags_list = get_the_tag_list( '', __( ', ', 'base' ) );
				if ( $tags_list ) :
			?>
			<span class="tags-links">
				<?php printf( __( 'Tagged:', 'base' ).'<i class="fa fa-tags"></i> '.$tags_list); ?>
			</span>
			<?php endif; // End if $tags_list ?>
		<?php endif; // End if 'post' == get_post_type() ?>
        </br>
		

		<?php edit_post_link( __( 'Edit', 'base' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
	<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
		<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'base' ), __( '1 Comment', 'base' ), __( '% Comments', 'base' ) ); ?></span>
		<?php endif; ?>
</article><!-- #post-## -->
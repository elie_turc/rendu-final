<?php 

//Ajout d'un theme enfant
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );
}


//Supression des rôles par défaut
remove_role('subscriber');
remove_role('editor');
remove_role('author');
remove_role('contributor');

//Autoriser l'utilisation de shortcode dans le menu
add_filter('wp_nav_menu_items', 'do_shortcode');


//Shortcode pour retourner le Pseudo utilisateur
add_shortcode( 'current-username' , 'ss_get_current_username' ); // Création d'un nouveau shortcode
function ss_get_current_username(){
    $user = wp_get_current_user();
    return $user->display_name;
}

//Retourne le role de l'auteur du post
function get_author_role()
{
    global $authordata;

    $author_roles = $authordata->roles;
    $author_role = array_shift($author_roles);

    return $author_role;
}

//Retourne l'icone de l'utilisateur en fonction de son role
//Retourne un string de type '<i class="fa fa-something"></i>'
function get_icon($position='', $size=1){
    if(get_author_role()=="membre"){$icon = '<i class="fa fa-user blue '.$position.' fa-'.$size.'x"></i>';}
    elseif(get_author_role()=="specialiste"){$icon = '<i class="fa fa-stethoscope green '.$position.' fa-'.$size.'x"></i>';}
    elseif(get_author_role()=="moderateur"){$icon = '<i class="fa fa-certificate orange '.$position.' fa-'.$size.'x"></i>';}
    elseif(get_author_role()=="administrator"){$icon = '<i class="fa fa-star yellow '.$position.' fa-'.$size.'x"></i>';}
    
    return $icon;
}


//Autorise l'utilisation de balise HTML dans la description des catégories
$filters = array('term_description','category_description','pre_term_description');
foreach ( $filters as $filter ) {
    remove_filter($filter, 'wp_filter_kses');
    remove_filter($filter, 'strip_tags');
}


//Affiche les pièce jointes liées à un article
add_filter( 'the_content', 'my_the_content_filter' );
function my_the_content_filter( $content ) {
	global $post;

	if ( is_single() && $post->post_type == 'post' && $post->post_status == 'publish' ) {
		$attachments = get_posts( array(
            'exclude' => get_post_thumbnail_id(),
			'post_type' => 'attachment',
			'posts_per_page' => 0,
			'post_parent' => $post->ID
		) );

		if ( $attachments ) {
			$content .= '<h3>Pièces jointes</h3>';
			$content .= '<ul class="post-attachments">';
			foreach ( $attachments as $attachment ) {
				$class = "post-attachment mime-" . sanitize_title( $attachment->post_mime_type );
				$title = wp_get_attachment_link( $attachment->ID, false );
				$content .= '<li class="' . $class . '">' . $title . '</li>';
			}
			$content .= '</ul>';
		}
	}

	return $content;
}
?>
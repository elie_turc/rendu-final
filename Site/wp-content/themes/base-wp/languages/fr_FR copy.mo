��    K      t  e   �      `  
   a     l  	   �     �  !   �  !   �  $   �  "     !   $  "   F     i     �      �     �      �             )   ,  "   V  +   y     �     �  
   �     �  
   �     �     �  3   �     	  5   0	  
   f	     q	     y	     �	  _   �	  W   �	     P
     `
     f
  	   r
     |
     �
  	   �
     �
     �
  %   �
     �
  "        &     5     B  F   I     �  
   �     �     �  \   �     %     .     :  )   H  9   r     �     �     �  	   �     �     �  	                  )     <     N  k  e     �     �     �     
  #     $   A  '   f  %   �  $   �  %   �     �  "     #   =  !   a  #   �     �  "   �  +   �  )   �  -   (     V     _     f     r     z     �     �  R   �     �  2        A  	   S     ]     }  T   �  Q   �     -     D     J  	   [     e     �     �     �     �  *   �       #        <     M  	   \  Q   f     �     �     �     �  l   �     l  	   s     }  +   �  U   �          #     7     ?     O     c     o     �     �     �     �     �           <      &               I   F       -       *   2                  J   0   +      =   (          
   G          $      '   %      /   @               1   B   7   "   	      )   !   #               5           K   9   A   6             D   ;          8   >                H             C       ?   .       4   ,   E                 :                               3    % Comments &larr; Older Comments 1 Comment Add favicon Add the url of your Dribbble page Add the url of your Facebook page Add the url of your Google Plus page Add the url of your Instagram page Add the url of your Linkedin page Add the url of your Pinterest page Add the url of your RSS Add the url of your Tumblr page Add the url of your Twitter page Add the url of your Vimeo page Add the url of your Youtube page Advanced Apple touch icon ipad (76x76 px) Apple touch icon ipad retina (152x152 px) Apple touch icon iphone (57x57 px) Apple touch icon iphone retina (120x120 px) Archives Audios Author: %s Authors Categories Category %1$s Chats Click OK to reset. Any theme settings will be lost! Comments are closed. Continue reading <span class="meta-nav">&rarr;</span> Custom css Day: %s Default options restored. Edit It looks like nothing was found at this location. Maybe try one of the links below or a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment Links Logo Upload Month: %s Most Used Categories Newer Comments &rarr; No Repeat No file chosen Nothing Found Oops! That page can&rsquo;t be found. Options saved. Pinned site Windows 8 (144x144 px) Posted in %1$s Primary Menu Quotes Ready to publish your first post? <a href="%1$s">Get started here</a>. Remove Repeat All Repeat Horizontally Repeat Vertically Sorry, but nothing matched your search terms. Please try again with some different keywords. Statuses Tagged %1$s Theme Options Try looking in the monthly archives. %1$s Upgrade your version of WordPress for full media support. Upload your favicon Upload your logo Videos View File Windows 8 pinned image Year: %s iPad icon iPad retina icon iPhone icon iPhone retina icon post authorby %s post datePosted on %s Project-Id-Version: Base WP v1.20
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-05-28 09:06+0100
PO-Revision-Date: 2015-06-05 08:56+0100
Last-Translator: iografica <info@iografica.it>
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.8.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ../
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 % Commentaires &larr; Anciens Commentaires 1 Commentaire Ajouter une icône Ajoutez l'URL de votre page Dribble Ajoutez l'URL de votre page Facebook Ajoutez l'URL de votre page Google Plus Ajoutez l'URL de votre page Instagram Ajoutez l'URL de votre page Linkedin Ajoutez l'URL de votre page Pinterest Ajoutez l'URL de votre RSS Ajoutez l'URL de votre page Tumblr Ajouter l'URL de votre page Twitter Ajoutez l'URL de votre page Vimeo Ajoutez l'URL de votre page Youtube Avancé Icône Apple Touch ipad (76x76 px) Icône Apple touch ipad retina (152x152 px) Icône pour Apple Touch iPhone (57x57 px) Icône Apple touch iphone retina (120x120 px) Archives Audios Auteur : %s Auteurs Catégories Catégorie %1$s Chats Clickez sur OK pour réinitialiser. Toutes les options du thèmes seront perdues ! Les Commentaires sont fermés. Lire la suite <span class="meta-nav">&rarr;</span> CSS Personnalisé Jour : %s Options par défaut restaurées Modifier On dirait qu'il ne se passe rien ici ! Peut-être essayez une des liens ci-dessous ? Impossible de trouver la page demandée. Peut-être la Recherche peut vous aider. Laisser un Commentaire Liens Uploader le Logo Mois : %s Catégorie la plus utilisée Nouveaux Commentaires &rarr; Pas de répétition Aucun fichier choisi Contenu Introuvable Oups ! Impossible de trouver cette page !  Options sauvées Site Favoris Windows 8 (144x144 px) Posté dans %1$s Menu Principal Citations Prêt à créer votre première publication ? <a href="%1$s">Commencez ici !</a>. Enlever Répéter tout Répétition Horizontale Répétition verticale Désolé mais rien de correspond à vos critères de recherche. Veuillez réessayer avec d'autres mots clé. Status Tags %1$s Options du Thème Essayez de chercher dans les archives. %1$s Mettez à jour votre version de WordPress pour une compatibilité totale des médias. Uploader l'icône Uploader votre Logo Vidéos Voir le fichier Image pour Windows8 Année : %s Icône pour iPad Icône iPad retina Icône pour iPhone Icône iPhone retina par %s Posté le %s 
��    S      �  q   L                  
   .     9  m   F  0   �  8   �  >     T   ]  W   �  s   
	  )   ~	     �	  \   �	  	   
     
     +
     F
     R
     d
     v
     ~
     �
     �
     �
     �
     �
     �
     �
     �
            
        '     D     Q  	   a     k  
   p     {  !   �     �     �     �     �     �     �     �     �     �          %     1     >  
   W     b     |     �     �  	   �     �  ]   �            5   -     c     i     �     �     �     �  
   �     �     �     �     �  	          *   !  *   L     w     �  �  �     �     �     �     �  w   �  2   K  ;   ~  ;   �  U   �  U   L  {   �  -        L  S   ]     �     �  %   �     �          1     L     _     y  .   �  	   �     �     �     �     �          )     <     D  3   L     �     �     �     �     �     �  9   �     ,     0     E     L     T     \     r     �     �     �     �     �  2   �     )  &   B     i     ~     �  
   �     �     �     �     �  F   �     -  $   3     X     j     w     �     �     �     �     �  %   �  	     
   $  -   /  1   ]     �  &   �     B       C      G   K   "   P           <   ,   L   2                    1       F      E       -   A   &   N       D      O   @   '   S         	      .   8       ?      +   $   =   R          #             )                            9       *           >                  H         ;       5   (       I          7         J      !      Q      :   M          3       6   /                         %   4   0       
    %s is missing %s's Dashboard (required) -- Select -- <strong>ERROR</strong>: Couldn&#8217;t register you... please contact the <a href="mailto:%s">webmaster</a> ! <strong>ERROR</strong>: Please enter a username. <strong>ERROR</strong>: Please type your e-mail address. <strong>ERROR</strong>: The email address isn&#8217;t correct. <strong>ERROR</strong>: This email is already registered, please choose another one. <strong>ERROR</strong>: This username is already registered, please choose another one. <strong>ERROR</strong>: This username is invalid because it uses illegal characters. Please enter a valid username. <strong>Success</strong>: Profile updated About Yourself Access Denied. Your site administrator has blocked your access to the WordPress back-office. Add Field Add New User Attachment file is too big Author Info Awaiting Approval Biographical Info Buy Now Confirm Password Contact Info Currently you have no post Delete Display to Public as E-mail Edit Empty post content Empty post title Expiration Time: Expired First Name Invalid attachment file type Invalid post Invalid post id Last Name Live Lock Post: Lock Reason: Lock user from creating new post. Name New Password Nickname Offline Options Options saved Password Strength Payment Please choose a category Please wait... Post Count: Post Deleted Post Editing is disabled Post Left: Post updated succesfully. Publish Time: Role Save Changes Scheduled Settings Saved Share a little biographical information to fill out your profile. This may be shown publicly. Status Strength indicator This page is restricted. Please %s to view this page. Title Type your new password again. Unlimited duration Update Update Field Update Profile User Added User Deleted User doesn't exists Username Usernames cannot be changed. Validity: Website You are not the post author. Cheeting huh! You don't have permission for this purpose You have created Your settings have been saved. Project-Id-Version: WP User Frontend
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-04-14 17:46+0600
PO-Revision-Date: 2015-06-04 16:46+0100
Last-Translator: Tareq Hasan <tareq@wedevs.com>
Language-Team: Tareq Hasan <tareq@wedevs.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _e;__
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.1
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SearchPath-0: ..
 %s est manquant(e) Tableau de bord de %s (requis) -- Sélectionnez -- <strong>ERREUR</strong>: Nous n'avons pas pu vous inscrire... Veuillez contacter le <a href="mailto:%s">webmaster</a> ! <strong>ERREUR</strong>: Veuillez entrer un Pseudo <strong>ERREUR</strong>: Veuillez entrer votre adresse mail <strong>ERREUR</strong>: L'adresse email n'est pas correcte <strong>ERREUR</strong>: Cet email est déjà utilisé, veuillez en choisir un autre. <strong>ERREUR</strong>: Ce Pseudo est déjà utilisé, veuillez en choisir un autre. <strong>ERREUR</strong>: Ce Pseudo est invalide car il utilise des caractères illégaux. Veuillez entrer un Pseudo valide. <strong>Succès </strong>: Profil mis à jour A propos de vous Access Refusé. L'administrateur a bloqué l'accès au Tableau de Bord de Wordpress Ajouter un champ Ajouter un utilisateur La pièce jointe est trop volumineuse Informations de l'auteur En attente de validation Informations biographiques Acheter maintenant Confirmez le mot de passe Informations de contact Pour le moment, vous n'avez aucune publication Supprimer Afficher en tant que E-mail Modifier Corps de la publication vide Titre de la publication vide Date d'expiration: Expiré Prénom Le type de fichier de la pièce-jointe est invalide Publication invalide id de la publication invalide Nom Online Verrouiller la publication Raison du verrouillage : Empecher l'utilisateur de créer une nouvelle publication Nom Nouveau mot de passe Surnom Offline Options Options enregistrées Force du mot de passe Paiement Veuillez choisir une catégorie Veuillez patienter... Nombre de publications: Publication supprimée La modification d'une publication est désactivée Publications restantes : Publication mise à jour avec succès. Date de publication: Rôle Sauver les changements Programmé Paramètres enregsitrés Description Statut Indicateur de force Cette page est protégée. Veuillez vous connecter pour y accéder. %s Titre Entrez à nouveau votre mot de passe Durée illimitée Mise à jour Mettre à jour un champ Mettre à jour le Profil Utilisateur ajouté Utilisateur supprimé L'utilisateur n'existe pas Pseudo Le Pseudo ne peut pas être modifié. Validité Site Webe2 Vous n'êtres pas l'auteur de la publication! Vous n'avez pas les autorisations pour faire cela Vous avez créé Vos paramètres ont été enregistrés 